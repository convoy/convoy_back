<?php
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
    $Headers['Apikey']='';
    }
if(ApiKeyString($Headers['Apikey'])==true){
    require_once("Email.php"); 
    require_once("ws-queries/Openpay.Queries.php");
    $handle = fopen('php://input','r');
    $jsonInput = fgets($handle);
    // Decoding JSON into an Array
    // $userId, $cardId, $planId, $trialDays
    $jsonArray = json_decode($jsonInput,true);
    $UserID = $jsonArray['UserID'];
    $cardID = $jsonArray['CardID'];
    //$trialDays = isset($jsonArray['trialDays']) ? $jsonArray['trialDays'] : false;

    $isWeb = (isset($jsonArray['isWeb']) && $jsonArray['isWeb'] == 1) ? true : false;
    // die(var_dump($isWeb));
    $openpay = new OpenPayDecorator($isWeb);
	//$result = $openpay->subscribeCustomerToPlan($UserID, $cardID, $trialDays);
    $result = $openpay->subscribeCustomerToPlan($UserID, $cardID);
	print_r(json_encode($result, JSON_PRETTY_PRINT));
} else {
    echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
}