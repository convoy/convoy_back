<?php
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("Email.php"); 
	require_once("ws-queries/Reports.Queries.php");
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	// Decoding JSON into an Array
	$jsonArray = json_decode($jsonInput,true);
	$UserID = $jsonArray['UserID'];
	$Report = $jsonArray['Report'];
	$ReportType = $jsonArray['ReportType'];
	ReportIncident($UserID, $ReportType, $Report);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>