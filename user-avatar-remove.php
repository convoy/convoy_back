<?php
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/User.Queries.php"); 
	$handle = fopen('php://input','r');
	$jSONInput = fgets($handle);
	$jSONArray = json_decode($jSONInput,true);
	$UserID = $jSONArray['UserID'];
	UserAvatarRemove($UserID);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>