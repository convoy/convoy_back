<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/Followers.Queries.php");
	require_once("ws-queries/Like.Queries.php"); 
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	$jsonArray = json_decode($jsonInput,true);
	$UserID = $jsonArray['UserID'];
	$FollowID = $jsonArray['FollowID'];

	$arCommon = getAllFollowedCommons($UserID, $FollowID);

	echo json_encode($arCommon, JSON_PRETTY_PRINT);
} else{
    echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
}
?>