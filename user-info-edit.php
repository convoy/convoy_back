<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/User.Queries.php");
	require_once("ws-queries/Purchase.Queries.php");
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	$jsonArray = json_decode($jsonInput,true);
	$UserID = $jsonArray['UserID'];
	//$UserViewing = $jsonArray['UserViewing'];
    $UserViewing = $UserID;
	$UserInfo = UserInfoEdit($UserID, $UserViewing);
    $Purchase = PurchaseReviewGeneral($UserViewing);

    if($UserID == $UserViewing){
        
        $arResp = array(
                        'UserInfo' => $UserInfo,
                        'PurchaseInfo' => $Purchase
                        );
        
        $result = array_merge($arResp['UserInfo'],$arResp['PurchaseInfo']);
        echo json_encode($result, JSON_PRETTY_PRINT);
    }else{
        $arResp = array(
                        'UserInfo' => $UserInfo
                        );
        echo json_encode($UserInfo, JSON_PRETTY_PRINT);
        
    }
    
}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
    http_response_code(401);//'Unauthorized'
	}
?>