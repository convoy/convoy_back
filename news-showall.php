<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/News.Queries.php");
	require_once("ws-queries/Purchase.Queries.php");
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	// Decoding JSON into an Array
	$jsonArray = json_decode($jsonInput,true);

	$UserID = $jsonArray['UserID'];
    if($jsonArray['LastUpdate']!=''){
    	$LastUpdate = $jsonArray['LastUpdate'];
    }else{
        $DateTime = date('Y-m-d H:i:s');
        $LastUpdate = strtotime ( '50 days' , strtotime ( $DateTime ) ) ;
        $LastUpdate = date ( 'Y-m-d H:i:s' , $LastUpdate );
        $LastUpdate = '';
    }

	$News_ShowAll = News_ShowAll($UserID, $LastUpdate);
    $Purchase = PurchaseReviewGeneral($UserID);
    
    $arResp = array(
                    'News_ShowAll' => $News_ShowAll,
                    'PurchaseInfo' => $Purchase
                    );
    
    $result = array_merge($arResp['News_ShowAll'],$arResp['PurchaseInfo']);
    echo json_encode($result, JSON_PRETTY_PRINT);
    
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
    http_response_code(401);//'Unauthorized'
	}
?>