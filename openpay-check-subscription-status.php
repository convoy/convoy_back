<?php
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
    $Headers['Apikey']='';
    }
if(ApiKeyString($Headers['Apikey'])==true){
    require_once("Email.php"); 
    require_once("ws-queries/Openpay.Queries.php");
    $handle = fopen('php://input','r');
    $jsonInput = fgets($handle);
    // Decoding JSON into an Array
    $jsonArray = json_decode($jsonInput,true);
    $UserID = $jsonArray['UserID'];
    $fromWeb = isset($jsonArray['fromWeb']) ? $jsonArray['fromWeb'] : false;
    $isWeb = (isset($jsonArray['isWeb']) && $jsonArray['isWeb'] == 1) ? true : false;
    $openpay = new OpenPayDecorator($isWeb);
	$result = $openpay->getSubscriptionStatus($UserID, $fromWeb);
	print_r(json_encode($result, JSON_PRETTY_PRINT));
} else {
    echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
}