<?php
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/User.Queries.php"); 
	require_once("ws-queries/Purchase.Queries.php"); 
	require_once("ws-queries/Openpay.Queries.php"); 
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	// Decoding JSON into an Array
	$jsonArray = json_decode($jsonInput,true);
	$Login = $jsonArray['Login'];
	$Password = $jsonArray['Password'];
	UserLogin($Login, $Password);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>