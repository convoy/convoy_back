<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/User.Queries.php"); 
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	$jsonArray = json_decode($jsonInput,true);
	$UserID = $jsonArray['UserID'];
	$Fullname = $jsonArray['Fullname'];
	$Descripcion = $jsonArray['Description'];
	$Website = $jsonArray['Website'];
	$Instagram = $jsonArray['Instagram'];
	$Twitter = $jsonArray['Twitter'];
	$Facebook = $jsonArray['Facebook'];
	$Perfil = $jsonArray['Profile'];
	UpdateUser($UserID, $Descripcion, $Website, $Instagram, $Twitter, $Facebook, $Perfil);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>