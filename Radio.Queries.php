<?php
function ConnDB(){ 
	$db_host    ="localhost";
    $db_usuario ="convoyne_convoy";
    $db_password="]ADFToZC!PMV";
    $db_dbname  ="convoyne_convoy";
	$Connection = new mysqli($db_host, $db_usuario, $db_password, $db_dbname);
	if (mysqli_connect_errno()){
		printf("Falló la conexión: %s\n", mysqli_connect_error());
		exit();
		}
	return $Connection;
	}


function UserStatus($Status){
	switch($Status){
		case 0: $Description = 'Inactivo, sin datos de tarjeta'; break;
		case 1: $Description = 'Activo'; break;
		case 2: $Description = 'Suspendido, tarjeta sin fondos'; break;
		case 3: $Description = 'Cancelado, suscripción cancelada por el usuario'; break;
		}
	return $Description;
	}



function RadioList($Fecha, $Hora, $UserID) {
	$Message = array();
	$BD=ConnDB();
	if($UserID!=''){
		$sU="SELECT user_Userid, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
		$qU=$BD->query($sU);
		$rU=$qU->fetch_array();
		$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
		$Today = date('Y-m-d');
		if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
			$s = "SELECT
				Radio.radi_RadioID,
				radi_Title,
				radio_Image,
				Radio.radi_Descripcion,
				Radio_Tracks.rdtr_Description,
				radi_DayOfWeek,
				radi_Date,
				radi_Hour,
				radi_DateTime,
				radi_Status,
				Radio_Tracks.rdtr_Status AS RadioStatus,
				Radio_Tracks.rdtr_Track
			FROM Radio 
			LEFT OUTER JOIN Radio_Tracks ON Radio_Tracks.rdtr_RadioID=radi_RadioID
			WHERE
				radi_Date = '".$Fecha."'
			ORDER BY radi_Hour ASC";
			$q = $BD->query($s);
			$t = $q->num_rows;

			if($t>0){
				$Message['app_status'] = '1';
				$Message['id_usuario'] = $rU['user_Userid'];
				$Message['usuario_status'] = '1';
				$Message['status_description'] = UserStatus($rU['user_Status']);
				$Message['radio'] = array();
				while($r=$q->fetch_array()){
					$row['id_radio'] = $r['radi_RadioID'];
					$row['titulo'] = utf8_encode($r['radi_Title']);
					$row['descripcion'] = utf8_encode($r['radi_Descripcion']);
					$row['url_imagen'] = $r['radio_Image'];
					$row['fecha'] = $r['radi_Date'];
					$row['hora'] = $r['radi_Hour'];
					$row['radio_url_share'] = "http://convoynetwork.com/convoy-webapp/radio?id_radio=".$r['radi_RadioID'];
					$row['url_radio'] = urldecode($r['rdtr_Track']);
					array_push($Message['radio'], $row);
					}
				}
			else{
				$Message['app_status']=0;
				$Message['usuario_status'] = '1';
				$Message['app_mensaje']="No se encontraron resultados";
				}
			}
		else{
			$Message['app_status']=2;
			$Message['app_mensaje']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
			}
		}
	else{
		$Message['app_status']=0;
		$Message['app_mensaje']="No proporcionó un usuario";
		}		
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	}
?>