<?php
function fecha_ahora ($anio, $mes, $dia){
	$meses=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	$fecha_ahora_dia = $dia;
	$fecha_ahora_mes = $mes;
	$fecha_ahora_year = $anio;
	$fecha_ahora = $fecha_ahora_dia." de ".$meses[$fecha_ahora_mes]." del ".$fecha_ahora_year;
	return $fecha_ahora;
	}

function UserStatus($Status){
	switch($Status){
		case 0: $Description = 'Inactivo, sin datos de tarjeta'; break;
		case 1: $Description = 'Activo'; break;
		case 2: $Description = 'Suspendido, tarjeta sin fondos'; break;
		case 3: $Description = 'Cancelado, suscripción cancelada por el usuario'; break;
		}
	return $Description;
	}

?>