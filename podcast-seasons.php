<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/Podcast.Queries.php");
    require_once("ws-queries/Purchase.Queries.php");
	$handle = fopen('php://input','r');
	// Decoding JSON into an Array
	$jsonInput=fgets($handle);
	$jsonArray = json_decode($jsonInput,true);
	$PodcastID = $jsonArray['PodcastID'];
	$UserID = $jsonArray['UserID'];
    $SeasonList = SeasonList($PodcastID, $UserID);
    $Purchase = PurchaseReviewGeneral($UserID);
    
    $arResp = array(
                    'SeasonList' => $SeasonList,
                    'PurchaseInfo' => $Purchase
                    );
    
    $result = array_merge($arResp['SeasonList'],$arResp['PurchaseInfo']);
    echo json_encode($result, JSON_PRETTY_PRINT);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
    http_response_code(401);//'Unauthorized'
	}
?>