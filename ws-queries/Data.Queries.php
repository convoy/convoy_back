<?php
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');

function UserStatus($Status){
	switch($Status){
		case 0: $Description = 'Inactivo, sin datos de tarjeta'; break;
		case 1: $Description = 'Activo'; break;
		case 2: $Description = 'Suspendido, tarjeta sin fondos'; break;
		case 3: $Description = 'Cancelado, suscripción cancelada por el usuario'; break;
		}
	return $Description;
	}


function DataDetail($UserID, $DataType){
    $DB = ConnectDB();
    $Message = array();
    if($DataType == "P")
        $sU="SELECT html_body FROM html_content WHERE id_html=2";
    if($DataType == "T")
        $sU="SELECT html_body FROM html_content WHERE id_html=3";
    $qU=$DB->query($sU);
    $tU = $qU->num_rows;
    if($tU>0){
        $rU=$qU->fetch_array();

        if($DataType == "P")
            $Message['ShareURL'] = 'https://convoynetwork.com/aviso-de-privacidad/';
        if($DataType == "T")
            $Message['ShareURL'] = 'https://convoynetwork.com/terminos-y-condiciones/';
        
        $addHMTLHeader = "<!DOCTYPE html><html><head><meta charset='UTF-8'><style type='text/css'>@font-face{font-family: 'TradeGothicLTStd-BdCn20'; src: url('https://convoynetwork.com/fonts/2F4CF1_0_0.eot?#iefix') format('embedded-opentype'), url('https://convoynetwork.com/fonts/2F4CF1_0_0.woff') format('woff'), url('fonts/2F4CF1_0_0.ttf') format('truetype'), url('https://convoynetwork.com/fonts/2F4CF1_0_0.svg#GothamRounded-Book') format('svg'); font-weight: normal; font-style: normal;}@font-face{font-family: 'Din'; src: url('https://convoynetwork.com/fonts/dinregular.eot?#iefix') format('embedded-opentype'), url('https://convoynetwork.com/fonts/dinregular.woff') format('woff'), url('fonts/dinregular.ttf') format('truetype'), url('https://convoynetwork.com/fonts/dinregular.svg#Din') format('svg'); font-weight: normal; font-style: normal;}body{background-color:#fff;color:#000;font-family:'Din', Arial, sans-serif;font-size:15px;}h1{font-family:'TradeGothicLTStd-BdCn20', Arial, sans-serif;font-size:2em;text-transform:uppercase;font-weight:normal;letter-spacing:0.01em;padding:0;margin:0;}img{width: 100%;height: auto;}a{color:#000;font-weight:bold;}</style><body>";
        $addHMTLFooter = "</body></html>";
        
        $Message['Content'] = $addHMTLHeader.$rU['html_body'].$addHMTLFooter;
        //			}
        //		else{
        //			$Message['AppStatus']=0;
        //			$Message['AppResponse']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
        //			}
    }
    else{
        $Message['AppStatus']='0';
        $Message['AppResponse']="Usuario inválido.";
    }
    print_r(json_encode($Message, JSON_PRETTY_PRINT));
}

    

?>