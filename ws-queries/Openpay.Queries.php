<?php

require_once(dirname(__FILE__) . '/../function.ConnDB.php');

require(dirname(__FILE__) . '/../openpay/Openpay.php');

class OpenPayDecorator
{
	public $openpay;

	// Prod
	private $openpayId = 'msterkwozwegqzaeowsu';
	private $openpayPrivateKey = 'sk_16cd9676fc3d4f839d607af089514f15';
	private $planId = 'pfqbtcejlaqgyypcwtop';
	private $openpayPublicKey = 'pk_7ea5c83b97754e1dafc70945872b6898';
	
	// Sandbox
	// private $openpayId = 'm1zbkbapzduqz7jtwp1p';
	// private $openpayPrivateKey = 'sk_c2dbe2d1e4574a568da2e09d1308737b';
	// private $planId = 'pnlvgakuwrpokrihus9t';
	// private $openpayPublicKey = 'pk_8a72764ebaa04c6dab408b2e3043656b';

	public function __construct($isWeb = false)
	{
		Openpay::setId($this->openpayId);
		Openpay::setApiKey($this->openpayPrivateKey);

		$this->openpay = $this->getInstance();

		// if (!$isWeb) {
		// 	//For Production stack only
		// 	Openpay::setProductionMode(true);
		// } else {
		// 	Openpay::setSandboxMode(true);
		// }

		Openpay::setProductionMode(true);

		// die(var_dump(Openpay::getSandboxMode()));
	}

	private function getInstance()
	{
		return Openpay::getInstance($this->openpayId, $this->openpayPrivateKey);
	}

	/**
	 * this returns an object like this...
	 *
	 * {
	 *  id : "axapgwwolofnckfui2wx",
	 *  name : "Mi cliente uno",
	 *  last_name : null,
	 *  email : "micliente@gmail.com", 
	 *  phone_number : null,
	 *  status : "active",
	 *  balance : 0,
	 *  clabe : "646180109400138692", ???
	 *  address : null,
	 *  creation_date : "2014-02-14T12:30:09-06:00"
	 * }
	 */
	public function createClient(
		$userId, 
		$lastName = false, 
		$phoneNumber = false, 
		$address = false
	)
	{
		$DB = ConnectDB();
		$sql = "SELECT * FROM Users WHERE user_Userid = " . $userId."; ";
		$qry = $DB->query($sql);
		$total = $qry->num_rows;

		if ($total > 0) {
            

			$sqlOP = 'SELECT UserID, UserOpenPayID FROM user_openpay WHERE UserID = ' . $userId;
			$qryOP = $DB->query($sqlOP);
			$userExists = ($qryOP->num_rows > 0) ? true : false;
            
            
            $sql2 = 'SELECT * FROM openpay_subscriptions WHERE UserID = ' . $userId;
            $qry2 = $DB->query($sql2);
            $subsExists = ($qry2->num_rows > 0) ? true : false;
            
			if ($userExists && $subsExists) {
				return array('AppStatus'=>'0', 'AppResponse' => 'Este Usuario ya ha sido registrado anteriormente y cuenta con una suscripción');
            }else if($userExists && !$subsExists ){
                $r = $qryOP ->fetch_array();
                return array('AppStatus'=>'2', 'AppResponse' => 'Este Usuario ya ha sido registrado anteriormente y no cuenta con una suscripción', 'UserOpenPayId'=>$r['UserOpenPayID']);
            }
            
            $s1 = "UPDATE  convoyne_convoy.Users SET user_SubType =  'a' WHERE  Users.user_Userid =". $userId .";";
            $q1 = $DB->query($s1);
            
			$userInfo = $qry->fetch_array(MYSQLI_ASSOC);
			$customerData = array(
				'name' => $userInfo['user_Login'],
				'email' => $userInfo['user_Email'],
				'last_name' => ($lastName) ? $lastName : null,
				'phone_number' => ($phoneNumber) ? $phoneNumber : null,
				'address' => ($address) ? $address : null,
			);

			try {

				$customer = $this->openpay->customers->add($customerData);

				// Get all information from OpenPay API and save it in the new
				// then return the curtomer->id with other data to the front

				// $cable = isset($customer->cable) ? $customer->cable : null;

				$sql = 'INSERT INTO user_openpay 
				(UserID, UserOpenPayID, Status, Balance, Clabe, Address, Creation_date)
				VALUES 
				('.$userInfo['user_Userid'].', \''.$customer->id.'\',
				 \''.$customer->status.'\', '.$customer->balance.',
				 \''.$customer->clabe.'\', \''.$customer->address.'\',
				 \''.$customer->creation_date.'\'
				);';
				$qry = $DB->query($sql);

				if (!$qry) {
					return array(
						'AppStatus' => '0',
						'AppResponse' => 'Error: no se ha logrado registrar al usuario localmente'
					);
				} else {
					return array(
						'AppStatus' => '1',
						'AppResponse' => 'Usuario agregado exitosamente',
						'userData' => array(
							'UserOpenPayID' => $customer->id,
							'Status' => $customer->status,
							'Balance' => $customer->balance,
							'Clabe' => $customer->clabe,
							'Address' => $customer->address,
							'Creation_date' => $customer->creation_date
						),
						'UserOpenPayId' => $customer->id
					);
				}

			} catch (OpenpayApiTransactionError $e) {
				$message = 'ERROR: ' . $e->getMessage() . 
			          ' [codigo de error: ' . $e->getErrorCode() . 
			          ', categoria: ' . $e->getCategory() . 
			          ', codigo HTTP: '. $e->getHttpCode() . 
			          ', request ID: ' . $e->getRequestId() . ']';
			    return array('AppStatus'=>'0', 'AppResponse' => $message);

			} catch (OpenpayApiRequestError $e) {
			    $message = 'ERROR: ' . $e->getMessage();
			    return array('AppStatus'=>'0', 'AppResponse' => $message);

			} catch (OpenpayApiConnectionError $e) {
			    $message = 'ERROR: Al intentar conectarse a la API: ' . $e->getMessage();
			    return array('AppStatus'=>'0', 'AppResponse' => $message);

			} catch (OpenpayApiAuthError $e) {
			    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
			    return array('AppStatus'=>'0', 'AppResponse' => $message);

			} catch (OpenpayApiError $e) {
			    $message = 'ERROR de la API: ' . $e->getMessage();
			    return array('AppStatus'=>'0', 'AppResponse' => $message);

			} catch (Exception $e) {
			    $message = 'Error del script: ' . $e->getMessage();
			    return array('AppStatus'=>'0', 'AppResponse' => $message);
			}
			
			
		} else {
			return array(
				'AppStatus' => '0',
				'AppResponse' => 'Error: no se ha encontrado a un usuario que coincida'
			);
		}
	}

	public function getPlan()
	{
		return $this->openpay->plans->get($this->planId);
	}

	public function getCustomerId($userId)
	{
        $DB = ConnectDB();
		//$sql = "SELECT * FROM user_openpay WHERE UserID = " . customerId .";";
        $sql = "SELECT * FROM user_openpay WHERE UserID = " . $userId .";";
		$qry = $DB->query($sql);
		$total = $qry->num_rows;
		if ($total == 0){
			return array('AppStatus'=>'0', 'AppResponse' => 'Este Usuario no se ha dado de alta en OpenPay, por favor dese de alta');
		} else {
			$userInfo = $qry->fetch_array(MYSQLI_ASSOC);
			return array(
				'AppStatus' => '1',
				'AppResponse' => 'Datos del usuario accedidos con exito.',
				'UserOpenPayId' => $userInfo['UserOpenPayID']
			);
		}
        $DB->close();
		// return $this->openpay->customers->add($customerData);
	}

	public function subscribeCustomerToPlan($userId, $cardId, $trialDays = false)
    {
        
        //trialDate default value = false
		$trialDate = (!$trialDays) ? false : $trialDate = date('Y-m-d',strtotime("+{$trialDays} day"));

		$userInfo = $this->getCustomerId($userId);
		if ($userInfo['AppStatus'] == '0') {
			return $userInfo;
		}

		$previousUserSubscription = $this->getCustomerSubscriptionId($userId);
		if ($previousUserSubscription['AppStatus'] == '1') {
			return array(
				'AppStatus' => 0,
				'AppResponse' => 'Este Usuario tiene una suscripcion anterior',
				'SubscriptionID' => $previousUserSubscription['SubscriptionID'],
			);
		}

		$subscriptionData = array(
		    'plan_id' => $this->planId,
		    'card_id' => $cardId
		);

		if ($trialDate) {
			$subscriptionData['trial_end_date'] = $trialDate;
		}

		try {
            $DB = ConnectDB();
            
            $customer = $this->openpay->customers->get($userInfo['UserOpenPayId']);
            //If subscription exists this should be invalid
			$subscription = $customer->subscriptions->add($subscriptionData);
            
            $subsDate = new DateTime($subscription->creation_date);
            $theDate = date_format($subsDate, 'Y-m-d H:i:s');
			
            $sql = "INSERT INTO openpay_subscriptions 
            (UserID, SubscriptionID, Status, Creation_date) 
            VALUES 
            (".$userId.",'".$subscription->id."','".$subscription->status."','".$theDate."') ";
			$qry = $DB->query($sql);

			if (!$qry) {
				return array(
					'AppStatus' => '0',
					'AppResponse' => 'Error: no se ha logrado registrar la suscripción localmente'
				);
			} else {
				return array(
					'AppStatus' => '1',
					'AppResponse' => 'La Suscripción inicio proceso exitosamente',
					'subscriptionData' => array(
						'SubscriptionID' => $subscription->id,
						'Status' => $subscription->status,
						'cancel_at_period_end' => $subscription->cancel_at_period_end,
						'charge_date' => $subscription->charge_date,
						'creation_date' => $subscription->creation_date
					),
					'SubscriptionID' => $subscription->id
				);
			}
            
		} catch (OpenpayApiRequestError $e) {
		    $message = 'ERROR: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiConnectionError $e) {
		    $message = 'ERROR: Al intentar colocarme en el mercado ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiAuthError $e) {
		    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiError $e) {
		    $message = 'ERROR de la API: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (Exception $e) {
		    $message = 'Error del script: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);
		}
        $DB->close();
	}

	public function getCustomerSubscriptionId($userId)
	{
        $DB = ConnectDB();
		//$sql = "SELECT * FROM openpay_subscriptions WHERE UserID = " . $userId."
        $sql = "SELECT * FROM openpay_subscriptions WHERE UserID = " . $userId ."
		ORDER BY Creation_date DESC, id DESC LIMIT 1;";
		$qry = $DB->query($sql);
		$total = $qry->num_rows;
		$userSubsription = $qry->fetch_array(MYSQLI_ASSOC);
		if ($total == 0){
			return array('AppStatus'=>'0', 'AppResponse' => 'El usuario no esta suscrito a ningun contenido');
		} elseif ($userSubsription['Status'] == 'canceled') {
			return array('AppStatus'=>'0', 'AppResponse' => 'El usuario ha cancelado la suscripción');
		} else {
			return array(
				'AppStatus' => '1',
				'AppResponse' => 'Este Usuario se ha suscrito a algo.',
				'SubscriptionID' => $userSubsription['SubscriptionID'],
			);
		}
        $DB->close();
		// return $this->openpay->customers->add($customerData);
	}

	public function getSubscriptionStatus($userId, $fromWeb = false)
	{
		//$userInfo = $this->getClientId($userId);
		$userInfo = $this->getCustomerId($userId);
        /*
        if(!$fromWeb)
            return array('AppStatus' => '0', 'AppResponse' => '¡Activa tu suscripción próximamente!');*/
		
        if ($userInfo['AppStatus'] == '0') {
			return $userInfo;
		}
            
		$subscriptionInfo = $this->getCustomerSubscriptionId($userId);
		
		if ($subscriptionInfo['AppStatus'] == '0') {
			return $subscriptionInfo;
		}
        
        $customer = $this->openpay->customers->get($userInfo['UserOpenPayId']);
        $subscription = $customer->subscriptions->get($subscriptionInfo['SubscriptionID']);
        $card = $subscription->card;
        return array(
			'AppStatus' => '1',
			'AppResponse' => 'Datos de suscripción accedidos exitosamente',
			'subscriptionData' => array(
				'SubscriptionID' => $subscription->id,
				'Status' => $subscription->status,
				'cancel_at_period_end' => $subscription->cancel_at_period_end,
				'charge_date' => $subscription->charge_date,
				'creation_date' => $subscription->creation_date,
				'card' => array(
					"id" => $card->id,
					"type" => $card->type,
					"brand" => $card->brand,
					"address" => $card->address,
					"card_number" => $card->card_number,
					"holder_name" => $card->holder_name,
					"expiration_year" => $card->expiration_year,
					"expiration_month" => $card->expiration_month,
					"allows_charges" => $card->allows_charges,
					"allows_payouts" => $card->allows_payouts,
					"bank_name" => $card->bank_name,
					"bank_code" => $card->bank_code
				),
			),
			'SubscriptionID' => $subscription->id
		);

	}

	/**
	 *
	 *
	 */
	public function getCustomerCards($userId)
	{
		$userInfo = $this->getCustomerId($userId);

		if ($userInfo['AppStatus'] == '0') {
			return $userInfo;
		}

		$findDataRequest = array(
		    'creation[gte]' => '2013-01-01',
		    'creation[lte]' => date('Y-m-d'),
		    'offset' => 0,
		    'limit' => 5
		);

		try {

			$customer = $this->openpay->customers->get($userInfo['UserOpenPayId']);
	        $cardList = $customer->cards->getList($findDataRequest);
	        $card = $cardList[0];
			return array(
				'AppStatus' => '1',
				'AppResponse' => 'Datos de tarjetas accedidos exitosamente',
				'CardID' => $card->id,
				'card' => array(
					"id" => $card->id,
					"type" => $card->type,
					"brand" => $card->brand,
					"address" => $card->address,
					"card_number" => $card->card_number,
					"holder_name" => $card->holder_name,
					"expiration_year" => $card->expiration_year,
					"expiration_month" => $card->expiration_month,
					"allows_charges" => $card->allows_charges,
					"allows_payouts" => $card->allows_payouts,
					"bank_name" => $card->bank_name,
					"bank_code" => $card->bank_code
				),
			);

		} catch (OpenpayApiRequestError $e) {
		    $message = 'ERROR: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiConnectionError $e) {
		    $message = 'ERROR: Al intentar colocarme en el mercado ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiAuthError $e) {
		    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiError $e) {
		    $message = 'ERROR de la API: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (Exception $e) {
		    $message = 'Error del script: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);
		}
        $DB->close();

	}

	/**
	 *
	 * cancel_at_period_end boolean (opcional) Indica si se cancela la suscripción al terminar el periodo
	 * trial_end_date string (opcional, longitud = 10)  Si no se indica se utilizará el valor de trial_days del plan para calcularlo. http://www.openpay.mx/docs/api/#actualizar-una-suscripción
	 * source_id string (opcional, longitud = 45)  Identificador del token o la tarjeta previamente registrada al cliente con la que se cobrará la suscripción.
	 * card object (opcional) Medio de pago con el cual se cobrará la suscripción. Ver objeto tarjeta.
	 */
	public function updateSubscription( $userId, 
		$cancelAtPeriodEnd = false, 
		$trialEndDate = false, 
		$sourceId = false)
	{
        
		if (!$cancelAtPeriodEnd && !$trialEndDate && !$sourceId && !$card) {
			return array(
				'AppStatus' => '0',
				'AppResponse' => 'Error: Debemos recibir al menos un parametro para procesar actualizacion'
			);
		} elseif ($cancelAtPeriodEnd !== 'false' && $cancelAtPeriodEnd !== 'true') {
			return array(
				'AppStatus' => '0',
				'AppResponse' => 'Error: Parametro CancelAtPeriodEnd debe ser "true" o "false"'
			);
		}

		$userInfo = $this->getCustomerId($userId);
        
		if ($userInfo['AppStatus'] == '0') {
			return $userInfo;
		}

		$subscriptionInfo = $this->getCustomerSubscriptionId($userId);
		
		if ($subscriptionInfo['AppStatus'] == '0') {
			return $subscriptionInfo;
		}

		try {
            $DB = ConnectDB();
			$customer = $this->openpay->customers->get($userInfo['UserOpenPayId']);
			$subscription = $customer->subscriptions->get($subscriptionInfo['SubscriptionID']);

			if ($cancelAtPeriodEnd) 
				$subscription->cancel_at_period_end = $cancelAtPeriodEnd == 'true' ? true : false;
			if ($trialEndDate)
				$subscription->trial_end_date = $trialEndDate;
			if ($sourceId)
				$subscription->source_id = $sourceId;

			$subscription->save();

			$card = $subscription->card;

			$subsDate = new DateTime($subscription->creation_date);
            $subsDate2 = date_format($subsDate, 'Y-m-d H:i:s');
            
            $sql = "INSERT INTO openpay_subscriptions (UserID, SubscriptionID, Status, Creation_date) VALUES (".$userId.",'".$subscription->id."','".$subscription->status."','".$subsDate2."') ";
			$qry = $DB->query($sql);

			if (!$qry) {
				return array(
					'AppStatus' => '0',
					'AppResponse' => 'Error: no se ha logrado actualizar la suscripción localmente'
				);
			} else {
				return array(
					'AppStatus' => '1',
					'AppResponse' => 'La Suscripción se actualizó exitosamente',
					'subscriptionData' => array(
						'SubscriptionID' => $subscription->id,
						'Status' => $subscription->status,
						'cancel_at_period_end' => $subscription->cancel_at_period_end,
						'charge_date' => $subscription->charge_date,
						'creation_date' => $subscription->creation_date,
						'card' => array(
							"id" => $card->id,
							"type" => $card->type,
							"brand" => $card->brand,
							"address" => $card->address,
							"card_number" => $card->card_number,
							"holder_name" => $card->holder_name,
							"expiration_year" => $card->expiration_year,
							"expiration_month" => $card->expiration_month,
							"allows_charges" => $card->allows_charges,
							"allows_payouts" => $card->allows_payouts,
							"bank_name" => $card->bank_name,
							"bank_code" => $card->bank_code
						),
					),
					'SubscriptionID' => $customer->id
				);
			}	
		
		} catch (OpenpayApiRequestError $e) {
		    $message = 'ERROR: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiConnectionError $e) {
		    $message = 'ERROR: Al intentar colocarme en el mercado ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiAuthError $e) {
		    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiError $e) {
		    $message = 'ERROR de la API: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (Exception $e) {
		    $message = 'Error del script: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);
		}
        $DB->close();
	}

	public function updateSubscriptionCard( $userId,
		$cancelAtPeriodEnd = 'false', 
		$trialEndDate = false, 
		$CardID)
	{
        
		if (!$cancelAtPeriodEnd && !$trialEndDate && !$sourceId && !$CardID) {
			return array(
				'AppStatus' => '0',
				'AppResponse' => 'Error: Debemos recibir al menos un parametro para procesar actualizacion'
			);
		} 

		if (!$CardID){
			return array(
				'AppStatus' => '0',
				'AppResponse' => 'Error: No ha seleccionado una tarjeta para este proceso'
			);
		}

		$userInfo = $this->getCustomerId($userId);
        
		if ($userInfo['AppStatus'] == '0') {
			return $userInfo;
		}

		$subscriptionInfo = $this->getCustomerSubscriptionId($userId);
		
		if ($subscriptionInfo['AppStatus'] == '0') {
			return $subscriptionInfo;
		}

		try {
            $DB = ConnectDB();
			$customer = $this->openpay->customers->get($userInfo['UserOpenPayId']);
			$subscription = $customer->subscriptions->get($subscriptionInfo['SubscriptionID']);
            $subscription->source_id = $CardID;

			if ($cancelAtPeriodEnd) 
				$subscription->cancel_at_period_end = $cancelAtPeriodEnd == 'true' ? true : false;
			if ($trialEndDate)
				$subscription->trial_end_date = $trialEndDate;

			$subscription->save();
			$card = $subscription->card;

			/*$sql = 'INSERT INTO openpay_subscriptions
			(UserID, SubscriptionID, Status, Creation_date)
			VALUES 
			('.$userInfo['user_Userid'].', \''.$subscription->id.'\', 
			'.$subscription->status.', '. $subscription->creation_date .');';*/
            $subsDate = new DateTime($subscription->creation_date);
            $subsDate2 = date_format($subsDate, 'Y-m-d H:i:s');
            
            $sql = "INSERT INTO openpay_subscriptions (UserID, SubscriptionID, Status, Creation_date) VALUES (".$userId.",'".$subscription->id."','".$subscription->status."','".$subsDate2."') ";
			$qry = $DB->query($sql);

			if (!$qry) {
				return array(
					'AppStatus' => '0',
					'AppResponse' => 'Error: no se ha logrado actualizar la suscripción localmente'
				);
			} else {
				return array(
					'AppStatus' => '1',
					'AppResponse' => 'La Suscripción se actualizó exitosamente',
					'subscriptionData' => array(
						'SubscriptionID' => $subscription->id,
						'Status' => $subscription->status,
						'cancel_at_period_end' => $subscription->cancel_at_period_end,
						'charge_date' => $subscription->charge_date,
						'creation_date' => $subscription->creation_date,
                        'card' => array(
							"id" => $card->id,
							"type" => $card->type,
							"brand" => $card->brand,
							"address" => $card->address,
							"card_number" => $card->card_number,
							"holder_name" => $card->holder_name,
							"expiration_year" => $card->expiration_year,
							"expiration_month" => $card->expiration_month,
							"allows_charges" => $card->allows_charges,
							"allows_payouts" => $card->allows_payouts,
							"bank_name" => $card->bank_name,
							"bank_code" => $card->bank_code
						),
					),
					'SubscriptionID' => $subscription->id
				);
			}	
		
		} catch (OpenpayApiRequestError $e) {
		    $message = 'ERROR: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiConnectionError $e) {
		    $message = 'ERROR: Al intentar colocarme en el mercado ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiAuthError $e) {
		    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiError $e) {
		    $message = 'ERROR de la API: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (Exception $e) {
		    $message = 'Error del script: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);
		}
        $DB->close();
	}

	/**
	*
	*/
	public function cancelCard( $userId, $cardId )
	{
        
		$userInfo = $this->getCustomerId($userId);
        
		if ($userInfo['AppStatus'] == '0') {
			return $userInfo;
		}

		try {
            $customer = $this->openpay->customers->get($userInfo['UserOpenPayId']);
			$card = $customer->cards->get($cardId);
			$card->delete();

			return array(
				'AppStatus' => '1',
				'AppResponse' => 'La terjeta se ha eliminado con exito',
				'UserOpenPayId' => $customer->id
			);
		
		} catch (OpenpayApiRequestError $e) {
		    $message = 'ERROR: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiConnectionError $e) {
		    $message = 'ERROR: Al intentar colocarme en el mercado ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiAuthError $e) {
		    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiError $e) {
		    $message = 'ERROR de la API: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (Exception $e) {
		    $message = 'Error del script: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);
		}
	}


	/**
	*
	*/
	public function cancelSubscription( $userId, $razon )
	{
        
		//$userInfo = $this->getClientId($userId);
        $userInfo = $this->getCustomerId($userId);
        
		if ($userInfo['AppStatus'] == '0') {
			return $userInfo;
		}

		$subscriptionInfo = $this->getCustomerSubscriptionId($userId);
		
		if ($subscriptionInfo['AppStatus'] == '0') {
			return $subscriptionInfo;
		}

		try {
            $DB = ConnectDB();
			$customer = $this->openpay->customers->get($userInfo['UserOpenPayId']);
			$subscription = $customer->subscriptions->get($subscriptionInfo['SubscriptionID']);

			$subscription->delete();

			/*$sql = 'INSERT INTO openpay_subscriptions
			(UserID, SubscriptionID, Status, Creation_date)
			VALUES 
			('.$userInfo['user_Userid'].', \''.$subscription->id.'\', 
			'.$subscription->status.', '. $subscription->creation_date .');';*/
            $subsDate = new DateTime($subscription->creation_date);
            $subsDate2 = date_format($subsDate, 'Y-m-d H:i:s');
            
            $sql = "INSERT INTO openpay_subscriptions (UserID, SubscriptionID, Status, Creation_date) VALUES (".$userId.",'".$subscription->id."','canceled','".$subsDate2."') ";
			$qry = $DB->query($sql);
            
            $sql2 = "UPDATE  convoyne_convoy.Users SET  user_CancelReason =  '".$razon."' WHERE  Users.user_Userid =".$userId.";";
            $qry2 = $DB->query($sql2);
            
			if (!$qry && !$qry2) {
				return array(
					'AppStatus' => '0',
					'AppResponse' => 'Error: no se ha logrado cancelado la suscripción localmente'
				);
			} else {
				return array(
					'AppStatus' => '1',
					'AppResponse' => 'La Suscripción se cancelado exitosamente',
					'subscriptionData' => array(
						'SubscriptionID' => $subscription->id,
						'Status' => 'canceled',
						'cancel_at_period_end' => $subscription->cancel_at_period_end,
						'charge_date' => $subscription->charge_date,
						'creation_date' => $subscription->creation_date
					),
					'SubscriptionID' => $subscription->id
				);
            }
            $DB->close();
		
		} catch (OpenpayApiRequestError $e) {
		    $message = 'ERROR: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiConnectionError $e) {
		    $message = 'ERROR: Al intentar colocarme en el mercado ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiAuthError $e) {
		    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiError $e) {
		    $message = 'ERROR de la API: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (Exception $e) {
		    $message = 'Error del script: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);
		}
	}

	/**
	 * just for test
	 */
	/*
	public function addCardToCustomer($userId)
	{
		try {
			$cardDataRequest = array(
			    'holder_name' => 'Test Cardcustomer',
			    'card_number' => '4111111111111111',
			    'cvv2' => '123',
			    'expiration_month' => '12',
			    'expiration_year' => '17'
			);

			$userInfo = $this->getCustomerId($userId);
	        
			if ($userInfo['AppStatus'] == '0') {
				return $userInfo;
			}

			$customer = $this->openpay->customers->get($userInfo['UserOpenPayId']);
			$card = $customer->cards->add($cardDataRequest);

			return array(
				'AppStatus' => '1',
				'AppResponse' => 'Card Successfully Created',
				'CardId' => $card->id,
			);
		} catch (OpenpayApiRequestError $e) {
		    $message = 'ERROR: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiConnectionError $e) {
		    $message = 'ERROR: Al intentar colocarme en el mercado ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiAuthError $e) {
		    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiError $e) {
		    $message = 'ERROR de la API: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (Exception $e) {
		    $message = 'Error del script: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);
		}
			

	}
	*/

	/**
	 * Hardcoding stuff
	 */
	public function hardCodeSubscribe()
	{

		$subscriptionData = array(
		    "trial_end_date" => '2016-02-29', 
		    'plan_id' => $this->planId,
		    'card_id' => 'krckx83arwhwkhpurhdn'
		);

		try {

			$customer = $this->openpay->customers->get('aty0ibsptet5fplmi6ut');
			$subscription = $customer->subscriptions->add($subscriptionData);

				return array(
					'AppStatus' => '1',
					'AppResponse' => 'La Suscripción inicio proceso exitosamente',
					'subscriptionData' => array(
						'SubscriptionID' => $subscription->id,
						'Status' => $subscription->status,
						'cancel_at_period_end' => $subscription->cancel_at_period_end,
						'charge_date' => $subscription->charge_date,
						'creation_date' => $subscription->creation_date
					),
					'SubscriptionID' => $customer->id
				);
			
		
		} catch (OpenpayApiRequestError $e) {
		    $message = 'ERROR: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiConnectionError $e) {
		    $message = 'ERROR: Al intentar colocarme en el mercado ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiAuthError $e) {
		    $message = 'ERROR: En la autenticación: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (OpenpayApiError $e) {
		    $message = 'ERROR de la API: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);

		} catch (Exception $e) {
		    $message = 'Error del script: ' . $e->getMessage();
		    return array('AppStatus'=>'0', 'AppResponse' => $message);
		}
	}
}

// $openpay = new OpenPayDecorator();
// $subs = $openpay->hardCodeSubscribe();
// var_dump($subs);
