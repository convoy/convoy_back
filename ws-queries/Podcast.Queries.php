<?php 
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');


function UserStatus($Status){
	switch($Status){
		case 0: $Description = 'Inactivo, sin datos de tarjeta'; break;
		case 1: $Description = 'Activo'; break;
		case 2: $Description = 'Suspendido, tarjeta sin fondos'; break;
		case 3: $Description = 'Cancelado, suscripción cancelada por el usuario'; break;
		}
	return $Description;
	}





function PodcastList($Date, $UserID){
	$BD=ConnectDB();
	$Message=array();
	$sU="SELECT user_UserID, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
	$qU=$BD->query($sU);
	$rU=$qU->fetch_array();
	$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
	$Today = date('Y-m-d');
//	if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
		$Message['AppStatus']=1;
		$Message['UserID']=$rU['user_UserID'];
		$Message['UserStatus']='1';
		$Message['UserStatusDescription']=UserStatus($rU['user_Status']);
		$s = "
		SELECT
			podc_PodcastID, podc_Title, podc_Description, podc_Image, podc_Creation, podc_Status,
			(SELECT COUNT(seas_SeasonID) FROM Podcast_Season WHERE seas_Status='A' AND seas_PodcastID = podc_PodcastID) AS NoSeasons,
			(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='P' AND like_ObjectID=podc_PodcastID) AS NoLikes,
			(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='P' AND like_ObjectID=podc_PodcastID AND like_UserID='".$UserID."') AS UserLike
		FROM Podcast
		WHERE podc_Status='A'";
		//WHERE podc_Status='A' AND podc_Creation <= '".$Today." 00:00:00'";
		//$s.="ORDER BY podc_PodcastID ASC LIMIT 0, 20";
        $s.="ORDER BY podc_Order ASC ";
		$q = $BD->query($s);
		$t = $q->num_rows;
		if($t>0){
			$Message['Podcasts']=array();
			while($r=$q->fetch_array()) {
				$row["PodcastID"] = $r['podc_PodcastID'];
				$row["PodcastTitle"]=utf8_encode($r['podc_Title']);
				$row["Description"]=utf8_encode($r['podc_Description']);
				$row["ImageURL"] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$r['podc_PodcastID'].'/'.$r['podc_Image'];
				$row["Seasons"] = $r['NoSeasons'];
				$row["UserLike"] = $r['UserLike'];
				$row["Likes"] = $r['NoLikes'];
				$row["ShareURL"] = "https://www.convoynetwork.com/convoy-webapp/podcasts?id=".$r['podc_PodcastID'];
				$row["PublishDate"] = $r['podc_Creation'];
				$row["PodcastStatus"]= $r['podc_Status'];
				array_push($Message['Podcasts'], $row);
				}
			}
		else{
			$Message['AppStatus']='0';
			$Message['AppResponse']="No hay resultados.";
			}
//		}
//	else{
//		$Message['AppStatus']=2;
//		$Message['AppResponse']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
//		}
	//print_r(json_encode($Message, JSON_PRETTY_PRINT));
    return $Message;
 	$BD->close();
 	}


function SeasonList($PodcastID, $UserID){
	$BD=ConnectDB();
	$Message=array();
	$sU="
	SELECT 
		user_UserID, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
	$qU=$BD->query($sU);
	$rU=$qU->fetch_array();
	$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
	$Today = date('Y-m-d');
//	if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
		$Message['AppStatus']=1;
		$Message['UserID']=$rU['user_UserID'];
		$Message['UserStatus']='1';
		$Message['UserStatusDescription']=UserStatus($rU['user_Status']);
		$s = "
				SELECT
			seas_SeasonID,
			seas_PodcastID,
			seas_Title,
			seas_Description,
			seas_Image,
			seas_Creation,
			seas_Status,
			(SELECT COUNT(epis_EpisodeID) FROM Podcast_Episodes WHERE epis_Status='A' AND epis_SeasonID = seas_PodcastID) AS NoEpisodes,
			(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='S' AND like_ObjectID=seas_SeasonID) AS NoLikes,
			(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='S' AND like_ObjectID=seas_SeasonID AND like_UserID='".$UserID."') AS UserLike
		FROM Podcast_Season
		WHERE seas_Status='A' AND seas_PodcastID = '".$PodcastID."'";
		$s.="ORDER BY seas_Title DESC LIMIT 0, 20";


		$q = $BD->query($s);
		$t = $q->num_rows;
		if($t>0){
			$Message['Seasons']=array();
			while($r=$q->fetch_array()) {
				$row["PodcastID"] = $r['seas_PodcastID'];
				$row["SeasonID"] = $r['seas_SeasonID'];
				$row["SeasonTitle"] = $r['seas_Title'];
				$row["Description"] = $r['seas_Description'];
				$row["ImageURL"] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$PodcastID.'/'. $r['seas_SeasonID']. '/' .$r['seas_Image'];
				$row["NoEpisodes"] = $r['NoEpisodes'];
				$row["Likes"] = $r['NoLikes'];
				$row["UserLike"] = $r['UserLike'];
				$row["ShareURL"] = "https://convoynetwork.com/convoy-webapp/season?id=".$r['seas_PodcastID'];
				$row["PublishDate"] = $r['seas_Creation'];
				$row["SeasonStatus"]= $r['seas_Status'];
				
				
				
				
				
				array_push($Message['Seasons'], $row);
				}
			}
		else{
			$Message['AppStatus']='0';
			$Message['AppResponse']="No hay resultados.";
			}
//		}
//	else{
//		$Message['AppStatus']=2;
//		$Message['AppResponse']="Tu periodo de prueba expirado necesitas renovar tu suscripción para ver este contenido.";
//		}
    return $Message;
    //print_r(json_encode($Message, JSON_PRETTY_PRINT));
 	$BD->close();
 	}


function EpisodesList($SeasonID, $UserID){
	$BD=ConnectDB();
	$Message=array();
	$sU="SELECT user_UserID, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
	$qU=$BD->query($sU);
	$rU=$qU->fetch_array();
	$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
	$Today = date('Y-m-d');
//	if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
		$Message['AppStatus']=1;
		$Message['UserID']=$rU['user_UserID'];
		$Message['UserStatus']='1';
		$Message['UserStatusDescription']=UserStatus($rU['user_Status']);
		$s = "
		SELECT
			podc_PodcastID,
			podc_Title,
			seas_Title,
			epis_EpisodeID,
			epis_SeasonID,
			epis_Episode,
			epis_Title,
			epis_Description,
            (SELECT seas_Image FROM Podcast_Season WHERE seas_SeasonID = epis_SeasonID  ) AS epis_Image,
			epis_TrackURL,
			epis_Length,
			epis_PlayDate,
			epis_Creation,
			epis_Status,
			(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='E' AND like_ObjectID=epis_EpisodeID) AS NoLikes,
			(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='E' AND like_ObjectID=epis_EpisodeID AND like_UserID='".$UserID."') AS UserLike
		FROM Podcast_Episodes
		INNER JOIN Podcast_Season ON epis_SeasonID=seas_SeasonID
		INNER JOIN Podcast ON seas_PodcastID=podc_PodcastID
		WHERE epis_Status='A' AND epis_SeasonID = '".$SeasonID."'";
		$s.="ORDER BY epis_PlayDate DESC LIMIT 0, 20";
		$q = $BD->query($s);
		$t = $q->num_rows;
		if($t>0){
			$Message['Episodes']=array();
			while($r=$q->fetch_array()) {
				$row["PodcastID"] = $r['podc_PodcastID'];
				$row["EpisodeID"] = $r['epis_EpisodeID'];
				$row["SeasonID"] = $r['epis_SeasonID'];
				$row["PodcastTitle"] = $r['podc_Title'];
				$row["SeasonTitle"] = $r['seas_Title'];
				$row["EpisodeTitle"] = $r['epis_Title'];
				$row["Description"] = $r['epis_Description'];
				$row["Length"] = $r['epis_Length'];
				//$row["Track"] = 'https://www.convoynetwork.com/convoy-webapp/audio-podcast/'.$r['epis_TrackURL'];
                $row['Track'] = 'https://www.convoynetwork.com/convoy-webapp/audio-podcast/'.$r['podc_PodcastID'].'/'.$r['epis_SeasonID'].'/'.$r['epis_TrackURL'];
				$row["ImageURL"] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$r['podc_PodcastID'].'/'.$r['epis_SeasonID'].'/'.$r['epis_Image'];
				$row["Likes"] = $r['NoLikes'];
				$row["UserLike"] = $r['UserLike'];
				$row["ShareURL"] = "https://convoynetwork.com/convoy-webapp/track?id=".$r['epis_EpisodeID'];
				$row["PublishDate"] = $r['epis_PlayDate'];
				array_push($Message['Episodes'], $row);
				}
			}
		else{
			$Message['AppStatus']='0';
			$Message['AppResponse']="No hay resultados.";
			}
    return $Message;
	//print_r(json_encode($Message, JSON_PRETTY_PRINT));
 	$BD->close();
 	}



function SearchPodcast($Podcast) {
	$BD=ConnectDB();
	$Message=array();
		$s="
		SELECT 
			podc_PodcastID,
			podc_Title,
			podc_Description,
			podc_Season,
			podc_Image,
			podc_PublishDate,
			podc_Status,
			(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='P' AND like_ObjectID=podc_PodcastID) AS NoLikes
		FROM Podcast
		LEFT OUTER JOIN Categories ON cate_CategoryID=podc_CategoryID
		WHERE podc_Status=0 AND podc_Title LIKE '%".$Podcast."%' ORDER BY podc_PodcastID DESC LIMIT 0, 20";
		$q=$BD->query($s); 
		$t = $q->num_rows;
		if($t>0){
			$Message['AppStatus']=1;
			$Message['Podcasts']=array();
			while($r=$q->fetch_array()) {
				$row["PodcastID"] = $r['podc_PodcastID'];
				$row["PodcastTitle"]=utf8_encode($r['podc_Title']);
				$row["Description"]=utf8_encode($r['podc_Description']);
				$row["Season"]=utf8_encode($r['podc_Season']);
				$row["ImageURL"] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$r['podc_Image'];
				$row["Seasons"] = $r['NoSeasons'];
				$row["Likes"] = $r['NoLikes'];
				$row["ShareURL"] = "https://convoynetwork.com/convoy-webapp/podcast?id_podcast=".$r['podc_PodcastID'];
				$row["PublishDate"] = $r['podc_PublishDate'];
				$row["PodcastStatus"]= $r['podc_Status'];
				$row["CategoryID"]= $r['podc_CategoryID'];
				$row["Category"]= $r['cate_Category'];
				array_push($Message['Podcasts'], $row);
				}
			}
		else{
			$Message['AppStatus']=0;
			$Message['AppResponse']='No hay resultados';
			}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
 	$BD->close();
	}



function EpisodeDetail($EpisodeID, $UserID) {
	$BD = ConnectDB();
	$Message=array();
	$s = "
	SELECT
		epis_EpisodeID,
		epis_SeasonID,
		epis_Episode,
		epis_Title,
		epis_Description,
		(SELECT seas_Image FROM Podcast_Season WHERE seas_SeasonID = epis_SeasonID  ) AS epis_Image,
		epis_TrackURL,
		epis_Length,
		epis_PlayDate,
		epis_Creation,
		epis_Status,
        (SELECT seas_PodcastID FROM Podcast_Season WHERE seas_SeasonID = epis_SeasonID) AS PodcastID,
		(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='E' AND like_ObjectID=epis_EpisodeID) AS NoLikes,
		(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='E' AND like_ObjectID=epis_EpisodeID AND like_UserID='".$UserID."') AS UserLike
	FROM Podcast_Episodes WHERE epis_EpisodeID='".$EpisodeID."'";
	$q = $BD->query($s);
	$t = $q->num_rows;
	if($t>0){
		$Message['AppStatus'] = '1';
		$r = $q->fetch_array();
			$Message['PodcastID'] = $r['epis_EpisodeID'];
			$Message['NoEpisode'] = $r['epis_Episode'];
			$Message['EpisodeTitle'] = $r['epis_Title'];
			$Message['Description'] = $r['epis_Description'];
			$Message['ImageURL'] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$r['PodcastID'].'/'.$r['epis_SeasonID'].'/'.$r['epis_Image'];
			//$Message['Track2'] = 'https://www.convoynetwork.com/convoy-webapp/audio-podcast/'.$r['epis_TrackURL'];
            $Message['Track'] = 'https://www.convoynetwork.com/convoy-webapp/audio-podcast/'.$r['PodcastID'].'/'.$r['epis_SeasonID'].'/'.$r['epis_TrackURL'];
			$Message['Length'] = $r['epis_Length'];
			$Message['Likes'] = $r['NoLikes'];
			$Message['UserLike'] = $r['UserLike'];
			$Message['PlayDate'] = $r['epis_PlayDate'];
			$Message['Creation'] = $r['epis_Creation'];
			$Message['EpisodeStatus'] = $r['epis_Status'];
		}
	else{
		$Message['AppStatus'] = '0';
		$Message['AppResponse'] = 'No existe el episodio';
		}
    return $Message;
    //print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$BD->close();
	}
?>