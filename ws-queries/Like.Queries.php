<?php 
date_default_timezone_set("America/Mexico_City");

    
function ConnDB() {
	$db_host    ="localhost";
	$db_usuario ="convoyne_convoy";
	$db_password="]ADFToZC!PMV";
	$db_dbname  ="convoyne_convoy";
	$conexion_= new mysqli($db_host, $db_usuario, $db_password, $db_dbname);
	if (mysqli_connect_errno()){
		printf("Falló la conexión: %s\n", mysqli_connect_error());
		exit();
		}
	return $conexion_;
	}

function AddLike($UserID, $ObjectType, $ObjectID){
	$Message = array();
	$DB=ConnDB();
	if($UserID!='' &&  $ObjectType!='' && $ObjectID!=''){
		$sC = "
		SELECT
			like_LikeID,
			like_UserID,
			like_ObjectType,
			like_ObjectID,
			like_Datetime
		FROM Likes 
		WHERE 
			like_UserID='".$UserID."' AND 
			like_ObjectType='".$ObjectType."' AND 
			like_ObjectID='".$ObjectID."'";
		$qC = $DB->query($sC);
		$tC = $qC->num_rows;
		if($tC>0){
			$fL=$qC->fetch_array();
			$s = "DELETE FROM Likes WHERE like_UserID='".$UserID."' AND like_ObjectType='".$ObjectType."' AND like_ObjectID='".$ObjectID."'";
			$q = $DB->query($s);
			$t = $DB->affected_rows;
			if($t>0){
				$Message['AppStatus'] = '1';
				$Message['AppResponseBinary'] = "0";
				$Message['AppResponse'] = "Ya no te gusta";
				}
			else{
				$Message['AppStatus'] = '0';
				$Message['AppResponse'] = "Ocurrió un error, inténtalo nuevamente.";
				}
			}
		else{
			$s="INSERT INTO Likes (like_UserID, like_ObjectType, like_ObjectID, like_Datetime) VALUES ('".$UserID."', '".$ObjectType."', '".$ObjectID."', NOW())";
			$q = $DB->query($s);
			$t = $DB->affected_rows;
			$InsertedID = $DB->insert_id;
			if($t>0){
				$Message['AppStatus'] = '1';
				$Message['AppResponseBinary'] = "1";
				$Message['AppResponse'] = "Te gusta";
				}
			else{
				$Message['AppStatus'] = '1';
				$Message['AppResponse'] = "Ocurrió un error, inténtalo nuevamente.";
				}
			}
		$sO = "
		SELECT
			like_LikeID
		FROM Likes 
		WHERE 
			like_ObjectType='".$ObjectType."' AND 
			like_ObjectID='".$ObjectID."'";
		$qO = $DB->query($sO);
		$tO = $qO->num_rows;
		$Message['TotalLikes'] = "'".$tO."'";
		}
	else{
		$Message['AppStatus'] = '2';
		$Message['AppResponse'] = "Ocurrió un error, inténtalo nuevamente.";
        
        $Message['UserID'] = $UserID;
        $Message['ObjectType'] = $ObjectType;
        $Message['ObjectID'] = $ObjectID;
		}
		
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}

	function NewsCommon($followerId, $profileId)
	{
		$userLoggedNewsArr = getActivity($followerId, 'news');
		$profileNewsArr = getActivity($profileId, 'news');

		foreach ($profileNewsArr as $key => $value) {
            $isCommon = "0";
			foreach ($userLoggedNewsArr as $element) {
				if ($element['NewsID'] == $value['NewsID'])
                    $isCommon = "1";
                
			}

			$profileNewsArr[$key]['Common'] = $isCommon;
		}
        
        if($profileNewsArr == null){
            $profileNewsArr = array();
        }

		return $profileNewsArr;
	}



	function EpisodesCommon($followerId, $profileId)
	{
		$userLoggedNewsArr = getActivity($followerId, 'episodes');
		$profileNewsArr = getActivity($profileId, 'episodes');

		// die(var_dump($userLoggedNewsArr, $profileNewsArr));

		foreach ($profileNewsArr as $key => $value) {
            $isCommon = "0";

            foreach ($userLoggedNewsArr as $element) {
                if ($element['EpisodeID'] == $value['EpisodeID'])
                    $isCommon = "1";
                
			}

			$profileNewsArr[$key]['Common'] = $isCommon;
		}
        if($profileNewsArr == null){
            $profileNewsArr = array();
        }
		return $profileNewsArr;
	}

    
    
	function RadioCommon($followerId, $profileId)
	{
		

		$userLoggedNewsArr = getActivity($followerId, 'radio');
		$profileNewsArr = getActivity($profileId, 'radio');

		// die(var_dump($userLoggedNewsArr, $profileNewsArr));

		foreach ($profileNewsArr as $key => $value) {
            $isCommon = "0";
			foreach ($userLoggedNewsArr as $element) {
                
                if ($element['RadioID'] == $value['RadioID'])
                    $isCommon = "1";
                
			}

			$profileNewsArr[$key]['Common'] = $isCommon;
		}
        if($profileNewsArr == null){
            $profileNewsArr = array();
        }
		return $profileNewsArr;
	}

    function NewsActivity($userId)
    {
        //$userIds = array($followerId, $profileId);
        $DB = ConnectDB();
        $newsInfo = array();
        empty($newsInfo);
            
            $s = "SELECT ".
            "n.news_NewsID as NewsID, ".
            "n.news_CategoryID as CategoryID, ".
            "CONCAT('Noticia: ',n.news_Title) as Title, ".
            "CONCAT('https://www.convoynetwork.com/convoy-webapp/img/img-noticias/',n.news_Image) as Image, ".
            "l.like_Datetime as Date, " .
            "n.news_Brief as Brief,".
            "FROM News AS n " .
            "INNER JOIN Likes AS l ON l.like_ObjectID = n.news_NewsID " .
            "WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'N';";
            
            $q = $DB->query($s);
            $t = $q->num_rows;
            
            if($t > 0)
            {
                while($r = $q->fetch_array(MYSQLI_ASSOC))
                {
                    $newsInfo[$userId][] = $r;
                }
            }

        $DB->close();
        
        $profileNewsArr = isset($newsInfo[$userId]) ? $newsInfo[$userId] : null;
        
        if($profileNewsArr == null){
            $profileNewsArr = array();
        }
        
        return $profileNewsArr;
    }
    
    
    
    function EpisodesActivity($userId)
    {
        $DB = ConnectDB();
        $podcastInfo = array();
        empty($podcastInfo);


            $s = "SELECT ".
            "e.epis_EpisodeID as EpisodeID, ".
            "e.epis_Title as EpisodeTitle, ".
            "e.epis_Description as EpisodeDescription, ".
            "e.epis_Length as EpisodeLength, ".
            "e.epis_PlayDate as PublishDate, ".
            "CONCAT('https://www.convoynetwork.com/convoy-webapp/podcasts-seasons?P=',e.epis_EpisodeID) as ShareURL,".
            "CONCAT('https://www.convoynetwork.com/convoy-webapp/audio-podcast/',p.podc_PodcastID,'/',e.epis_SeasonID,'/',e.epis_TrackURL) as TrackURL, ".
            "e.epis_SeasonID as SeasonID, ".
            "s.seas_Title as SeasonTitle, ".
            "s.seas_Description as SeasonDescription, ".
            "p.podc_PodcastID as PodcastID, ".
            "p.podc_Title as PodcastTitle, ".
            "(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='E' AND like_ObjectID=e.epis_EpisodeID) AS Likes,".
            "CONCAT('Episodio: ',e.epis_Title) as Title, ".
            "s.seas_Title as SeasonTitle, ".
            "l.like_Datetime as Date, " .
            "CONCAT('https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/',s.seas_PodcastID,'/', s.seas_SeasonID,'/',s.seas_Image) as Image FROM Podcast_Episodes AS e " .
            "INNER JOIN Podcast_Season AS s ON s.seas_SeasonID = e.epis_SeasonID " .
            "INNER JOIN Podcast AS p ON p.podc_PodcastID = s.seas_PodcastID " .
            "INNER JOIN Likes AS l ON l.like_ObjectID = e.epis_EpisodeID " .
            "WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'E';";
            
            $q = $DB->query($s);
            $t = $q->num_rows;
            
            if($t > 0)
            {
                while($r = $q->fetch_array(MYSQLI_ASSOC))
                {
                    $podcastInfo[$userId][] = $r;
                }
            }
        
        $DB->close();
        
        $profileNewsArr = isset($podcastInfo[$userId]) ? $podcastInfo[$userId] : null;
        
        
        if($profileNewsArr == null){
            $profileNewsArr = array();
        }
        return $profileNewsArr;
    }
    
    
    
    function RadioActivity($userId)
    {
        $DB = ConnectDB();

            
            $s = "SELECT ".
            "r.radi_RadioID as RadioID, ".
            "CONCAT('Radio: ',r.radi_Title) as Title, ".
            "r.radi_Description as RadioDescription, ".
            "r.radi_IniDatetime as RadioIniDatetime, ".
            "r.radi_EndDatetime as RadioEndDatetime, ".
            "CONCAT('https://www.convoynetwork.com/convoy-webapp/&fecha_tiempo=',DATE(r.radi_IniDatetime)) as ShareURL,".
            "(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='R' AND like_ObjectID=r.radi_RadioID) AS RadioLikes, ".
            "CONCAT('https://www.convoynetwork.com/convoy-webapp/audio-radio/',r.radi_Track) as RadioTrack, ".
            "l.like_Datetime as Date, " .
            "CONCAT('https://www.convoynetwork.com/convoy-webapp/img/cover-radio/',r.radi_Image) as Image ".
            "FROM `Radio` AS r " .
            "INNER JOIN Likes AS l ON l.like_ObjectID = r.radi_RadioID " .
            "WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'R';";
            
            $q = $DB->query($s);
            $t = $q->num_rows;
            
            if($t > 0)
            {
                while($r = $q->fetch_array(MYSQLI_ASSOC))
                {
                    $radioInfo[$userId][] = $r;
                }
            }

        $DB->close();
        
        $profileNewsArr = isset($radioInfo[$userId]) ? $radioInfo[$userId] : null;
        

        if($profileNewsArr == null){
            $profileNewsArr = array();
        }
        return $profileNewsArr;
    }

    function getActivity($userId, $table)
    {
    	$DB = ConnectDB();
		$returnInfo = array();
        empty($returnInfo);

    	switch ($table) {
    		case 'news':
    			$s = "SELECT n.news_NewsID as NewsID,n.news_CategoryID as CategoryID,n.news_Title as Title, l.like_Datetime as Date, " .
				"n.news_Brief as Brief, ".
                "CONCAT('https://www.convoynetwork.com/convoy-webapp/img/img-noticias/',n.news_Image) as Image ".
				"FROM News AS n " .
				"INNER JOIN Likes AS l ON l.like_ObjectID = n.news_NewsID " .
				"WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'N';";
				break;
    		case 'episodes':
    			/*$s = "SELECT p.podc_PodcastID as PodcastID,p.podc_Title as Title, l.like_Datetime as Date, " .
				"p.podc_Image as Image FROM Podcast AS p " .
				"INNER JOIN Likes AS l ON l.like_ObjectID = p.podc_PodcastID " .
				"WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'E';";*/
	            /*$s = "SELECT ".
                "e.epis_EpisodeID as EpisodeID, ".
                "e.epis_SeasonID as SeasonID, ".
                "e.epis_Title as Title, ".
                "l.like_Datetime as Date, " .
	            "CONCAT('https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/',s.seas_PodcastID,'/', s.seas_SeasonID,'/',s.seas_Image) as Image, ".
                "s.seas_Title as SeasonTitle ".
                "FROM Podcast_Episodes AS e " .
	            "INNER JOIN Podcast_Season AS s ON s.seas_SeasonID = e.epis_SeasonID " .
	            "INNER JOIN Likes AS l ON l.like_ObjectID = e.epis_EpisodeID " .
	            "WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'E';";*/
                $s = "SELECT ".
                "e.epis_EpisodeID as EpisodeID, ".
                "e.epis_Title as EpisodeTitle, ".
                "e.epis_Description as EpisodeDescription, ".
                "e.epis_Length as EpisodeLength, ".
                "e.epis_PlayDate as PublishDate, ".
                "CONCAT('https://www.convoynetwork.com/convoy-webapp/podcasts-seasons?P=',e.epis_EpisodeID) as ShareURL,".
                "CONCAT('https://www.convoynetwork.com/convoy-webapp/audio-podcast/',p.podc_PodcastID,'/',e.epis_SeasonID,'/',e.epis_TrackURL) as TrackURL, ".
                "e.epis_SeasonID as SeasonID, ".
                "s.seas_Title as SeasonTitle, ".
                "s.seas_Description as SeasonDescription, ".
                "p.podc_PodcastID as PodcastID, ".
                "p.podc_Title as PodcastTitle, ".
                "(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='E' AND like_ObjectID=e.epis_EpisodeID) AS Likes,".
                "CONCAT('Episodio: ',e.epis_Title) as Title, ".
                "s.seas_Title as SeasonTitle, ".
                "l.like_Datetime as Date, " .
                "CONCAT('https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/',s.seas_PodcastID,'/', s.seas_SeasonID,'/',s.seas_Image) as Image FROM Podcast_Episodes AS e " .
                "INNER JOIN Podcast_Season AS s ON s.seas_SeasonID = e.epis_SeasonID " .
                "INNER JOIN Podcast AS p ON p.podc_PodcastID = s.seas_PodcastID " .
                "INNER JOIN Likes AS l ON l.like_ObjectID = e.epis_EpisodeID " .
                "WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'E';";
    			break;
    		case 'followed':
    			$s = "SELECT u.user_Userid as UserID, u.user_Login as Username, CONCAT('https://www.convoynetwork.com/convoy-webapp/image-avatar/',u.user_Userid,'/',u.user_Avatar) as Avatar, u.user_Description as Description,
	            u.user_Website as Website, f.foll_Datetime as FollowDate FROM Users AS u
	            INNER JOIN Followers AS f ON f.foll_UserID1 = u.user_Userid
	            WHERE f.foll_UserID2 = " . $userId . ";";
    			break;
    		case 'follows':
    			$s = "SELECT u.user_Userid as UserID, u.user_Login as Username, CONCAT('https://www.convoynetwork.com/convoy-webapp/image-avatar/',u.user_Userid,'/',u.user_Avatar) as Avatar, u.user_Description as Description,
				u.user_Website as Website, f.foll_Datetime as FollowDate FROM Users AS u
				INNER JOIN Followers AS f ON f.foll_UserID2 = u.user_Userid 
				WHERE f.foll_UserID1 = " . $userId . ";";
    			break;
    		case 'radio':
                $s = "SELECT ".
                "r.radi_RadioID as RadioID, ".
                "CONCAT('Radio: ',r.radi_Title) as Title, ".
                "r.radi_Description as RadioDescription, ".
                "r.radi_IniDatetime as RadioIniDatetime, ".
                "r.radi_EndDatetime as RadioEndDatetime, ".
                "CONCAT('https://www.convoynetwork.com/convoy-webapp/&fecha_tiempo=',DATE(r.radi_IniDatetime)) as ShareURL,".
                "(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='R' AND like_ObjectID=r.radi_RadioID) AS RadioLikes, ".
                "CONCAT('https://www.convoynetwork.com/convoy-webapp/audio-radio/',r.radi_Track) as RadioTrack, ".
                "l.like_Datetime as Date, " .
                "CONCAT('https://www.convoynetwork.com/convoy-webapp/img/cover-radio/',r.radi_Image) as Image ".
                "FROM `Radio` AS r " .
                "INNER JOIN Likes AS l ON l.like_ObjectID = r.radi_RadioID " .
                "WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'R';";
                /*$s = "SELECT ".
                "r.radi_RadioID as RadioID, ".
                "r.radi_Title as Title, ".
                "l.like_Datetime as `Date`, " .
	            "CONCAT('https://www.convoynetwork.com/convoy-webapp/img/cover-radio/',r.radi_Image) as Image FROM `Radio` AS r " .
	            "INNER JOIN Likes AS l ON l.like_ObjectID = r.radi_RadioID " .
	            "WHERE l.like_UserID = " . $userId . " AND l.like_ObjectType = 'R';";*/
    			break;
    		default:
    			return false;
    			break;
    	}

    	$q = $DB->query($s);
		$t = $q->num_rows;

		if($t > 0)
		{
			while($r = $q->fetch_array(MYSQLI_ASSOC))
			{
				$returnInfo[] = $r;
			}
		}

		return $returnInfo;
    }

    function getAllFollowedCommons($UserID, $FollowID)
    {
    	$followersCommons = FollowersCommon($UserID, $FollowID);
	    $followingCommons = FollowingCommon($UserID, $FollowID);
		$newsCommon = NewsCommon($UserID, $FollowID);
		$episodesCommon = EpisodesCommon($UserID, $FollowID);
	    $radioCommon = RadioCommon($UserID, $FollowID);

	    $arResp = array( 
			'follows' => $followersCommons,
	        'followed' => $followingCommons,
			'news' => $newsCommon,
			'episodes' => $episodesCommon,
	        'radio' => $radioCommon
		);

		return $arResp;
    }
	    
?>