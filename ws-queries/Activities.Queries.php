<?php
require_once("function.ConnDB.php"); 
require_once("function.General.php"); 

function ShowActivities($UserID, $Datetime){
	$DB=ConnectDB();
	$Message = array();

	if($UserID!='' && $Datetime!=''){
	//QUERY FOLLOWERS
		$sF = "SELECT foll_UserID2 FROM Followers WHERE foll_UserID1='".$UserID."'";
		$qF = $DB->query($sF);
		$Following = '';
		while($rF = $qF->fetch_array()){
			$Following.= $rF['foll_UserID2'].', ';
			}
	$Following.= "'".$UserID."'";
        $UserViewingID;
//		$Following = substr_replace($Following, "", -2);
	
	
	//QUERY ACTIVITY
		$s = "
			(
			
			SELECT
			like_ObjectType AS Type,
			like_ObjectID AS ID,
			(
				CASE like_ObjectType
					WHEN 'P' THEN
						(SELECT CONCAT(user_Login,' le ha gustado el podcast ',podc_Title,'Y') FROM Podcast WHERE podc_PodcastID=like_ObjectID)
					WHEN 'S' THEN
						(SELECT CONCAT(user_Login,' le ha gustado la temporada ',seas_Title,' del podcast ', podc_Title) FROM Podcast_Season INNER JOIN Podcast ON podc_PodcastID=seas_PodcastID WHERE seas_SeasonID=like_ObjectID)
					WHEN 'E' THEN
						(SELECT CONCAT(user_Login,' le ha gustado el episodio ',epis_Title,' de la temporada ',seas_Title,' del podcast', podc_Title) FROM Podcast_Episodes INNER JOIN Podcast_Season ON seas_SeasonID=epis_SeasonID INNER JOIN Podcast ON podc_PodcastID=seas_PodcastID WHERE epis_EpisodeID=like_ObjectID)
					WHEN 'R' THEN
						(SELECT CONCAT(user_Login,' le ha gustado el programa de radio ',radi_Title,'Y') FROM Radio WHERE radi_RadioID=like_ObjectID)
					WHEN 'N' THEN
						(SELECT CONCAT(user_Login,' le ha gustado la noticia ',news_Title,'Y') FROM News WHERE news_NewsID=like_ObjectID)
				END
			) AS Title,
			like_Datetime AS Datetime
			FROM Likes 
			INNER JOIN Users ON like_UserID=user_Userid
			WHERE 
				like_Datetime BETWEEN '2015-12-29 00:00:00' AND NOW()
				AND like_UserID IN (".$Following.")
			)
			UNION
			(
			SELECT 
			'F' AS Type,
			foll_FollowerID AS ID,
			CONCAT(a.user_Login,' ahora sigue a ',b.user_Login) AS Title,
			foll_Datetime AS Datetime
			FROM Followers
			INNER JOIN Users AS a ON foll_UserID1=a.user_Userid
			INNER JOIN Users AS b ON foll_UserID2=b.user_Userid
			
			WHERE foll_Datetime BETWEEN '2015-12-29 00:00:00' AND NOW()
			AND foll_UserID1 IN (".$Following.")
			)
        
			ORDER BY Datetime DESC
			;
			";
		$q = $DB->query($s); 
		$t = $q->num_rows;
		if($t>0){
			$Message['AppStatus'] = '1';
			$Message['Activities'] = array();
			while($r = $q->fetch_array()){
				$row['ObjectType'] = $r['Type'];
				$row['ObjectID'] = $r['ID'];
				$row['Title'] = $r['Title'];
				$row['Datetime'] = $r['Datetime'];
				array_push($Message['Activities'], $row);
				}
			}
		else{
			$Message['AppStatus'] = '0';
			$Message['AppResponse'] = 'No hay actividad';
			}
		}
	else{
		$Message['AppStatus'] = '0';
		$Message['AppResponse'] = 'No hay actividad';
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	}
?>