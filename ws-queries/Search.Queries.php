<?php
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');
// require_once('Notification.Queries.php');


function Search($UserID, $Search, $Type){
	$DB=ConnectDB();
	$Message=array();
	if($Search!=''){
        if($Type == "U"){
            $s = "
                SELECT 
                    user_Userid,
                    user_Login,
                    user_Genre,
                    user_Avatar,
                    /*user_FullName,*/
        
                    (SELECT COUNT(*) FROM Followers WHERE foll_UserID1=".$UserID." AND foll_UserID2=user_Userid) AS Following,
                    (SELECT COUNT(*) FROM Followers WHERE foll_UserID2=".$UserID." AND foll_UserID1=user_Userid) AS Followers
                FROM 
                    Users
                WHERE 
                    user_Login LIKE '%".$Search."%'
                    AND user_Status IN ('0', '1')
                    AND user_Userid != '".$UserID."'";
            $q = $DB->query($s);
            $t=$q->num_rows;
            if ($t>0){
                $Message['AppStatus']=1;
                $Message['Usuarios']=array();
                
                while($r=$q->fetch_array()) {
                    $row['UserID'] = $r['user_Userid'];
                    $row['Login'] = $r['user_Login'];
                    //$row['Fullname'] = $r['user_FullName'];
                    $row['Genre'] = $r['user_Genre'];
                    if($r['user_Avatar']!=''){
                        $row['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'.$r['user_Userid'].'/'.$r['user_Avatar'];
                        }
                    else{
                        $row['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.png';
                        }
                    $row['Followers'] = $r['Followers'];
                    $row['Following'] = $r['Following'];
                    array_push($Message['Usuarios'], $row);
                    }
            }
            else{
                $Message['AppStatus']='0';
                $Message['AppResponse']='No hay resultados';
                $Message['Usuarios']=array();
                }
        }//if search is Users
        else if($Type == "C" ){
            
            $s = "
            (SELECT
                news_NewsID as MainID,
                /*null as otherID, /* null for support */
                news_Title as s_Title,
                news_Brief as s_Desc,
                news_Image as s_URL,
                news_Datetime as s_Date,
                'N' as s_Type
             FROM
                News

             WHERE
                news_Title LIKE '%".$Search."%'
             OR
                news_Brief LIKE '%".$Search."%'
             OR
                news_Content LIKE '%".$Search."%'
            )
            UNION ALL
            (SELECT
                podc_PodcastID as MainID,
                /*null as otherID, /* null for support */
                podc_Title as s_Title,
                podc_Description as s_Desc,
                podc_Image as s_URL,
                podc_Creation as s_Date,
                'P' as s_Type
            FROM
                Podcast
            WHERE
                podc_Title LIKE '%".$Search."%'
            OR
                podc_Description LIKE '%".$Search."%'
            AND
                podc_Status = 'A')
            ORDER BY
            s_Date DESC 
            /* UNION
            (SELECT
                seas_SeasonID as MainID,
                seas_PodcastID as otherID,
                seas_Title as s_Title,
                seas_Description as s_Desc,
                seas_Image as s_URL,
                seas_Creation as s_Date,
                'S' as s_Type
            FROM
                Podcast_Season
            WHERE
                seas_Title LIKE '%".$Search."%'
            OR
                seas_Description LIKE '%".$Search."%'
            AND
                seas_Status = 'A')
            UNION
            (SELECT
                epis_EpisodeID as MainID,
                epis_SeasonID  as otherID,
                epis_Title as s_Title,
                epis_Description as s_Desc,
                epis_TrackURL as s_URL,
                epis_Creation as s_Date,
                'E' as s_Type
             FROM
                Podcast_Episodes
             WHERE
                epis_Title LIKE '%".$Search."%'
             OR
                epis_Description LIKE '%".$Search."%'
             AND
                epis_Status = 'A') */
            ";
            $q = $DB->query($s);
            $t=$q->num_rows;
            if ($t>0){
                $Message['AppStatus']='1';
                $Message['Search']=array();

                while($r=$q->fetch_array()) {
                    if($r['s_Type'] == "P"){
                        $row['Type'] = $r['s_Type'];
                        $row['ID'] = $r['MainID'];
                        $row['Title'] = $r['s_Title'];
                        $row['Description'] = $r['s_Desc'];
                        $row['Image'] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$r['MainID'].'/'.$r['s_URL'];
                        $row['Date'] = $r['s_Date'];
                    }
                    else if($r['s_Type'] == "N"){
                        $row['Type'] = $r['s_Type'];
                        $row['ID'] = $r['MainID'];
                        $row['Title'] = $r['s_Title'];
                        $row['Description'] = $r['s_Desc'];
                        $row['Image'] = 'https://www.convoynetwork.com/convoy-webapp/img/img-noticias/'.$r['s_URL'];
                        $row['Date'] = $r['s_Date'];
                    }
                    /*
                    else if($r['s_Type'] == "S"){
                        $row['Type'] = $r['s_Type'];
                        $row['ID'] = $r['MainID'];
                        $row['PodcastID'] = $r['otherID'];
                        $row['Title'] = $r['s_Title'];
                        $row['Description'] = $r['s_Desc'];
                        $row['Image'] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$r['otherID'].'/'. $r['MainID']. '/'.$r['s_URL'];
                        $row['Date'] = $r['s_Date'];
                    }
                    else if($r['s_Type'] == "E"){
                        $row['Type'] = $r['s_Type'];
                        $row['ID'] = $r['MainID'];
                        $row['PodcastID'] = $r['otherID'];
                        $row['Title'] = $r['s_Title'];
                        $row['Description'] = $r['s_Desc'];
                        $row['Image'] = 'https://www.convoynetwork.com/convoy-webapp/audio-podcast/'.$r['s_URL'];
                        $row['Date'] = $r['s_Date'];
                    }
                    */
                    
                    
                    array_push($Message['Search'], $row);
                }//while
            }
            else{
                $Message['AppStatus']='0';
                $Message['AppResponse']='No hay resultados';
                $Message['Search']=array();
            }
        }
        
    }// if search is null
	else{
		$Message['AppStatus']=0;
		$Message['mensaje']='Ingrese un termino de búsqueda.';
		}

	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}
