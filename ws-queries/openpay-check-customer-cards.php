<?php
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
    $Headers['Apikey']='';
    }
if(ApiKeyString($Headers['Apikey'])==true){
    require_once("ws-queries/Openpay.Queries.php");
    $handle = fopen('php://input','r');
    $jsonInput = fgets($handle);
    // Decoding JSON into an Array
    $jsonArray = json_decode($jsonInput,true);
    $UserID = $jsonArray['UserID'];
    $openpay = new OpenPayDecorator();
	$result = $openpay->getCustomerCards($UserID);
	print_r(json_encode($result, JSON_PRETTY_PRINT));
} else {
    echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
}