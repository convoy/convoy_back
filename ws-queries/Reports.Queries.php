<?php
require_once("function.ConnDB.php"); 
require_once("function.General.php"); 
function ReportIncident($UserID, $ReportType, $Report){
	$BD=ConnectDB();
	$Message=array();
	if($Report!=''){
		$sql = 'SELECT user_Login as Username, user_Email as Email ' .
			   'FROM Users WHERE user_Userid = ' . $UserID . ' LIMIT 1;';
		$qry = $BD->query($sql);
		$total = $qry->num_rows;

		if($total > 0)
		{
			$userInfo = $qry->fetch_array(MYSQLI_ASSOC);
		}


		$s = "INSERT INTO Reports (repo_UserID, repo_Type, repo_Content, repo_Datetime, repo_IPAddress) VALUES (".$UserID.", '".$ReportType."', '".$Report."', NOW(), '".$_SERVER['REMOTE_ADDR']."')";
		$q = $BD->query($s);
		$Message['AppStatus'] = "1";
		$Message['AppResponse'] = "Recibimos tu reporte ¡Gracias!";
        
    } else {
		$Message['AppStatus'] = "0";
		$Message['AppResponse'] = "Error: su reporte no se envió por falta de información";
        http_response_code(400);//'Bad Request'
        exit;
	}
    
    // prepare email body text

	// $emailTo = 'dominguez.arevalo@gmail.com'; // "karkaim@gmail.com";
    $emailTo = "soporte@convoynetwork.com";
    $emailTo2 = $userInfo['Email'];
    $subject = "Convoy - Reporte";
    
    $userInfo['Report'] = $Report;

    $themail = new Email();
    $sent = $themail->sendMail([$emailTo, $emailTo2], $subject, 'report', $userInfo, "Mensaje de Convoy:");

	if ( is_string($sent) ) {
		error_log("An error ocurred: " . $themail->mail->ErrorInfo, 3, "/var/log/app/my-errors.log");
		$Message['AppStatus'] = "0";
		$Message['AppResponse'] = "Error: su reporte no se ha podido enviar por un error en el servidor";
        http_response_code(500); // Internal server error
		print_r(json_encode($Message, JSON_PRETTY_PRINT));
        exit;
	}
    
    /* // send prepared message
    $sent = mail($emailTo, $subject, $body);*/
    
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	}

?>