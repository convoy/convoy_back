<?php
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');


function UserStatus($Status){
	switch($Status){
		case 0: $Description = 'Inactivo, sin datos de tarjeta'; break;
		case 1: $Description = 'Activo'; break;
		case 2: $Description = 'Suspendido, tarjeta sin fondos'; break;
		case 3: $Description = 'Cancelado, suscripción cancelada por el usuario'; break;
		}
	return $Description;
	}





function DownloadList($UserID){
	$BD=ConnectDB();
	$Message=array();
	$sU="SELECT user_Userid, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_Userid='".$UserID."'";
	$qU=$BD->query($sU);
	$rU=$qU->fetch_array();
	$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
	$Today = date('Y-m-d');
	if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){

		$s = "
		SELECT
			epis_EpisodeID AS EpisodeID,
			epis_Title AS EpisodeTitle,
            epis_TrackURL AS TrackURL,
            seas_Image AS EpImage,
            epis_Description AS EpDesc,
            epis_Length AS EpLength,
            epis_PlayDate AS EpPlay,
			seas_SeasonID AS SeasonID,
			seas_Title AS SeasonTitle,
			podc_PodcastID AS PodcastID,
			podc_Title AS PodcastTitle,
			user_Login,
			/*user_FullName AS UserFullname,*/
			down_DownloadID,
			down_EpisodeID,
			down_UserID,
			down_Datetime
		FROM Downloads
			INNER JOIN Users ON user_Userid=down_UserID
			INNER JOIN Podcast_Episodes ON epis_EpisodeID=down_EpisodeID
			INNER JOIN Podcast_Season ON seas_SeasonID=epis_SeasonID
			INNER JOIN Podcast on podc_PodcastID=seas_PodcastID
		WHERE down_UserID='".$UserID."'
		";
		$q = $BD->query($s);
		$t = $q->num_rows;
		if($t>0){
            $Message['AppStatus']='1';
            $Message['UserID']=$rU['user_Userid'];
            $Message['UserStatus']='1';
            //$Message['UserLogin'] = $r['user_Login'];
            //$Message['UserStatusDescription']=UserStatus($rU['user_Status']);
			$Message['Downloads']=array();
			while($r=$q->fetch_array()) {
                $row['DownloadID'] = $r['down_DownloadID'];
                $row['DownloadDatetime'] = $r['down_Datetime'];
                $row['PodcastID'] = $r['PodcastID'];
                $row['PodcastTitle'] = $r['PodcastTitle'];
				$row['SeasonID'] = $r['SeasonID'];
				$row['SeasonTitle'] = $r['SeasonTitle'];
                $row['ID'] = $r['EpisodeID'];
                $row['Title'] = $r['EpisodeTitle'];
                $row['EpisodeTitle'] = $r['EpisodeTitle'];
                $row['Description'] = $r['EpDesc'];
                $row['ImageURL'] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$r['PodcastID'].'/'.$r['SeasonID'].'/'.$r['EpImage'];
                $row['Image'] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-podcast/'.$r['PodcastID'].'/'.$r['SeasonID'].'/'.$r['EpImage'];
                //$row['TrackURL'] = 'https://www.convoynetwork.com/convoy-webapp/audio-podcast/'.$r['TrackURL'];
                $row['TrackURL'] = 'https://www.convoynetwork.com/convoy-webapp/audio-podcast/'.$r['PodcastID'].'/'.$r['SeasonID'].'/'.$r['TrackURL'];
                $row['Track'] = 'https://www.convoynetwork.com/convoy-webapp/audio-podcast/'.$r['PodcastID'].'/'.$r['SeasonID'].'/'.$r['TrackURL'];
                $row['Length'] = $r['EpLength'];
				$row['ShareURL'] = 'https://convoynetwork.com/convoy-webapp/track?id='.$r['epis_EpisodeID'];
				$row['PublishDate'] = $r['EpPlay'];
				
				/*$row['UserFullname'] = $r['UserFullname'];*/
				
				
				array_push($Message['Downloads'], $row);
				}
			}
		else{
			$Message['AppStatus']='0';
			$Message['AppResponse']="No hay resultados.";
            http_response_code(404);//'Not Found'
			}
		}
	else{
		$Message['AppStatus']='2';
		$Message['AppResponse']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
        http_response_code(402);//'Payment required'
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
 	$BD->close();
 	}


function DownloadRegister($UserID, $EpisodeID){
	$BD=ConnectDB();
	$Message=array();
	$sU="SELECT user_Userid, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_Userid='".$UserID."'";
	$qU=$BD->query($sU);
	$rU=$qU->fetch_array();
	$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
	$Today = date('Y-m-d');
	if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
		$Message['AppStatus']='1';
		$Message['UserID']=$rU['user_Userid'];
		$Message['UserStatus']='1';
		$Message['UserStatusDescription']=UserStatus($rU['user_Status']);
		$s = "INSERT INTO Downloads (down_UserID, down_EpisodeID) VALUES ('".$UserID."', '".$EpisodeID."')";
		$q = $BD->query($s);
		$t = $BD->affected_rows;
		if($t>0){
			$Message['AppStatus']='1';
			$Message['AppResponse']="Descarga registrada.";
			}
		else{
			$Message['AppStatus']='0';
			$Message['AppResponse']="No se registró la descarga.";
            http_response_code(400);//'Bad Request'
			}
		}
	else{
		$Message['AppStatus']='2';
		$Message['AppResponse']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
        http_response_code(402);//'Payment required'
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
 	$BD->close();
	}

?>