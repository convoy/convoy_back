<?php
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');

function UserStatus($Status){
	switch($Status){
		case 0: $Description = 'Inactivo, sin datos de tarjeta'; break;
		case 1: $Description = 'Activo'; break;
		case 2: $Description = 'Suspendido, tarjeta sin fondos'; break;
		case 3: $Description = 'Cancelado, suscripción cancelada por el usuario'; break;
		}
	return $Description;
	}


function News_ShowAll($UserID, $LastUpdate){
	$DB = ConnectDB();
	$Message = array();
	$sU = "SELECT user_UserID, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
	$qU = $DB->query($sU);
	$tU = $qU->num_rows;
	if($tU>0){
		$rU=$qU->fetch_array();
		$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
		$Today = date('Y-m-d');
//		if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
			$Message['AppStatus']='1';
			$Message['UserID']=$rU['user_UserID'];
			$Message['UserStatus']='1';
			$Message['UserStatusDescription']=UserStatus($rU['user_Status']);
			$s = "
			SELECT 
				news_NewsID, 
				news_Title, 
				news_Brief, 
				news_Content, 
				news_Image, 
				news_Datetime,
				(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID) AS NoLikes,
				(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID AND like_UserID='".$UserID."') AS UserLike

				FROM News 
				WHERE news_Datetime <=  NOW()
                ORDER BY news_Datetime DESC";
			$q = $DB->query($s);
			$t = $q->num_rows;
			if($t>0){
				$Message['News'] = array();
				while($r = $q->fetch_array()){
					$row['NewsID'] = $r['news_NewsID'];
					$row['Title'] = $r['news_Title'];
					//$row['Brief'] = utf8_decode($r['news_Brief']);
                    //$row['Brief'] = mb_convert_encoding($r['news_Brief'], "UTF-8", mb_detect_encoding($r['news_Brief'], "UTF-8, ISO-8859-1, ISO-8859-15", true));
                    $row['Brief'] = $r['news_Brief'];
					$row['ImageURL'] = 'https://www.convoynetwork.com/convoy-webapp/img/img-noticias/'.$r['news_Image'];
					$row['Date'] = $r['news_Datetime'];
					$row['Likes'] = $r['NoLikes'];
					$row['UserLike'] = $r['UserLike'];
					$row['ShareURL']='https://www.convoynetwork.com/convoy-webapp/noticias?NewsID='.$r['news_NewsID'];
					array_push($Message['News'], $row);
					}
				}
			else{
				$Message['AppStatus']='0';
				$Message['AppResponse']="No hay resultados.";
				}
//			}
//		else{
//			$Message['AppStatus']=2;
//			$Message['AppResponse']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
//			}
		}
	else{
		$Message['AppStatus']='0';
		$Message['AppResponse']="Usuario inválido.";
		}
	//print_r(json_encode($Message, JSON_PRETTY_PRINT));
    return $Message;
	}


function News_Detail($UserID, $NewsID){
	$DB = ConnectDB();
	$Message = array();
	$sU="SELECT user_UserID, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
	$qU=$DB->query($sU);
	$tU = $qU->num_rows;
	if($tU>0){
		$rU=$qU->fetch_array();
		$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
		$Today = date('Y-m-d');
//		if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
			$s = "
			SELECT 
				news_NewsID,
				news_Title,
				news_Brief,
				news_Content,
				news_Image,
				news_Datetime,
				(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID) AS NoLikes,
				(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID AND like_UserID='".$UserID."') AS UserLike
			FROM 
				News 
			WHERE 
				news_NewsID='".$NewsID."'";
			$q = $DB->query($s);
			$r = $q->fetch_array();
			$Message['AppStatus'] = '1';

			$Message['UserID']=$rU['user_UserID'];
			$Message['UserStatus']='1';
			$Message['UserStatusDescription']=UserStatus($rU['user_Status']);

			$Message['NewsID'] = $r['news_NewsID'];
			$Message['Title'] = $r['news_Title'];
			$Message['Brief'] = $r['news_Brief'];
            $addHMTLHeader = "<!DOCTYPE html><html>";
			$Message['Content'] = $r['news_Content'];
            $addHMTLFooter = "</body></html>";
			$Message['ImageURL'] = 'https://www.convoynetwork.com/convoy-webapp/img/img-noticias/'.$r['news_Image'];
			$Message['Date'] = $r['news_Datetime'];
			$Message['Likes'] = $r['NoLikes'];
			$Message['UserLike'] = $r['UserLike'];
			$Message['ShareURL'] = 'https://www.convoynetwork.com/convoy-webapp/noticias?NewsID='.$r['news_NewsID'];
//			}
//		else{
//			$Message['AppStatus']=0;
//			$Message['AppResponse']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
//			}
		}
	else{
		$Message['AppStatus']='0';
		$Message['AppResponse']="Usuario inválido.";
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	}

function News_Detail_HTML($UserID, $NewsID){
    $DB = ConnectDB();
    $Message = array();
    $sU="SELECT user_UserID, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
    $qU=$DB->query($sU);
    $tU = $qU->num_rows;
    if($tU>0){
        $rU=$qU->fetch_array();
        $FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
        $Today = date('Y-m-d');
        //		if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
        $s = "
        SELECT
        news_NewsID,
        news_Title,
        news_Brief,
        news_Content,
        news_Image,
        news_Datetime,
        (SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID) AS NoLikes,
        (SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID AND like_UserID='".$UserID."') AS UserLike
        FROM
        News
        WHERE
        news_NewsID='".$NewsID."'";
        $q = $DB->query($s);
        $r = $q->fetch_array();
        $Message['AppStatus'] = '1';
        
        $Message['UserID']=$rU['user_UserID'];
        $Message['UserStatus']='1';
        $Message['UserStatusDescription']=UserStatus($rU['user_Status']);
        
        $Message['NewsID'] = $r['news_NewsID'];
        //$Message['Title'] = $r['news_Title'];
        //$Message['Brief'] = $r['news_Brief'];

        $date = date_create($r['news_Datetime']);
 
        $Message['ImageURL'] = 'https://www.convoynetwork.com/convoy-webapp/img/img-noticias/'.$r['news_Image'];
        $Message['Date'] = $r['news_Datetime'];
        $Message['Likes'] = $r['NoLikes'];
        $Message['UserLike'] = $r['UserLike'];
        $Message['ShareURL'] = 'https://www.convoynetwork.com/convoy-webapp/noticias?NewsID='.$r['news_NewsID'];
        
        $addHMTLHeader = "<!DOCTYPE html><html><head><meta charset='UTF-8'><style type='text/css'>@font-face{font-family: 'TradeGothicLTStd-BdCn20'; src: url('https://convoynetwork.com/fonts/2F4CF1_0_0.eot?#iefix') format('embedded-opentype'), url('https://convoynetwork.com/fonts/2F4CF1_0_0.woff') format('woff'), url('fonts/2F4CF1_0_0.ttf') format('truetype'), url('https://convoynetwork.com/fonts/2F4CF1_0_0.svg#TradeGothicLTStd-BdCn20') format('svg'); font-weight: normal; font-style: normal;}@font-face{font-family: 'Din'; src: url('https://convoynetwork.com/fonts/dinregular.eot?#iefix') format('embedded-opentype'), url('https://convoynetwork.com/fonts/dinregular.woff') format('woff'), url('fonts/dinregular.ttf') format('truetype'), url('https://convoynetwork.com/fonts/dinregular.svg#Din') format('svg'); font-weight: normal; font-style: normal;}body{background-color:#fff;color:#000;font-family:'Din', Arial, sans-serif;font-size:15px;}h1{font-family:'TradeGothicLTStd-BdCn20', Arial, sans-serif;font-size:2em;text-transform:uppercase;font-weight:normal;letter-spacing:0.01em;padding:0;margin:0;}img{width: 100%;height: auto;}a{color:#000;font-weight:bold;}</style><body><span>". date_format($date, 'M d, y') ."</span><h1>".$r['news_Title']."</h1><img src='". $Message['ImageURL'] ."' />";
        $addHMTLFooter = "</body></html>";
        
        $Message['Content'] = $addHMTLHeader.$r['news_Content'].$addHMTLFooter;
        //			}
        //		else{
        //			$Message['AppStatus']=0;
        //			$Message['AppResponse']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
        //			}
    }
    else{
        $Message['AppStatus']='0';
        $Message['AppResponse']="Usuario inválido.";
    }
    print_r(json_encode($Message, JSON_PRETTY_PRINT));
}


function News_Search($UserID, $Term){
	$DB = ConnectDB();
	$Message = array();
	$sU = "SELECT user_UserID, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
	$qU = $DB->query($sU);
	$tU = $qU->num_rows;
	if($tU>0){
		$rU=$qU->fetch_array();
		$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
		$Today = date('Y-m-d');
//		if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
			$Message['AppStatus']='1';
			$Message['UserID']=$rU['user_UserID'];
			$Message['UserStatus']='1';
			$Message['UserStatusDescription']=UserStatus($rU['user_Status']);
			$s = "
			SELECT 
				news_NewsID, 
				news_Title, 
				news_Brief, 
				news_Content, 
				news_Image, 
				news_Datetime,
				(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID) AS NoLikes,
				(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='N' AND like_ObjectID=news_NewsID AND like_UserID='".$UserID."') AS UserLike
			FROM 
				News 
			WHERE 
				news_Title LIKE '%".$Term."%' OR news_Brief LIKE '%".$Term."%' OR news_Content LIKE '%".$Term."%'";
			$q = $DB->query($s);
			$t = $q->num_rows;
			if($t>0){
				$Message['News'] = array();
				while($r = $q->fetch_array()){
					$row['NewsID'] = $r['news_NewsID'];
					$row['Title'] = $r['news_Title'];
					$row['Brief'] = $r['news_Brief'];
					$row['ImageURL'] = 'https://www.convoynetwork.com/convoy-webapp/img/img-noticias/'.$r['news_Image'];
					$row['Date'] = $r['news_Datetime'];
					$row['Likes'] = $r['NoLikes'];
					$row['UserLike'] = $r['UserLike'];
					$row['ShareURL']='https://www.convoynetwork.com/convoy-webapp/noticias?NewsID='.$r['news_NewsID'];
					array_push($Message['News'], $row);
					}
				}
			else{
				$Message['AppStatus']='0';
				$Message['AppResponse']="No hay resultados.";
				}
//			}
//		else{
//			$Message['AppStatus']=2;
//			$Message['AppResponse']="Tu periodo de prueba expiró o necesitas renovar tu suscripción para ver este contenido.";
//			}
		}
	else{
		$Message['AppStatus']='0';
		$Message['AppResponse']="Usuario inválido.";
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	}

?>