<?php
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');


function UserStatus($Status){
	switch($Status){
		case 0: $Description = 'Inactivo, sin datos de tarjeta'; break;
		case 1: $Description = 'Activo'; break;
		case 2: $Description = 'Suspendido, tarjeta sin fondos'; break;
		case 3: $Description = 'Cancelado, suscripción cancelada por el usuario'; break;
		}
	return $Description;
	}



function RadioList($Datetime, $UserID) {
	$Message = array();
	$BD=ConnectDB();
	if($UserID!=''){
		$sU="SELECT user_Userid, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_UserID='".$UserID."'";
		$qU=$BD->query($sU);
		$rU=$qU->fetch_array();
		$FechaLimite = date('Y-m-d', strtotime($rU['Fecha']. ' + 7 days'));
		$Today = date('Y-m-d');
		if(($rU['user_Status']==0 && $Today<=$FechaLimite) || ($rU['user_Status']==1)){
			$Last7Day = date('Y-m-d', strtotime($Datetime. ' - 7 days')).' 00:00:00';
			$Next7Day = date('Y-m-d', strtotime($Datetime. ' + 7 days')).' 23:59:59';
			$sD = "
			SELECT
				DATE_FORMAT(radi_IniDatetime, '%Y-%m-%d') AS Dates
			FROM Radio 
			WHERE
				radi_IniDatetime BETWEEN '".$Last7Day."' AND '".$Next7Day."'
				AND radi_Status='A'
			GROUP BY Dates
			ORDER BY radi_IniDatetime ASC";
			$qD = $BD->query($sD);

			$s = "
			SELECT
				radi_RadioID,
				radi_Title,
				radi_Description,
				radi_Image,
				radi_Track,
				radi_IniDatetime,
				radi_EndDatetime,
				(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='R' AND like_ObjectID=radi_RadioID) AS NoLikes,
				(SELECT COUNT(*) FROM Likes WHERE like_ObjectType='R' AND like_ObjectID=radi_RadioID AND like_UserID='".$UserID."') AS UserLike
			FROM Radio 
			WHERE
				radi_IniDatetime BETWEEN '".$Last7Day."' AND '".$Next7Day."'
				AND radi_Status='A'
			ORDER BY radi_IniDatetime ASC";
			$q = $BD->query($s);
			$t = $q->num_rows;


			if($t>0){
				$Message['AppStatus'] = '1';
				$Message['UserID']=$rU['user_Userid'];
				$Message['UserStatus']='1';
				$Message['UserStatusDescription']=UserStatus($rU['user_Status']);
				$Message['Dates'] = array();
				while($rD=$qD->fetch_array()){
					$row['Date'] = $rD['Dates'];
					array_push($Message['Dates'], $row);
					}
				$Message['Radio'] = array();
				while($r=$q->fetch_array()){
					$row['RadioID'] = $r['radi_RadioID'];
					$row['Title'] = $r['radi_Title'];
					$row['Description'] = $r['radi_Description'];
					$row['ImageURL'] = 'https://www.convoynetwork.com/convoy-webapp/img/cover-radio/'.$r['radi_Image'];
					$row['TrackURL'] = 'https://www.convoynetwork.com/convoy-webapp/audio-radio/'.$r['radi_Track'];
					$row['IniDatetime'] = $r['radi_IniDatetime'];
					$row['EndDatetime'] = $r['radi_EndDatetime'];
					$row['NoLikes'] = $r['NoLikes'];
					$row['UserLike'] = $r['UserLike'];
					array_push($Message['Radio'], $row);
					}
				}
			else{
				$Message['AppStatus']=0;
				$Message['AppResponse']="No se encontraron resultados";
				}
			}
		else{
			$Message['AppStatus']=2;
			$Message['AppResponse']="Tu periodo de prueba expir� o necesitas renovar tu suscripci�n para ver este contenido.";
			}
		}
	else{
		$Message['AppStatus']=0;
		$Message['AppResponse']="No proporcion� un usuario";
		}
    return $Message;
	//print_r(json_encode($Message, JSON_PRETTY_PRINT));
	}
?>