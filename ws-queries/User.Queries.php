<?php
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');
// require_once('Notification.Queries.php');

function UserRegister($Login, $Email, $Password, $Birthdate){
	$Message=array();
    if($Birthdate == false)
        $Birthdate = '0000-00-00';
	if($Email!='' && $Password!='' && $Login!='' && $Birthdate != ''){
		$DB = ConnectDB(); 
			$sC="SELECT user_Email, user_Login FROM Users WHERE user_Email='".$Email."' OR user_Login='".$Login."'";
		$qC = $DB->query($sC);
		$tC = $qC->num_rows;
		$rC = $qC->fetch_array();
		if($tC>0) {
			$Message['AppStatus']='0';
			$Message['AppResponse']='';
			if($rC['user_Login']==$Login){
				$Message['AppResponse'].="El usuario que proporcionaste ya se encuentra registrado.";
                http_response_code(400);//'Bad Request'
				}
			if($rC['user_Email']==$Email){
				$Message['AppResponse'].="El correo que proporcionaste ya se encuentra registrado.";
                http_response_code(400);//'Bad Request'
				}
			}
        
		else{
			$AuthCode = rand(100000,999999);
			$s = "INSERT INTO Users(user_Login, user_Email, user_Password, user_Birthdate, user_AuthCode) VALUES ('".$Login."', '".$Email."', '".$Password."', '".$Birthdate."', '".$AuthCode."')";
			$q = $DB->query($s);
			$a = $DB->affected_rows;
			$id = $DB->insert_id;
			if($a>0){
				$Message['AppStatus']='1';
				$Message['AppResponse']="Bienvenido a CONVOY. Tienes una prueba de 7 días gratis.";
				$Message['UserID'] = strval($id);
				}
			else{
				$Message['AppStatus']='0';
				$Message['AppResponse']="Error al crear registro.";
                http_response_code(400);//'Bad Request'
				}
			}
		}
	else{
		$Message['AppStatus']='0';
		$Message['AppResponse']="Usuario, contraseña y fecha de nacimiento son obligatorios.";
        http_response_code(400);//'Bad Request'
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}

//Login
function UserLogin($Login, $Password){
	$DB = ConnectDB();
	$Message=array();
	$s = "
		SELECT
			user_Userid,
			user_Login,
			user_Password,
			user_Email,
			/*user_FullName,*/
			user_Birthdate,
			user_Avatar,
			user_ProfileType,
			user_Creation,
			user_SubType, 
			DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha,
			user_Status,
			(SELECT COUNT(*) FROM Followers WHERE foll_UserID1 = user_Userid) AS Following,
			(SELECT COUNT(*) FROM Followers WHERE foll_UserID2 = user_Userid) AS Followers
		
		FROM Users 
		WHERE user_Login='".$Login."' OR user_Email='".$Login."'
		AND user_Password = '" . $Password . "'";
	$q = $DB->query($s);
	$t = $q->num_rows;
	if($t>0) {
		$r = $q->fetch_array();
		
		$Message['AppStatus']="1";
		$Message['UserID'] = $r['user_Userid'];
		$Message['Login'] = $r['user_Login'];
		//$Message['Fullname'] = $r['user_FullName'];
		$Message['Email'] = $r['user_Email'];
		$Message['Birthdate'] = $r['user_Birthdate'];
		$userAvatar = ($r['user_Avatar']!='') ? $Message['UserID'].'/'.$r['user_Avatar'] : 'avatar.png';
		$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'.$userAvatar;
		$Message['ProfileType'] = $r['user_ProfileType'];
		$Message['SubType'] = $r['user_SubType'];

		$Message['Followers'] = $r['Followers'];
		$Message['Following'] = $r['Following'];

		$Message['Status']='1';
		$sLL = "UPDATE Users SET user_LastSession=NOW() WHERE user_Userid='".$r['user_Userid']."'";
		$qLL = $DB->query($sLL);

		if(validUserLogin($r['user_Userid'], $r['user_SubType'], $r['Fecha'], $r['user_Status'])) {
			$Message['StatusKey'] = 'OK';
		} else {
			$Message['StatusKey'] = 'TRIAL_PERIOD_EXPIRED';
			$Message['AppResponse']="Tu periodo de prueba ha expirado o necesitas reactivar tu cuenta.";
		}
	} else {
		$Message['AppStatus']="0";
		$Message['AppResponse']="El usuario no existe o la contraseña es invalida";
        http_response_code(403); //'Frobidden'
	}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
}


function validUserLogin($userId, $userSubType, $creationDate, $userStatus) {
	$FechaLimite = date('Y-m-d', strtotime($creationDate. ' + 7 days'));
	$Today = date('Y-m-d');
	if ($userStatus == 0 || $FechaLimite <= $Today) {
		switch ($userSubType) {
			case 'a':
			case 'w':
				// ask for openpay status
				$openpay = new OpenPayDecorator();
				$subscription = $openpay->getCustomerSubscriptionId($userId);
				return ($subscription['AppStatus'] == '0') ? false : true;
				break;
			case 'i':
				// ask for ioPurchase
				$ioPurchase = PurchaseReviewGeneral($userId);
				return ($ioPurchase['SubscriptionStatus'] == '0') ? fasle : true;
				break;
			default:
				// ask for what is already there
				// could it happen that a non 'wai' user can be an active
				// user??
				return ($userStatus == 1 || $FechaLimite >= $Today);
				break;
		}
	} else {
		return true;
	}
		
}


function UserInfoEdit($UserID, $UserViewing) {
	$DB = ConnectDB(); 
	$Message=array();
	$s = "
	SELECT Users.*,
	(SELECT COUNT(*) FROM Followers WHERE foll_UserID1=".$UserID." AND foll_Status = '') AS Following,
	(SELECT COUNT(*) FROM Followers WHERE foll_UserID2=".$UserID." AND foll_Status = '') AS Followers,
    (SELECT COUNT(*) FROM Followers WHERE foll_UserID1=".$UserViewing." AND foll_UserID2=user_Userid AND foll_Status = '') AS FollowingMe,
    (SELECT COUNT(*) FROM Followers WHERE foll_UserID2=".$UserViewing." AND foll_UserID1=user_Userid AND foll_Status = '') AS FollowsMe
	FROM Users
	WHERE user_Userid='".$UserID."'";
	$q = $DB->query($s);
	$t =$q->num_rows;
	if($t>0){
		$Message['AppStatus']="1";
		$r = $q->fetch_array();
				$Message['UserID'] = $r['user_Userid'];
				$Message['Login'] = $r['user_Login'];
				//$Message['Password'] = $r['user_Password'];
				//$Message['Fullname'] = $r['user_FullName'];
				$Message['Email'] = $r['user_Email'];
				$Message['Genre'] = $r['user_Genre'];
				$Message['Birthdate'] = $r['user_Birthdate'];
				if($r['user_Avatar']!=''){
					$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'.$UserID.'/'.$r['user_Avatar'];
					}
				else{
					$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.png';
					}
        
				$Message['Description'] = $r['user_Description'];
                $Message['Website'] = $r['user_Website'];
                $Message['Instagram'] = $r['user_Instagram'];
				$Message['Twitter'] = $r['user_Twitter'];
				$Message['Facebook'] = $r['user_Facebook'];
				$Message['Followers'] = $r['Followers'];
				$Message['Following'] = $r['Following'];
                $Message['FollowsMe'] = $r['FollowsMe'];
                $Message['FollowingMe'] = $r['FollowingMe'];
				$Message['ProfileType'] = $r['user_ProfileType'];
				$Message['Creation'] = $r['user_Creation'];
				$Message['LastSession'] = $r['user_LastSession'];
				$Message['Subscription'] = $r['user_SubType'];
				$Message['Status'] = $r['user_Status'];
        
		}
	else{
		$Message['AppStatus']="0";
		$Message['AppResponse']="No hay resultados";
        http_response_code(404);//'Not Found'
		}
    return $Message;
	//print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}


function UserInfo($UserID, $UserViewing) {
	$DB = ConnectDB(); 
	$Message=array();
	$s = "
	SELECT Users.*,
	(SELECT COUNT(*) FROM Followers WHERE foll_UserID1=".$UserID." AND foll_Status = '') AS Following,
	(SELECT COUNT(*) FROM Followers WHERE foll_UserID2=".$UserID." AND foll_Status = '') AS Followers,
    (SELECT COUNT(*) FROM Followers WHERE foll_UserID1=".$UserViewing." AND foll_UserID2=user_Userid AND foll_Status = '') AS FollowingMe,
    (SELECT COUNT(*) FROM Followers WHERE foll_UserID2=".$UserViewing." AND foll_UserID1=user_Userid AND foll_Status = '') AS FollowsMe
	FROM Users
	WHERE user_Userid='".$UserID."'";
	$q = $DB->query($s);
	$t =$q->num_rows;
	if($t>0){
		$Message['AppStatus']="1";
		$r = $q->fetch_array();
				$Message['UserID'] = $r['user_Userid'];
				$Message['Login'] = $r['user_Login'];
				//$Message['Password'] = $r['user_Password'];
				//$Message['Fullname'] = $r['user_FullName'];
				$Message['Email'] = $r['user_Email'];
				$Message['Genre'] = $r['user_Genre'];
				$Message['Birthdate'] = $r['user_Birthdate'];
				if($r['user_Avatar']!=''){
					$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'.$UserID.'/'.$r['user_Avatar'];
					}
				else{
					$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.png';
					}
        
                $urlLink = '';
                $Instagram = '';
                $Twitter = '';
                $Facebook = '';
        
                if($r['user_Website'] != ''){
                    $urlLink = str_replace("http://","",$r['user_Website']);
                    $urlLink = str_replace("https://","",$r['user_Website']);
                    $urlLink = str_replace("www.","",$r['user_Website']);
                    $urlLink = "http://www.".$urlLink;
                }
                if($r['user_Instagram'] != ''){
                            $Instagram = str_replace("@","",$r['user_Instagram']);
                            $Instagram = "https://www.instagram.com/".$r['user_Instagram'];
                }
               
                if($r['user_Twitter'] != ''){
                            $Twitter = str_replace("@","",$r['user_Twitter']);
                            $Twitter = "https://twitter.com/".$r['user_Twitter'];
                }
        
                if($r['user_Facebook'] != '')
                    $Facebook = "https://www.facebook.com/".$r['user_Facebook'];
        
				$Message['Description'] = $r['user_Description'];
                $Message['Website'] = $urlLink;
                $Message['Instagram'] = $Instagram;
				$Message['Twitter'] = $Twitter;
				$Message['Facebook'] = $Facebook;
				$Message['Followers'] = $r['Followers'];
				$Message['Following'] = $r['Following'];
                $Message['FollowsMe'] = $r['FollowsMe'];
                $Message['FollowingMe'] = $r['FollowingMe'];
				$Message['ProfileType'] = $r['user_ProfileType'];
				$Message['Creation'] = $r['user_Creation'];
				$Message['LastSession'] = $r['user_LastSession'];
				$Message['Subscription'] = $r['user_SubType'];
				$Message['Status'] = $r['user_Status'];
        
		}
	else{
		$Message['AppStatus']="0";
		$Message['AppResponse']="No hay resultados";
        http_response_code(404);//'Not Found'
		}
    return $Message;
	//print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}
	

function UpdateUser($UserID, /*$Fullname,*/ $Description, $Website, $Instagram, $Twitter, $Facebook, $Perfil){
	$DB = ConnectDB();
	$Message=array();
	/*$s = "UPDATE Users SET user_FullName='".$Fullname."', user_Description='".$Description."', user_Website='".$Website."', user_Instagram='".$Instagram."', user_Twitter='".$Twitter."', user_Facebook='".$Facebook."', user_ProfileType='".$Perfil."' WHERE user_Userid='".$UserID."'";*/
    $s = "UPDATE Users SET user_Description='".$Description."', user_Website='".$Website."', user_Instagram='".$Instagram."', user_Twitter='".$Twitter."', user_Facebook='".$Facebook."', user_ProfileType='".$Perfil."' WHERE user_Userid='".$UserID."'";
	$q = $DB->query($s);
	$t = $DB->affected_rows;
    
    
	if($t>0){
		$Message['AppStatus']="1";
		$Message['AppResponse']="Actualizado";
        
		}
	else{
		$Message['AppStatus']="0";
		$Message['AppResponse']="No se actualizaron datos";
        http_response_code(400);//'Bad Request'
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}


function SearchUser($UserID, $Search){
	$DB=ConnectDB();
	$Message=array();
	if($Search!=''){
		$s = "
			SELECT 
				user_Userid,
				user_Login,
				user_Genre,
				user_Avatar,
				/*user_FullName,*/
	
				(SELECT COUNT(*) FROM Followers WHERE foll_UserID1=user_Userid AND foll_UserID2=user_Userid) AS Following,
				(SELECT COUNT(*) FROM Followers WHERE foll_UserID2=user_Userid AND foll_UserID1=user_Userid) AS Followers
			FROM 
				Users
			WHERE 
				user_Login LIKE '%".$Search."%'
				AND user_Status IN ('0', '1')";
		$q = $DB->query($s);
		$t=$q->num_rows;
		if ($t>0){
			$Message['AppStatus']='1';
			$Message['Usuarios']=array();
			
			while($r=$q->fetch_array()) {
				$row['UserID'] = $r['user_Userid'];
				$row['Login'] = $r['user_Login'];
				//$row['Fullname'] = $r['user_FullName'];
				$row['Genre'] = $r['user_Genre'];
				if($r['user_Avatar']!=''){
					//$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'.$r['user_Avatar'];
					$row['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'.$UserID.'/'.$r['user_Avatar'];
					}
				else{
					//$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.png';
					$row['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.png';
					}
				$row['Followers'] = $r['Followers'];
				$row['Following'] = $r['Following'];
				array_push($Message['Usuarios'], $row);
				}
			}
		else{
			$Message['AppStatus']='0';
			$Message['AppResponse']='No hay resultados';
            $Message['Usuarios']=array();
            //http_response_code(404);//'Not Found'
			}
		}
	else{
		$Message['AppStatus']='0';
		$Message['mensaje']='Ingrese un termino de búsqueda.';
        http_response_code(404);//'Not Found'
		}

	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}


function UserAvatarRemove($UserID){
	$DB = ConnectDB();
	$Message = array();
    $s1 = "SELECT user_Avatar FROM Users WHERE user_UserID='".$UserID."'";
    $q1 = $DB->query($s1);
    $r1=$q1->fetch_array();
    
    $t1=$q1->num_rows;
    if($t1>0){
        $dirNameID = '../convoy-webapp/image-avatar/'.$UserID;
        unlink($dirNameID);
        $s = "UPDATE Users SET user_Avatar='' WHERE user_UserID='".$UserID."'";
        $q = $DB->query($s);
        $Message['AppStatus'] = '1';
        $Message['AppResponse'] = 'Imagen eliminada correctamente';
        $Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.png';
    }
    else{
        $Message['AppStatus'] = '0';
        $Message['AppResponse'] = 'Error al eliminar imagen.';
        http_response_code(400);//'Bad Request';
    }
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
}

function UserAvatarAdd($UserID, $Image, $Mime){
	$DB = ConnectDB();
	$Message = array();
	if($UserID!='' || $Image!=''){
		switch($Mime){
			case 'image/gif': $ext = 'gif'; break;
			case 'image/jpeg': $ext = 'jpg'; break;
			case 'image/jpg': $ext = 'jpg'; break;
			case 'image/png': $ext = 'png'; break;
			}
        $dirNameID = '../convoy-webapp/image-avatar/'.$UserID;
        if (!file_exists($dirNameID)) {
            mkdir($dirNameID, 0755);
        }
        
        $Filename = md5(uniqid(rand(), true)). "_" . $UserID .'.'.$ext;
        
		//$Filename = str_pad($UserID,20,'0',STR_PAD_LEFT).'.'.$ext;
		$ifp = fopen('../convoy-webapp/image-avatar/'.$UserID.'/'.$Filename, "wb");
		$UserImage = fwrite($ifp, base64_decode($Image)); 
		fclose($ifp); 
		if($UserImage==true){
			$s = "UPDATE Users SET user_Avatar='".$Filename."' WHERE user_UserID='".$UserID."'";
			$q = $DB->query($s);
			$Message['AppStatus'] = '1';
			$Message['AppResponse'] = 'Imagen actualizada correctamente';
			$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'.$UserID.'/'.$Filename;
			}
		else{
			$Message['AppStatus'] = '0';
			$Message['AppResponse'] = 'Error al actualizar la imagen';
			$Message['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.png';
            http_response_code(400);//'Bad Request'
			}		
		}
	else{
		$Message['AppStatus'] = '0';
		$Message['AppResponse'] = 'Debe proporcionar un usuario e imagen';
        http_response_code(400);//'Bad Request'
		}	
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}

?>