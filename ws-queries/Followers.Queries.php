<?php
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');

//SHOW FOLLOWERS
function FollowersShowAll($UserID, $UserViewingID){
	$Message=array();
	$DB = ConnectDB(); 
	$s = "
	SELECT
		foll_UserID1,
		foll_UserID2,
		foll_Datetime,
		user_Login,
		user_FullName,
		user_Avatar
	FROM Followers
	INNER JOIN Users ON foll_UserID1=user_Userid
	WHERE foll_UserID2='".$UserID."'
    AND foll_Status = ''
	";
	$q = $DB->query($s);
	$t = $q->num_rows;
	if($t>0){
		$Message['AppStatus'] = '1';
		$Message['Followers'] = array();
		while($r = $q->fetch_array()){
            $followerID = $r['foll_UserID1'];
            $s2 ="
            SELECT
            foll_UserID1,
            foll_UserID2
            FROM Followers
            WHERE foll_UserID1='".$UserViewingID."'
            AND foll_UserID2='".$followerID."'
            ";
            $q2 = $DB->query($s2);
            $t2 = $q2->num_rows;
			$row['UserID'] = $r['foll_UserID1'];
			$row['Username'] = $r['user_Login'];
			$row['Fullname'] = $r['user_FullName'];
			if($r['user_Avatar']!=''){
				$row['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'. $r['foll_UserID1'] .'/'.$r['user_Avatar'];
				}
			else{
				$row['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.jpg';
				}
            
            if($t2 > 0){
                $row['Following'] = "1";
            }else{
                $row['Following'] = "0";
            }
            
			array_push($Message['Followers'], $row);
			}
		}
	else{
		$Message['AppStatus'] = '0';
		$Message['AppResponse'] = 'No tienes seguidores';
        $Message['Followers'] = array();
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}


//SHOW FOLLOWING
function FollowingShowAll($UserID, $UserViewingID){
	$Message=array();
	$DB = ConnectDB(); 
	$s = "
	SELECT
		foll_UserID1,
		foll_UserID2,
		foll_Datetime,
		user_Login,
		user_FullName,
		user_Avatar
	FROM Followers
	INNER JOIN Users ON foll_UserID2=user_Userid
	WHERE foll_UserID1='".$UserID."'
    AND foll_Status = ''
	";
	$q = $DB->query($s);
	$t = $q->num_rows;
	if($t>0){
		$Message['AppStatus'] = '1';
		$Message['Following'] = array();
		while($r = $q->fetch_array()){
            $followerID = $r['foll_UserID2'];
            $s2 ="
            SELECT
            foll_UserID1,
            foll_UserID2
            FROM Followers
            WHERE foll_UserID1='".$UserViewingID."'
            AND foll_UserID2='".$followerID."'
            ";
            $q2 = $DB->query($s2);
            $t2 = $q2->num_rows;
            
			$row['UserID'] = $r['foll_UserID2'];
			$row['Username'] = $r['user_Login'];
			$row['Fullname'] = $r['user_FullName'];
			if($r['user_Avatar']!=''){
				$row['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/'. $r['foll_UserID2'].'/'.$r['user_Avatar'];
				}
			else{
				$row['Avatar'] = 'https://www.convoynetwork.com/convoy-webapp/image-avatar/avatar.jpg';
				}
            if($t2 > 0){
                $row['Following'] = "1";
            }else{
                $row['Following'] = "0";
            }
			array_push($Message['Following'], $row);
			}
		}
	else{
		$Message['AppStatus'] = '0';
		$Message['AppResponse'] = 'No sigues a nadie';
        $Message['Following'] = array();

		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}



//ADD FOLLOWERS
function FollowersAdd($UserID, $FollowID){
	$Message=array();
	$DB = ConnectDB();
    
    $sU = "SELECT user_ProfileType FROM Users WHERE user_Userid = ". $FollowID;
    $qU = $DB->query($sU);
    $rU = $qU->fetch_array();
    
	$sC = "SELECT foll_FollowerID FROM Followers WHERE foll_UserID1='".$UserID."' AND foll_UserID2='".$FollowID."'"; 
	$qC = $DB->query($sC);
	$tC = $qC->num_rows;
	if($tC==0){
        if($rU['user_ProfileType'] == "P"){
            $s = "INSERT INTO Followers (foll_UserID1, foll_UserID2) VALUES ('".$UserID."', '".$FollowID."')";
        }else if($rU['user_ProfileType'] == "C"){
            $s = "INSERT INTO Followers (foll_UserID1, foll_UserID2, foll_Status) VALUES ('".$UserID."', '".$FollowID."', 'P')";
        }
		$q = $DB->query($s);
		$t = $DB->affected_rows;
		if($t>0){
			$Message['AppStatus'] = '1';
            if($rU['user_ProfileType'] == "P"){
                $Message['AppResponse'] = 'Ahora sigues a esta persona';
                $Message['Following'] = '1';
                $Message['Pending'] = '0';
            }else if($rU['user_ProfileType'] == "C"){
                $Message['AppResponse'] = 'Solicitud enviada';
                $Message['Following'] = '0';
                $Message['Pending'] = '1';
            }
			
            
			}
		else{
			$Message['AppStatus'] = '0';
			$Message['AppResponse'] = 'Error al seguir';
            http_response_code(400);//'Bad Request'
			}
		}
	else{
		$Message['AppStatus'] = '0';
		$Message['AppResponse'] = 'Ya sigues a esta persona';
        $Message['Following'] = '1';
        http_response_code(400);//'Bad Request'
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
}


//DELETE FOLLOWING
function FollowersDelete($UserID, $FollowID){
	$Message=array();
	$DB = ConnectDB(); 
	if($tC==0){
		$s = "DELETE FROM Followers WHERE foll_UserID1='".$UserID."' AND foll_UserID2='".$FollowID."'";
		$q = $DB->query($s);
		$Message['AppStatus'] = '1';
		$Message['AppResponse'] = 'Ya no sigues a esta persona';
        $Message['Following'] = '0';
			}
	else{
		$Message['AppStatus'] = '0';
		$Message['AppResponse'] = 'Ocurrió un error, intentalo nuevamente';
        http_response_code(400);//'Bad Request'
			}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}

//"BLOCK" YOUR FOLLOWER (DELETE FOLLOW OF SOMEONE THAT FOLLOWS YOU)
function FollowersBlock($UserID, $BlockID){
	$Message=array();
	$DB = ConnectDB();
    
	if($tC==0){
		$s = "DELETE FROM Followers WHERE foll_UserID1='".$BlockID."' AND foll_UserID2='".$UserID."'";
		$q = $DB->query($s);
		$Message['AppStatus'] = '1';
		$Message['AppResponse'] = 'has bloqueado al usuario exitosamente';
        $Message['Following'] = '0';
        
			}
	else{
		$Message['AppStatus'] = '0';
		$Message['AppResponse'] = 'Ocurrió un error, intentalo nuevamente';
        http_response_code(400);//'Bad Request'
			}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
	}

function FollowersCommon($followerId, $followedId)
{
	$logedUserInfo = getActivity($followerId, 'follows');
	$followedInfo = getActivity($followedId, 'follows');

    // die(var_dump($logedUserInfo, $followedInfo));

	foreach ($followedInfo as $key => $value) {
        $index = "0";

		foreach ($logedUserInfo as $element) {
			if ($element['UserID'] == $value['UserID'])
                $index = "1";
		}

		$followedInfo[$key]['Common'] = $index;
	}
    if($followedInfo == null){
        $followedInfo = array();
    }
	return $followedInfo;
}

    function FollowingCommon($followerId, $followedId)
    {
        $logedUserInfo = getActivity($followerId, 'followed'); //followed
        $followedArr = getActivity($followedId, 'followed'); //followed
        
        foreach ($followedArr as $key => $value) {
            $index = "0";
            
            foreach ($logedUserInfo as $element) {
                if ($element['Userid'] == $value['Userid'])
                    $index = "1";
            }
            
            $followedArr[$key]['Common'] = $index;
        }
        if($followedArr == null){
            $followedArr = array();
        }
        return $followedArr;
    }
    
    function FollowersActivity($userId)
    {
        //$followedId = $followerId;
        //$userIds = array($followerId, $followedId);
        $DB = ConnectDB();
        $usersInfo = array();
        empty ( $usersInfo );
        //foreach ($userIds as $userId)
        //{
            
            $s = "SELECT ".
            "u.user_Userid as UserID, ".
            "CONCAT('Has seguido a ',u.user_Login) as Username, ".
            "CONCAT('https://www.convoynetwork.com/convoy-webapp/image-avatar/',u.user_Userid,'/',u.user_Avatar) as Avatar, ".
            "u.user_Description as Description, ".
            "u.user_Website as Website, ".
            "f.foll_Datetime as FollowDate FROM Users AS u ".
            "INNER JOIN Followers AS f ON f.foll_UserID2 = u.user_Userid ".
            "WHERE f.foll_UserID1 = " . $userId . " AND f.foll_Status = '';";
            
            $q = $DB->query($s);
            $t = $q->num_rows;
            
            if($t > 0)
            {
                while($r = $q->fetch_array(MYSQLI_ASSOC))
                {
                    
                    $usersInfo[$userId][] = $r;
                    
                }
            }
        //}
        $DB->close();
        
        $followerArr = $usersInfo[$userId];
        $followedArr = $usersInfo[$userId];
        

        
        if($followedArr == null){
            $followedArr = array();
        }
        return $followedArr;
    }

    function FollowingActivity($userId)
    {
        $DB = ConnectDB();
        $usersInfo = array();
        empty ( $usersInfo );
        
        $s = "SELECT ".
        "u.user_Userid as UserID, ".
        "CONCAT(u.user_Login,' te ha seguido') as Username, ".
        "CONCAT('https://www.convoynetwork.com/convoy-webapp/image-avatar/',u.user_Userid,'/',u.user_Avatar) as Avatar, ".
        "u.user_Description as Description, ".
        "u.user_Website as Website, ".
        "f.foll_Datetime as FollowDate FROM Users AS u ".
        "INNER JOIN Followers AS f ON f.foll_UserID1 = u.user_Userid ".
        "WHERE f.foll_UserID2 = " . $userId . " AND f.foll_Status = '';";
        
        $q = $DB->query($s);
        $t = $q->num_rows;
        
        if($t > 0)
        {
            while($r = $q->fetch_array(MYSQLI_ASSOC))
            {
                
                $r['Activity'] = getAllFollowedCommons($userId, $r['UserID']);
                $usersInfo[$userId][] = $r;
            }
        }

        $DB->close();
        
        $followerArr = $usersInfo[$userId];
        $followedArr = $usersInfo[$userId];
        
        
        
        if($followedArr == null){
            $followedArr = array();
        }
        return $followedArr;
    }
    
    function PendingRequests($userId)
    {
        $DB = ConnectDB();
        $usersInfo = array();
        empty ( $usersInfo );
        
        $s = "SELECT u.user_Userid as UserID, u.user_Login as Username, CONCAT('https://www.convoynetwork.com/convoy-webapp/image-avatar/',u.user_Userid,'/',u.user_Avatar) as Avatar, u.user_Description as Description,
        u.user_Website as Website, f.foll_Datetime as FollowDate FROM Users AS u
        INNER JOIN Followers AS f ON f.foll_UserID1 = u.user_Userid
        WHERE f.foll_UserID2 = " . $userId . " AND foll_Status = 'P';";
        
        $q = $DB->query($s);
        $t = $q->num_rows;
        
        if($t > 0)
        {
            while($r = $q->fetch_array(MYSQLI_ASSOC))
            {
                $usersInfo[$userId][] = $r;
            }
        }
        
        $DB->close();
        
        $followerArr = $usersInfo[$userId];
        $followedArr = $usersInfo[$userId];
        
        
        
        if($followedArr == null){
            $followedArr = array();
        }
        return $followedArr;
    }
    
    function FollowApprove($UserID, $RequesterID, $Approval){
    
        $DB = ConnectDB();
        $Message = array();
        if($Approval == "1"){
            $s= "UPDATE  convoyne_convoy.Followers SET  foll_Status =  '' WHERE  Followers.foll_UserID1 = ". $RequesterID ." AND Followers.foll_UserID2 = ". $UserID ." ;";
            $q = $DB->query($s);
            $Message['AppStatus'] = "1";
            $Message['Approval'] = "1";
            $Message['FollowStatus'] = "Solicitud aceptada.";

        }else if ($Approval == "0"){
           $s= "DELETE FROM convoyne_convoy.Followers WHERE Followers.foll_UserID1 = ". $RequesterID ." AND Followers.foll_UserID2 = ". $UserID ." ;";
            $q = $DB->query($s);
            $Message['AppStatus'] = "1";
            $Message['Approval'] = "0";
            $Message['FollowStatus'] = "Solicitud rechazada.";
        }
        print_r(json_encode($Message, JSON_PRETTY_PRINT));
        $DB->close();
    }
    ?>