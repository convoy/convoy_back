<?php
    
require_once('function.ConnDB.php');

/**
 * Here the process to reset the password is started, it only requires the $Email
 * but we are leaving the user id as well...
 * A validation string (token) will be generated and sent thru an email
 *
 * @param string $Email Valid email that is related to an account
 * @return An email with the generated validation token
 */
function RequestPassReset($UserID, $Email)
{
	$BD=ConnectDB();
	$Message=array();

	$mainUrl = 'https://www.convoynetwork.com/convoy-webapp/';
			 

	$s="SELECT user_Login, user_Email FROM Users WHERE user_Email ='".$Email."'";

	$qry = $BD->query($s);
	$total = $qry->num_rows;
	
	if($total>0){

		$userInfo = $qry->fetch_array(MYSQLI_ASSOC);
		$token = createRequestToken($userInfo['user_Login'], $Email);
		$themail = new Email();

		$emailTo = 'karkaim@gmail.com';
		$emailTo2 = $Email;
		$subject = 'Recuperar tu contraseña';
		$params = array(
			'Username' => $userInfo['user_Login'],
			'Email' => $Email,
			'url' => $mainUrl . 'password-reset?email=' . $Email . '&token=' . $token
			// 'password' => $newPassword;
		);

		$sent = $themail->sendMail($emailTo2, $subject, 'resetPass', $params, "Resetear contraseña:");

		if ( is_string($sent) ) {
			//error_log("An error ocurred: " . $themail->mail->ErrorInfo, 3, require(dirname(__FILE__) . "/../error_log");
			$Message['AppStatus'] = "0";
			$Message['AppResponse'] = "Error: no se ha enviado el correo para resetear contraseña";
	        http_response_code(500); // Internal server error
		} else {
			$Message['AppStatus']='1';
        	$Message['AppResponse']="Se ha enviado un correo de restauraciónde contraseña.";
		}

	} else {
		$Message['AppStatus']='0';
		$Message['AppResponse']="No existe nungú usuario registrado con ese correo";
	}

	print_r(json_encode($Message, JSON_PRETTY_PRINT));
 	$BD->close();
}

/**
 * Once the user has started the process to reset her/his password it will check both
 * The User email and the validation Token
 *
 * @param string $Email Valid email that is related to an account
 * @param string $token Validation token provided via email
 * @return $Message weather with an error or with the user id, so the app is able to change the password
 */
function RequestPassCheck($Email, $token)
{
	$BD=ConnectDB();
	$Message=array();

	$s="SELECT user_UserID, user_Login, user_Email FROM Users WHERE user_Email ='".$Email."'";

	$qry = $BD->query($s);
	$total = $q->num_rows;
	$userInfo = $qry->fetch_array(MYSQLI_ASSOC);
	if($total>0){

		$compareToken = createRequestToken($userInfo['user_Login'], $Email);

		if (!$compareToken == $token) {
			$Message['AppStatus'] = "0";
			$Message['AppResponse'] = "Error: El token no es valido";
			http_response_code(404); // request error
		} else {
			$Message['AppStatus']='1';
			$Message['AppResponse']="Los datos son correctos, ya puede resetear el password";
			$userInfo['token'] = $token;
			$Message['UserInfo'] = $userInfo;
		}

	} else {
		$Message['AppStatus'] = "0";
		$Message['AppResponse']="No existe nungún usuario registrado con ese correo";
		http_response_code(404); // request error
	}

	$BD->close();
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
}

/**
 * The final step of the process to reset the user password.
 * Receives the UserId, the new password and the validation token
 *
 * @param string $Email Valid email that is related to an account
 * @param string $newPass
 * @param string $token Validation token provided via email
 * @return $Message weather with an error or a success message
 */
function PasswordReset($Email, $newPass, $token = false)
{
	$DB=ConnectDB();
	$Message=array();

	$s="SELECT user_UserID, user_Login, user_Email FROM Users WHERE user_Email = '".$Email."' ";

	$qry = $DB->query($s);
	$total = $qry->num_rows;
	$userInfo = $qry->fetch_array(MYSQLI_ASSOC);
	if($total>0) {

		$compareToken = createRequestToken($userInfo['user_Login'], $Email);

		if (!$token) {
			// ERROR
			$Message['AppStatus'] = "0";
			$Message['AppResponse'] = "Error: Debes proveer un token valido";
			http_response_code(404); // Internal server error
		} else {
			// CHECK FOR TOKEN AND UPDATE
			if ($compareToken != $token) {
				// ERROR
				$Message['AppStatus'] = "0";
				$Message['AppResponse'] = "Error: El token de validación provisto es incorrecto";
				http_response_code(404); // Internal server error
			} elseif (!updateUserPassword($userInfo['user_UserID'], $newPass, $DB)) { // UPDATE PASSWORD WITH newPass
				// ERROR
				$Message['AppStatus'] = "0";
				$Message['AppResponse'] = "Error: No se ha podido actualizar la contraseña";
				http_response_code(500); // Internal server error
			} else {
				// SUCCESS, SEND EMAIL

				$themail = new Email();

				// $emailTo = 'karkaim@gmail.com';
				$emailTo = $userInfo['user_Email'];
				$subject = 'Has reseteado tu contraseña exitosamente';
				$params = array(
					'Username' => $userInfo['user_Login'],
				);

				$sent = $themail->sendMail([$emailTo], $subject, 'resetPassSuccess', $params, "Has reseteado tu contraseña exitosamente");

				if ( is_string($sent) ) {
					error_log("An error ocurred: " . $themail->mail->ErrorInfo, 3, "/var/log/app/my-errors.log");
					$Message['AppStatus'] = "0";
					$Message['AppResponse'] = "Error: no se ha enviado el correo para resetear contraseña";
			        http_response_code(500); // Internal server error
				} else {
					$Message['AppStatus']='1';
	            	$Message['AppResponse']="Se ha reseteado la contraseña y se ha enviado un correo de confirmación.";
				}

				//$Message['AppStatus'] = "1";
				//$Message['AppResponse'] = "Error: No se ha podido actualizar la contraseña";
			}
		}

	} else {
		$Message['AppStatus']='0';
		$Message['AppResponse']="No existe nungú usuario registrado con ese correo";
        $Message['Email']= $Email;
		http_response_code(404); // Internal server error
	}
	
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
}


function createRequestToken($userName, $email)
{
	$thisMonth = date("Y-m");
	return md5($userName . $email . $thisMonth);
}

function randomPassword()
{
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	return substr(str_shuffle($chars),0,7);
}

function updateUserPassword($Userid, $newPass, $DB)
{
	$sql = "UPDATE Users SET user_Password = '" . $newPass . "' WHERE user_Userid = " . $Userid;
	return (!$DB->query($sql)) ? false : true;
}


?>