<?php
date_default_timezone_set("America/Chicago");
require_once('function.ConnDB.php');
require_once('function.General.php');

function guardar_notificacion($id_usuario, $modulo, $id) {
	$ConnDB=ConnectDB();
	switch($modulo){
		case 'noticia': $iField = 'noti_NewsID'; break;
		case 'podcast': $iField = 'noti_PodcastID'; break;
		case 'radio': $iField = 'noti_RadioID'; break;
		case 'followers': $iField = 'noti_FollowerID'; break;
		}
	$s = "INSERT INTO Notifications (".$iField.", noti_UserID, noti_Datetime) VALUES ('".$id."', '".$id_usuario."', NOW())"; 
	$q=$ConnDB->query($s);
	return true;
	$ConnDB->close();
	}

function SaveNotification($UserID, $Module, $ID) {
	$ConnDB=ConnectDB();
	switch($Module){
		case 'like': $iField = 'noti_LikeID'; break;
		case 'noticia': $iField = 'noti_NewsID'; break;
		case 'podcast': $iField = 'noti_PodcastID'; break;
		case 'radio': $iField = 'noti_RadioID'; break;
		case 'followers': $iField = 'noti_FollowerID'; break;
		}
	$s = "INSERT INTO Notifications (".$iField.", noti_UserID, noti_Datetime) VALUES ('".$ID."', '".$UserID."', NOW())"; 
	$q=$ConnDB->query($s);
	return true;
	$ConnDB->close();
	}


function eliminar_notificacion($id_usuario, $modulo, $id) {
	$ConnDB=ConnectDB();
	switch($modulo){
		case 'news': $iField = 'noti_NewsID'; break;
		case 'podcast': $iField = 'noti_PodcastID'; break;
		case 'radio': $iField = 'noti_RadioID'; break;
		case 'followers': $iField = 'noti_FollowerID'; break;
		case 'like': $iField = 'noti_LikeID'; break;
		}
	$s = "DELETE FROM Notifications WHERE ".$iField."='".$id."' AND noti_UserID='".$id_usuario."'"; 
	$q=$ConnDB->query($s);
	return true;
	$ConnDB->close();
	}



function LastNotifications($UserID, $Datetime){$sFol = "SELECT foll_UserID2 FROM Followers WHERE foll_UserID1='".$UserID."'";
	$BD = ConnectDB();
	$Messages = array();
	$sSts = "SELECT user_Userid, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_Userid='".$UserID."'";
	$qSts = $BD->query($sSts);
	$rSts = $qSts->fetch_array();
	$FechaLimite = date('Y-m-d', strtotime($rSts['Fecha']. ' + 7 days'));
	if(($rSts['user_Status']==0 && $rSts['Fecha']<=$FechaLimite) || ($rSts['user_Status']==1)){
		$sFol = "SELECT foll_UserID2 FROM Followers WHERE foll_UserID1='".$UserID."'";
		$qFol = $BD->query($sFol);
		$Following = '';
		while($rFol = $qFol->fetch_array()){
			$Following.= $rFol['foll_UserID2'].', ';
			}
		$Following.= '0';
		if($Datetime!=''){
			$DateCondition = " AND noti_Datetime BETWEEN '".$Datetime."' AND NOW() ";
			}
		$s = "
		SELECT  
			noti_NotificationID
			FROM Notifications
		WHERE 
			noti_UserID IN (".$Following.")
			".$DateCondition;
		$q = $BD->query($s);
		$t = $q->num_rows;
		$Message['app_status'] = '1';
		$Message['usuario_status'] = '1';
		$Message['notificaciones'] = $t;
		}
	else{
		$Message['app_status'] = '0';
		$Message['usuario_status'] = '2';
		$Message['app_response'] = 'Tu suscripcion ha expirado';
		}
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$BD->close();
	}



function ShowNotifications($UserID, $Datetime){
	$BD = ConnectDB();
	$Messages = array();
	$sSts = "SELECT user_Userid, user_Status, DATE_FORMAT(user_Creation, '%Y-%m-%d') AS Fecha FROM Users WHERE user_Userid='".$UserID."'";
	$qSts = $BD->query($sSts);
	$rSts = $qSts->fetch_array();
	$FechaLimite = date('Y-m-d', strtotime($rSts['Fecha']. ' + 7 days'));
	if(($rSts['user_Status']==0 && $rSts['Fecha']<=$FechaLimite) || ($rSts['user_Status']==1)){

		$sFol = "SELECT foll_UserID2 FROM Followers WHERE foll_UserID1='".$UserID."'";
		$qFol = $BD->query($sFol);
		$Following = '';
		while($rFol = $qFol->fetch_array()){
			$Following.= $rFol['foll_UserID2'].', ';
			}
		$Following.= '0';
		$DateCondition = '';
		if($Datetime!=''){
			$DateCondition = " AND noti_Datetime BETWEEN '".$Datetime."' AND NOW() ";
			}
		$s = "
		SELECT  
			user_Userid,
			user_Login,
			user_Status,
			noti_Datetime,
			noti_NotificationID,
			noti_LikeID,
			noti_NewsID,
			noti_PodcastID,
			noti_RadioID,
			noti_FollowerID,
			noti_ReadStatus
		FROM Notifications
		LEFT OUTER JOIN Users ON user_Userid=noti_UserID
		WHERE 
			noti_UserID IN (".$Following.")
			".$DateCondition."
		ORDER BY noti_Datetime DESC
		";
		$q = $BD->query($s);
		$t = $q->num_rows;
		if($t>0){
			$Message['app_status'] = '1';
			$Message['usuario_status'] = '1';
			$Message['notificaciones'] = array();
			while($r = $q->fetch_array()){
				if($r['user_Userid']==null){
					$row['id_usuario'] = '';
					$row['usuario'] = '';
				}
				else{
					$row['id_usuario'] = $r['user_Userid'];
					$row['usuario'] = $r['user_Login'];
					}
	
				$row['fecha'] = $r['noti_Datetime'];
				$row['id_notificacion'] = $r['noti_NotificationID'];
				$row['leido'] = $r['noti_ReadStatus'];
		
					$row['tipo'] = '';
					$row['noticia_balazo'] = '';
					$row['noticia_texto_completo'] = '';
					$row['noticia_cabeza'] = '';
					$row['noticia_url_imagen'] = '';
					$row['noticia_url_share'] = '';
		
					$row['podcast_id_podcast'] = '';
					$row['podcast_titulo'] = '';
					$row['podcast_url_imagen'] = '';
					$row['podcast_url_share'] = '';
		
					$row['radio_id_radio'] = '';
					$row['radio_titulo'] = '';
					$row['radio_descripcion'] = '';
					$row['radio_url_imagen'] = '';
					$row['radio_dia_semana'] = '';
					$row['radio_fecha'] = '';
					$row['radio_hora'] = '';
					$row['radio_fecha_hora'] = '';
		
					$row['followers_id_follower'] = '';
					$row['followers_id_usuario_propio'] = '';
					$row['follower_id_usuario'] = '';
					$row['followers_usuario_amigo'] = '';
					$row['followers_usuario_amigo_url_imagen'] = '';
					$row['followers_fecha_hora'] = '';
					
					$row['likes_id_like']='';
					$row['likes_tipo_elemento'] = '';
					$row['likes_id_elemento'] = '';
					$row['likes_descripcion'] = '';
		
		
				if($r['noti_LikeID']!=''){
					
//					switch()






					$row['likes_id_like'] = $r['noti_LikeID'];
					$row['likes_tipo_elemento'] = $r['noti_LikeID'];
					$row['likes_id_elemento'] = $r['noti_LikeID'];
					$row['likes_descripcion'] = $r['noti_LikeID'];
					
					
					}	
		
				else if($r['noti_NewsID']!=''){
					$sS = "
					SELECT 
						news_NewsID,
						news_Title,
						news_Brief,
						news_Content,
						news_Image
					FROM News
					WHERE news_NewsID='".$r['noti_NewsID']."'
					";
					$qS = $BD->query($sS);
					$rS = $qS->fetch_array();
						$row['tipo'] = "noticia";
						$row['noticia_id_noticia'] = $rS['news_NewsID'];
						$row['noticia_cabeza'] = $rS['news_Title'];
						$row['noticia_url_imagen'] = $rS['news_Image'];
						$row['noticia_balazo'] = $rS['news_Brief'];
						$row['noticia_texto_completo'] = $rS['news_Content'];
						$row['noticia_url_share'] = "http://convoynetwork.com/convoy-webapp/noticias?id_noticia=".$rS['news_NewsID'];
					}
				else if($r['noti_PodcastID']!=''){
					$row['tipo'] = "podcast";
					$sS = "
					SELECT
						podc_PodcastID,
						podc_Title,
						podc_Image
					FROM Podcast 
					WHERE podc_PodcastID='".$r['noti_PodcastID']."'
					";
					$qS = $BD->query($sS);
					$rS = $qS->fetch_array();
						$row['podcast_id_podcast'] = $rS['podc_PodcastID'];
						$row['podcast_titulo'] = $rS['podc_Title'];
						$row['podcast_url_imagen'] = $rS['podc_Image'];
						$row['podcast_url_share'] = 'http://convoynetwork.com/convoy-webapp/podcast?id_podcast='.$rS['podc_PodcastID'];
					}
				else if($r['noti_RadioID']!=''){
					$row['tipo'] = "radio";
					$sS = "
					SELECT
						radi_RadioID,
						radi_Title,
						radi_Descripcion,
						radio_Image,
						radi_DayOfWeek,
						radi_Date,
						radi_Hour,
						radi_Datetime,
						radi_Status
					FROM Radio
					WHERE radi_RadioID='".$r['noti_RadioID']."'
					
					";
					$qS = $BD->query($sS);
					$rS = $qS->fetch_array();
					$row['radio_id_radio'] = $rS['radi_RadioID'];
					$row['radio_titulo'] = $rS['radi_Title'];
					$row['radio_descripcion'] = $rS['radi_Descripcion'];
					$row['radio_url_imagen'] = $rS['radio_Image'];
					$row['radio_dia_semana'] = $rS['radi_DayOfWeek'];
					$row['radio_fecha'] = $rS['radi_Date'];
					$row['radio_hora'] = $rS['radi_Hour'];
					$row['radio_fecha_hora'] = $rS['radi_Datetime'];
					}
				else if($r['noti_FollowerID']!=''){
					$row['tipo'] = "follower";
					$sS = "
					SELECT 
						foll_FollowerID,
						foll_UserID1,
						foll_UserID2, 
						user_Login,
						user_Avatar,
						foll_Datetime
					FROM Followers 
					LEFT OUTER JOIN 
						Users ON user_UserID=foll_UserID2 
					WHERE foll_FollowerID='".$r['noti_FollowerID']."'";
					$qS = $BD->query($sS);
					$rS = $qS->fetch_array();
					$row['followers_id_follower'] = $rS['foll_FollowerID'];
					$row['followers_id_usuario_propio'] = $rS['foll_UserID1'];
					$row['follower_id_usuario'] = $rS['foll_UserID2'];
					$row['followers_usuario_amigo'] = $rS['user_Login'];
					$row['followers_usuario_amigo_url_imagen'] = $rS['user_Avatar'];
					$row['followers_fecha_hora'] = $rS['foll_Datetime'];
		
					}
				array_push($Message['notificaciones'], $row);
				}
			}
		else{
			$Message['usuario_status'] = '2';
			$Message['app_response'] = 'Tu suscripcion ha expirado';
			}
		
		}
	else{
		}
	
	
		

	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$BD->close();
	}


?>