<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/Followers.Queries.php");
	require_once("ws-queries/Like.Queries.php"); 
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	$jsonArray = json_decode($jsonInput,true);
	$UserID = $jsonArray['UserID'];
	$followersActivity = FollowersActivity($UserID);
    $followingActivity = FollowingActivity($UserID);
    $pedingRequests = PendingRequests($UserID);
    $newsActivity = NewsActivity($UserID);
	$episodesActivity = EpisodesActivity($UserID);
    $radioActivity = RadioActivity($UserID);
    
	$arResp = array(
        'pending' => $pedingRequests,
		'follows' => $followersActivity,
        'followed' => $followingActivity,
		'news' => $newsActivity,
		'episodes' => $episodesActivity,
        'radio' => $radioActivity
	);

	echo json_encode($arResp, JSON_PRETTY_PRINT);
	}
    else{
        echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>