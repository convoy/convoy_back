<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/Like.Queries.php"); 
$handle = fopen('php://input','r');
$jsonInput = fgets($handle);
$jsonArray = json_decode($jsonInput,true);
$UserID = $jsonArray['UserID'];
$ObjectType = $jsonArray['ObjectType'];
$ObjectID = $jsonArray['ObjectID'];
AddLike($UserID, $ObjectType, $ObjectID);
?>