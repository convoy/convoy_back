<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/Podcast.Queries.php");
	$handle = fopen('php://input','r');
	// Decoding JSON into an Array
	$jsonInput=fgets($handle);
	$jsonArray = json_decode($jsonInput,true);
	$SeasonID = $jsonArray['SeasonID'];
	$UserID = $jsonArray['UserID'];
	TracksList($SeasonID, $UserID);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>