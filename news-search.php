<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/News.Queries.php"); 
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	// Decoding JSON into an Array
	$jsonArray = json_decode($jsonInput,true);
	$Term = '';
	$UserID = $jsonArray['UserID'];
	if($jsonArray['Term']!=''){
		$Term = $jsonArray['Term'];
		}
	News_Search($UserID, $Term);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>