<?php 
header('Content-Type: application/json');
require_once("ws-queries/Activities.Queries.php"); 
$handle = fopen('php://input','r');
$jsonInput = fgets($handle);
$jsonArray = json_decode($jsonInput,true);
$Datetime = $jsonArray['Datetime'];
$UserID = $jsonArray['UserID'];
ShowActivities($UserID, $Datetime);
?>