<?php
date_default_timezone_set("America/Mexico_City");
require_once('function.ConnDB.php');

// TODO: DELETE THIS?
function AddPurchase($UserID, $Purchase, $Expires, $ProductID, $LastPurchase, $Quantity, $TransactionID, $CancelDate){
	$Message=array();

    $DB = ConnectDB();
	$s0 = "SELECT * FROM iTunes_Subscription WHERE iTSub_UserID ='". $UserID ."'";
    $q0 = $DB->query($s0);
    $t0 = $q0->num_rows;
    if($t0>0){
        $Message['AppStatus']='0';
        $Message['AppResponse']="Ya existe registro con este usuario.";
        http_response_code(400);//'Bad Request'
    }
    else{
        $s = "INSERT INTO iTunes_Subscription".
             " (iTSub_UserID, iTSub_OrigPurch, iTSub_Expire, iTSub_ProdID,".
             " iTSub_LastPurch, iTSub_Qty, iTSub_TransID, iTSub_CancelStatus,".
             " iTSub_CancelDate) VALUES ".
             "('".$UserID."', '".
                $Purchase."', '".
                $Expires."', '".
                $ProductID."', '".
                $LastPurchase."', '".
                $Quantity."', '".
                $TransactionID ."',".
                " 0, '".
                $CancelDate ."')";
        $q = $DB->query($s);
        $a = $DB->affected_rows;
        $id = $DB->insert_id;        
        
        $s1 = "UPDATE  convoyne_convoy.Users SET  user_SubType =  'i' WHERE  Users.user_Userid =". $UserID .";";
        $q1 = $DB->query($s1);

        if($a>0){
            $s1 = "SELECT * FROM iTunes_Subscription WHERE iTSub_UserID ='". $UserID ."'";
            $q1 = $DB->query($s1);
            $r1 = $q1->fetch_array();
            $t1 = $q1->num_rows;
            if($t1>0){
                //$Message['PurchaseID'] = $r1['id_iTSub'];
                $Message['UserID'] = $r1['iTSub_UserID'];
                $Message['UserType'] = 'i';
                $Message['OriginalPurchase'] = $r1['iTSub_OrigPurch'];
                $Message['Expires'] = $r1['iTSub_Expire'];
                $Message['ProductID'] = $r1['iTSub_ProdID'];
                $Message['LastPurchase'] = $r1['iTSub_LastPurch'];
                $Message['Quantity'] = $r1['iTSub_Qty'];
                $Message['TransactionID'] = $r1['iTSub_TransID'];
                $Message['CancelStatus'] = $r1['iTSub_CancelStatus'];
                $Message['CancelDate'] = $r1['iTSub_CancelDate'];
            }
        }else{
            $Message['AppStatus']='0';
            $Message['AppResponse']="Error al crear registro.";
            http_response_code(400);//'Bad Request'
        }
    }//else from if t0
	print_r(json_encode($Message, JSON_PRETTY_PRINT));
	$DB->close();
    }
    
function PurchaseReview($UserID){
    $Message=array();
    
    $DB = ConnectDB();
    $Today = date('Y-m-d H:i');
    $s1 = "SELECT Users.user_UserID as UserID, Users.user_SubType AS SubType, Users.user_Creation AS CreationDate, ".
    "iTunes_Subscription.id_iTSub, iTunes_Subscription.iTSub_UserID, iTunes_Subscription.iTSub_OrigPurch, iTunes_Subscription.iTSub_Expire,
    iTunes_Subscription.iTSub_ProdID, iTunes_Subscription.iTSub_LastPurch, iTunes_Subscription.iTSub_Qty, 
    iTunes_Subscription.iTSub_TransID, iTunes_Subscription.iTSub_CancelStatus, iTunes_Subscription.iTSub_CancelDate ".
    "FROM iTunes_Subscription INNER JOIN Users ON iTunes_Subscription.iTSub_UserID = Users.user_UserID ".
    "WHERE iTSub_UserID = ". $UserID." ORDER BY iTunes_Subscription.id_iTSub DESC LIMIT 1";
    $q1 = $DB->query($s1);
    $r1 = $q1->fetch_array();
    $t1 = $q1->num_rows;
    if($t1>0){

        $Message['PurchaseID'] = $r1['id_iTSub'];
        $Message['UserID'] = $r1['iTSub_UserID'];
        $Message['SubType'] = $r1['SubType'];
        $Message['OriginalPurchase'] = $r1['iTSub_OrigPurch'];
        $Message['Expires'] = $r1['iTSub_Expire'];
        $Message['ProductID'] = $r1['iTSub_ProdID'];
        $Message['LastPurchase'] = $r1['iTSub_LastPurch'];
        $Message['Quantity'] = $r1['iTSub_Qty'];
        $Message['TransactionID'] = $r1['iTSub_TransID'];
        $Message['CancelStatus'] = $r1['iTSub_CancelStatus'];
        $Message['CancelDate'] = $r1['iTSub_CancelDate'];
        //$Message['Today'] = $Today;
        
        $datetime2 = new DateTime($r1['iTSub_Expire']);
        $datetime1 = new DateTime($Today);
        $interval = $datetime1->diff($datetime2);
        //$Message['Transcurridos'] = $interval->format('%R%a');
        $Transcurridos = $interval->format('%R%a');
        
        if($Transcurridos>=0){
            $Message['SubscriptionStatus'] = "1";
            $Message['SubscriptionMessage'] = "La suscripción es vigente.";
        }else{
            $Message['SubscriptionStatus'] = "0";
            //http_response_code(402);//'Payment Required'
            $Message['SubscriptionMessage'] = "La suscripción ha expirado.";
        }

    }
    else{
        
        $s2 = "SELECT user_SubType AS SubType, user_Creation AS CreationDate FROM Users WHERE user_UserID = ". $UserID;
        $q2 = $DB->query($s2);
        $r2 = $q2->fetch_array();
        $t2 = $q2->num_rows;
        
        
        
        //Expire Date
        $date = date_create($r2['CreationDate']);
        $date = date_format($r2['CreationDate'],'Y-m-d');
        //$date = strtotime($date, '+7 day');
        
        $date = date("Y-m-d",strtotime("+7 day",strtotime($r2['CreationDate']) ));
        
        //$date = date('Y-m-d', $date);
        
        $Message['Expires'] = $date;
        
        //Días transcurridos de prueba
        $datetime2 = new DateTime($date);
        $datetime1 = new DateTime($Today);
        
        $Message['datetime'] = $datetime1;
        $Message['datetime2'] = $datetime2;
        
        $interval = $datetime1->diff($datetime2);
        $Transcurridos = $interval->format('%R%a');
        
        $Message['transc'] = $Transcurridos;
        if($Transcurridos>=0){
            $Message['SubscriptionStatus'] = "1";
            $Message['SubscriptionMessage'] = "La suscripción es vigente.";
        }else{
            $Message['SubscriptionStatus'] = "0";
            //http_response_code(402);//'Payment Required'
            $Message['SubscriptionMessage'] = "La suscripción ha expirado.";
        }
        
        $Message['CancelStatus'] = "0";
        $Message['SubType'] = "n";
        $Message['AppStatus']='0';
        $Message['AppResponse']="No existe registro del pago.";
        //http_response_code(404);//'Not Found'
    }//else from if t0
    print_r(json_encode($Message, JSON_PRETTY_PRINT));
    $DB->close();
}

function PurchaseReviewGeneral($UserID){
    $Message=array();
    
    $DB = ConnectDB();
    $Today = date('Y-m-d H:i');
    $s1 = "SELECT ".
    " Users.user_UserID as UserID,".
    " Users.user_SubType AS SubType,".
    " iTunes_Subscription.iTSub_Expire,".
    "FROM iTunes_Subscription ".
    
    "INNER JOIN Users ON iTunes_Subscription.iTSub_UserID = Users.user_UserID ".
    "WHERE iTSub_UserID = ". $UserID." ORDER BY iTunes_Subscription.id_iTSub DESC LIMIT 1";
    $q1 = $DB->query($s1);
    $r1 = $q1->fetch_array();
    $t1 = $q1->num_rows;
    
    if($t1>0){
        
        $Message['SubType'] = $r1['SubType'];
        
        $datetime2 = new DateTime($r1['iTSub_Expire']);
        $datetime1 = new DateTime($Today);
        $interval = $datetime1->diff($datetime2);
        $Transcurridos = $interval->format('%R%a');
        
        if($Transcurridos>=0){
            $Message['SubscriptionStatus'] = "1";
            $Message['SubscriptionMessage'] = "La suscripción es vigente.";
        }else{
            $Message['SubscriptionStatus'] = "0";
            http_response_code(402);//'Payment Required'
            $Message['SubscriptionMessage'] = "La suscripción ha expirado.";
        }

    }
    else{
        
        $s2 = "SELECT ".
        "user_SubType AS SubType, ".
        "user_Creation AS CreationDate ".
        "FROM Users ".
        "WHERE user_UserID = ". $UserID;
        $q2 = $DB->query($s2);
        $r2 = $q2->fetch_array();
        $t2 = $q2->num_rows;
        
        //Expire Date + date formtatting + add 7 days for trial
        $date = date_create($r2['CreationDate']);
        $date = date_format($r2['CreationDate'],'Y-m-d');
        $date = date("Y-m-d",strtotime("+7 day",strtotime($r2['CreationDate']) ));
        
        //$Message['Expires'] = $date;
        
        //Días transcurridos de prueba
        $datetime2 = new DateTime($date);
        $datetime1 = new DateTime($Today);
        
        //$Message['datetime'] = $datetime1;
        //$Message['datetime2'] = $datetime2;
        
        $interval = $datetime1->diff($datetime2);
        $Transcurridos = $interval->format('%R%a');
        
        //$Message['transc'] = $Transcurridos;
        
        $Message['SubType'] = $r2['SubType'];
        
        if($Transcurridos>=0){
            $Message['SubscriptionStatus'] = "1";
            $Message['SubscriptionMessage'] = "La la semana de prueba es vigente.";
        }else{
            $Message['SubscriptionStatus'] = "0";
            $Message['SubscriptionMessage'] = "La la semana de prueba ha expirado.";
        }
        $Message['AppResponse']="No existe registro del pago.";
    } //else from if t0
    print_r(json_encode($Message, JSON_PRETTY_PRINT));
    $DB->close();
   
}
    

/**
 * This method insert a new record anytime a purchase is charged
 * @param int UserID
 * @param date Expires
 * @param int ProductID
 * @param date LastPurchase
 * @param int Quantity
 * @param string TransactionID
 * @param date CancelDate
 * 
 * @return json response with wether the user info or the errors
 */    
function PurchaseUpdate($UserID, $Expires, $ProductID, $LastPurchase, $Quantity, $TransactionID, $CancelDate){
    $Message=array();
    $Purchase = $LastPurchase;
    $defaultDaysLeft = 10;
    $DB = ConnectDB();
    $Today = date('Y-m-d H:i');

    //get the first records from iTunes_Subscription that match with the user id
    $sql =  'SELECT * FROM iTunes_Subscription 
        WHERE iTSub_UserID = ' . $UserID . ' 
        AND iTSub_OrigPurch != \'\'
        ORDER BY id_iTSub ASC LIMIT 1;';

    $qry = $DB->query($sql);
    $total = $qry->num_rows;

    if ($total > 0) {
        //set the original purchase date with the latest record in the DB
        $result = $qry->fetch_array();
        $Purchase = $result['iTSub_OrigPurch'];
    } 


    $daysToExpire = ($Expires !== '') ? getDaysLeft($Expires) : $defaultDaysLeft;

    $daysToCancel = ($CancelDate !== '') ? getDaysLeft($CancelDate) : $defaultDaysLeft;

    //CancelStatus is true if there are no more daysToCancel: $daysToExpire <= 0 OR there are no more days to Expire
    $CancelStatus = ( $daysToCancel <= 0 || $daysToExpire <= 0 ) ? 1 : 0;

    $s = "INSERT INTO iTunes_Subscription " .
         "(iTSub_UserID, iTSub_OrigPurch, iTSub_Expire, iTSub_ProdID, iTSub_LastPurch, " .
         "iTSub_Qty, iTSub_TransID, iTSub_CancelDate, iTSub_CancelStatus ) " .
         "VALUES ('".$UserID."', '".$Purchase."', '".$Expires."', '".$ProductID."', " . 
         "'".$LastPurchase."', '". $Quantity."', '". $TransactionID ."', ". 
         "'".$CancelDate ."', ". $CancelStatus.")";

    $q = $DB->query($s);
    $a = $DB->affected_rows;
    $id = $DB->insert_id;

    if($a>0){
        $s1 = "SELECT * FROM iTunes_Subscription " . 
              "WHERE iTSub_UserID ='". $UserID ."' " .
              "AND iTSub_OrigPurch = '". $Purchase . "' " .
              "AND iTSub_Expire = '". $Expires . "' " .
              "AND iTSub_ProdID = '". $ProductID . "' " .
              "AND iTSub_LastPurch = '". $LastPurchase . "' " .
              "AND iTSub_Qty = '". $Quantity . "' " .
              "AND iTSub_TransID = '". $TransactionID . "' " .
              "AND iTSub_CancelStatus = ". $CancelStatus . " ";
              //"AND iTSub_CancelDate = '". $CancelDate . "' " ; //se quita este porque iOS envía un array vacío, y la BD busca una fecha y no retorna nada
        $q1 = $DB->query($s1);
        $r1 = $q1->fetch_array();
        $t1 = $q1->num_rows;
        if($t1>0) {
            //$Message['PurchaseID'] = $r1['id_iTSub'];
            $Message['UserID'] = $r1['iTSub_UserID'];
            $Message['SubType'] = 'i'; // TODO: how do we change this guy? <-- it is un the Users DB field: user_SubType
            $Message['SubscriptionStatus'] = "1"; // SubStatus has to appear as well *
            $Message['OriginalPurchase'] = $r1['iTSub_OrigPurch'];
            $Message['Expires'] = $r1['iTSub_Expire'];
            $Message['ProductID'] = $r1['iTSub_ProdID'];
            $Message['LastPurchase'] = $r1['iTSub_LastPurch'];
            $Message['Quantity'] = $r1['iTSub_Qty'];
            $Message['TransactionID'] = $r1['iTSub_TransID'];
            $Message['CancelStatus'] = $r1['iTSub_CancelStatus'];
            $Message['CancelDate'] = $r1['iTSub_CancelDate'];
        }
    } else {
        $Message['AppStatus']='0';
        $Message['AppResponse']="no se ha podido liberar el cobro de esta compra.";
        http_response_code(400);//'Bad Request'
    }//else from if t0
    print_r(json_encode($Message, JSON_PRETTY_PRINT));
    $DB->close();
}

/**
 * get the expiration date and compare it with today's date to get the diff
 * @param date $expireDate in mySQL format Y-m-d H:i:s
 * @return The ammount of days diff
 */
function getDaysLeft($dueDate)
{
    $today = new DateTime(date('Y-m-d H:i:s')); // Today's date
    $dueDate = new DateTime($dueDate);
    $interval = $today->diff($dueDate);
    return $interval->format('%R%a days');
}


?>