<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/Followers.Queries.php"); 
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	$jsonArray = json_decode($jsonInput,true);
	$UserID = $jsonArray['UserID'];
    $UserViewingID = $jsonArray['UserViewingID'];
	FollowingShowAll($UserID, $UserViewingID);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>