<?php 

	date_default_timezone_set("America/Chicago");
    // error_reporting(0);
	function fecha_ahora ($anio, $mes, $dia){
		$meses=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		$fecha_ahora_dia = $dia;
		$fecha_ahora_mes = $mes;
		$fecha_ahora_year = $anio;
		$fecha_ahora = $fecha_ahora_dia." de ".$meses[$fecha_ahora_mes]." del ".$fecha_ahora_year;
		return $fecha_ahora;
	}
	function conexion() {
		$db_host    ="localhost";
		$db_usuario ="servidor_convoy";
		$db_password="diferencia";
		$db_dbname  ="servidor_convoy";
		$conexion_= new mysqli($db_host, $db_usuario, $db_password, $db_dbname);
		if (mysqli_connect_errno()) {
    		printf("Falló la conexión: %s\n", mysqli_connect_error());
    		exit();
		}
		return $conexion_;
	}
	function usuario_info($id_usuario) {

		$base_datos = conexion(); 
		$seleccionar_usuario="SELECT * FROM Users WHERE user_Userid='".$id_usuario."'";
		$seleccionando_usuario=$base_datos->query($seleccionar_usuario);
		if ($seleccionando_usuario->num_rows > 0) {
		$mensaje=array();
		$mensaje['app_status']=1;

		// Followers y followins

		$seleccionar_followers="SELECT * FROM Followers WHERE foll_UserID2='".$id_usuario."'";
		$seleccionando_followers=$base_datos->query($seleccionar_followers);
		$mensaje['followers']=$seleccionando_followers->num_rows;

		$seleccionar_followers="SELECT * FROM Followers WHERE foll_UserID1='".$id_usuario."'";
		$seleccionando_followers=$base_datos->query($seleccionar_followers);
		$mensaje['following']=$seleccionando_followers->num_rows;

		$fila_usuario=$seleccionando_usuario->fetch_array(MYSQLI_ASSOC);
			$mensaje_tal=array();
			$mensaje_tal['id_usuario']=$fila_usuario['user_Userid'];
			$mensaje_tal['usuario']=$fila_usuario['user_Login'];
			$mensaje_tal['password']=$fila_usuario['user_Password'];
			$mensaje_tal['nombre']=$fila_usuario['user_FullName'];
			$mensaje_tal['email']=$fila_usuario['user_Email'];
			$mensaje_tal['genero']=$fila_usuario['user_Genre'];
			$menseje_tal['fecha_cumple']=$fila_usuario['user_Birthdate'];
			$mensaje_tal['url_imagen']=$fila_usuario['user_Avatar'];
			$mensaje_tal['descripcion']=$fila_usuario['user_Description'];
			$mensaje_tal['website']=$fila_usuario['user_Website'];
			$mensaje_tal['instagram']=$fila_usuario['user_Instagram'];
			$mensaje_tal['twitter']=$fila_usuario['user_Twitter'];
			$mensaje_tal['facebook']=$fila_usuario['user_Facebook'];
			$mensaje_tal['tipo_perfil']=$fila_usuario['user_ProfileType'];
			$mensaje_tal['fecha_hora_ingresado']=$fila_usuario['user_Creation'];
			$mensaje_tal['fecha_hora_sesion']=$fila_usuario['user_LastSession'];
			$mensaje_tal['cc']=$fila_usuario['user_CCSecurity'];
			$mensaje_tal['customer_id']=$fila_usuario['user_CustomerID'];
			$mensaje_tal['ip']=$fila_usuario['user_IPAddress'];
			$mensaje_tal['codigo_autentifica']=$fila_usuario['user_AuthCode'];
			$mensaje_tal['status']=$fila_usuario['user_Status'];
			$mensaje_tal['razon_cancelacion']=$fila_usuario['user_CancelReason'];
		$fila_maestra=array_merge($mensaje_tal, $mensaje);
		print_r(json_encode($fila_maestra, JSON_PRETTY_PRINT));
		}
	}
	function usuario_login($usuario, $password) {
		$base_datos = conexion(); 
		$mensaje=array();
		$mensaje_tal=array();
		$seleccionar_usuario="SELECT * FROM Users WHERE user_Login='".$usuario."'";
		$seleccionando_usuario=$base_datos->query($seleccionar_usuario);
 
		$fila_usuario=$seleccionando_usuario->fetch_array(MYSQLI_ASSOC);
		if ($fila_usuario['user_Status']=='1') { 
			// $fila_usuario=$seleccionando_usuario->fetch_array(MYSQLI_ASSOC);
			if ($fila_usuario['user_Password']==$password) { 

					// Login sucessful 
					$actualizar_usuario="UPDATE Users SET user_IPAddress='".$_SERVER['REMOTE_ADDR']."', user_LastSession=NOW() WHERE user_Userid='".$fila_usuario['user_Userid']."'";
					$actualizando_usuario=$base_datos->query($actualizar_usuario);
					
					$mensaje_tal['app_status']=1;

			$mensaje_tal['app_status']=1;
			$mensaje_tal['id_usuario']=$fila_usuario['user_Userid'];
			$mensaje_tal['usuario']=$fila_usuario['user_Login'];
			$mensaje_tal['password']=$fila_usuario['user_Password'];
			$mensaje_tal['email']=$fila_usuario['user_Email'];
			$mensaje_tal['genero']=$fila_usuario['user_Genre'];
			$menseje_tal['fecha_cumple']=$fila_usuario['user_Birthdate'];
			$mensaje_tal['url_imagen']=$fila_usuario['user_Avatar'];
			$mensaje_tal['descripcion']=$fila_usuario['user_Description'];
			$mensaje_tal['website']=$fila_usuario['user_Website'];
			$mensaje_tal['instagram']=$fila_usuario['user_Instagram'];
			$mensaje_tal['twitter']=$fila_usuario['user_Twitter'];
			$mensaje_tal['facebook']=$fila_usuario['user_Facebook'];
			$mensaje_tal['tipo_perfil']=$fila_usuario['user_ProfileType'];
			$mensaje_tal['fecha_hora_ingresado']=$fila_usuario['user_Creation'];
			$mensaje_tal['fecha_hora_sesion']=$fila_usuario['user_LastSession'];
			$mensaje_tal['cc']=$fila_usuario['user_CCSecurity'];
			$mensaje_tal['customer_id']=$fila_usuario['user_CustomerID'];
			$mensaje_tal['ip']=$fila_usuario['user_IPAddress'];
			$mensaje_tal['codigo_autentifica']=$fila_usuario['user_AuthCode'];
			$mensaje_tal['status']=$fila_usuario['user_Status'];
			$mensaje_tal['razon_cancelacion']=$fila_usuario['user_CancelReason'];
			$fila_total=$mensaje_tal;
					// print_r(json_encode($fila_total, JSON_PRETTY_PRINT));
			} else { 
			$mensaje['error']="Contraseña no válida";
			$mensaje['app_status']=0;
			$fila_total=$mensaje; 
			}
		} if ($fila_usuario['user_Status']=='0') {  
			 
			$mensaje_tal['app_status']=1;
			$mensaje_tal['id_usuario']=$fila_usuario['user_Userid'];
			$mensaje_tal['usuario']=$fila_usuario['user_Login'];
			$mensaje_tal['password']=$fila_usuario['user_Password'];
			$mensaje_tal['email']=$fila_usuario['user_Email'];
			$mensaje_tal['genero']=$fila_usuario['user_Genre'];
			$menseje_tal['fecha_cumple']=$fila_usuario['user_Birthdate'];
			$mensaje_tal['url_imagen']=$fila_usuario['user_Avatar'];
			$mensaje_tal['descripcion']=$fila_usuario['user_Description'];
			$mensaje_tal['website']=$fila_usuario['user_Website'];
			$mensaje_tal['instagram']=$fila_usuario['user_Instagram'];
			$mensaje_tal['twitter']=$fila_usuario['user_Twitter'];
			$mensaje_tal['facebook']=$fila_usuario['user_Facebook'];
			$mensaje_tal['tipo_perfil']=$fila_usuario['user_ProfileType'];
			$mensaje_tal['fecha_hora_ingresado']=$fila_usuario['user_Creation'];
			$mensaje_tal['fecha_hora_sesion']=$fila_usuario['user_LastSession'];
			$mensaje_tal['cc']=$fila_usuarios['user_CCSecurity'];
			$mensaje_tal['customer_id']=$fila_usuario['user_CustomerID'];
			$mensaje_tal['ip']=$fila_usuario['user_IPAddress'];
			$mensaje_tal['codigo_autentifica']=$fila_usuario['user_AuthCode'];
			$mensaje_tal['status']=$fila_usuario['user_Status'];
			$mensaje_tal['razon_cancelacion']=$fila_usuario['user_CancelReason'];
			$fila_total=$mensaje_tal;
        } if ($fila_usuario['user_Status']=='3') {
			 
			$mensaje_tal['app_status']=1;$mensaje_tal['id_usuario']=$fila_usuario['user_Userid'];
			$mensaje_tal['usuario']=$fila_usuario['user_Login'];
			$mensaje_tal['password']=$fila_usuario['user_Password'];
			$mensaje_tal['email']=$fila_usuario['user_Email'];
			$mensaje_tal['genero']=$fila_usuario['user_Genre'];
			$menseje_tal['fecha_cumple']=$fila_usuario['user_Birthdate'];
			$mensaje_tal['url_imagen']=$fila_usuario['user_Avatar'];
			$mensaje_tal['descripcion']=$fila_usuario['user_Description'];
			$mensaje_tal['website']=$fila_usuario['user_Website'];
			$mensaje_tal['instagram']=$fila_usuario['user_Instagram'];
			$mensaje_tal['twitter']=$fila_usuario['user_Twitter'];
			$mensaje_tal['facebook']=$fila_usuario['user_Facebook'];
			$mensaje_tal['tipo_perfil']=$fila_usuario['user_ProfileType'];
			$mensaje_tal['fecha_hora_ingresado']=$fila_usuario['user_Creation'];
			$mensaje_tal['fecha_hora_sesion']=$fila_usuario['user_LastSession'];
			$mensaje_tal['cc']=$fila_usuarios['user_CCSecurity'];
			$mensaje_tal['customer_id']=$fila_usuario['user_CustomerID'];
			$mensaje_tal['ip']=$fila_usuario['user_IPAddress'];
			$mensaje_tal['codigo_autentifica']=$fila_usuario['user_AuthCode'];
			$mensaje_tal['status']=$fila_usuario['user_Status'];
			$mensaje_tal['razon_cancelacion']=$fila_usuario['user_CancelReason'];
			$fila_total=$mensaje_tal;
		}  if ($seleccionando_usuario->num_rows==0) {
			$mensaje['app_status']=0;
			$mensaje['error']="El usuario ".$usuario." no existe.";
			// $mensaje['consulta']=$seleccionar_usuario;
			$fila_total=$mensaje;
		} 
		return $fila_total;

	} 
	function registro_usuario($usuario, $password, $email, $url_imagen, $descripcion, $website, $instagram, $twitter, $facebook, $genero, $fecha_cumple) {
		$base_datos = conexion(); 

		$seleccionar_usuario="SELECT * FROM usuarios WHERE usuario='".$usuario."'";
		$seleccionando_usuario=$base_datos->query($seleccionar_usuario);

		$mensaje=array();
		
		if ($seleccionando_usuario->num_rows > 0) {  
			$mensaje['app_status']=0;
			$mensaje['error']="El usuario ".$usuario." ya existe.";
		}  
		
		$seleccionar_correo="SELECT * FROM usuarios WHERE email='".$email."'";
		$seleccionando_correo=$base_datos->query($seleccionar_correo);

		if ($seleccionando_correo->num_rows > 0) {
			$mensaje['app_status']=0;
			$mensaje['error']="El email ".$email." ya existe";
		} 
		 
		if (isset($mensaje['error'])) { } else {
 
				$codigo_autentifica=rand(100000,999999);

				$insertar_usuario="INSERT INTO usuarios (usuario, password, email, genero, fecha_cumple, url_imagen, descripcion, website, instagram, twitter, facebook, fecha_hora_ingresado, ip, codigo_autentifica, status) VALUES ('".$usuario."', '".$password."', '".$email."', '".$genero."', '".$fecha_cumple."', '".$url_imagen."', '".$descripcion."', '".$website."', '".$instagram."', '".$twitter."', '".$facebook."', NOW(), '".$_SERVER['REMOTE_ADDR']."', '".$codigo_autentifica."', '0')";
				$insertando_usuario=$base_datos->query($insertar_usuario);
				 
				$seleccionar_usuario_p="SELECT * FROM usuarios WHERE usuario='".$usuario."'";
				$seleccionando_usuario_p=$base_datos->query($seleccionar_usuario_p);
				$fila_usuario_p=$seleccionando_usuario_p->fetch_array(MYSQLI_ASSOC);

				$insertar_notificacion="INSERT INTO notificaciones (notificacion, id_usuario, fecha_hora) VALUES ('BIENVENIDO A CONVOY', '".$fila_usuario_p['id_usuario']."', NOW())";
				$insertando_notificacion=$base_datos->query($insertar_notificacion);
 
				$mensaje['app_status']=1;
 				$mensaje['aviso']="Usuario registrado satisfactoriamente";
 				$mensaje=array_merge($mensaje, $fila_usuario_p);

			} 
			return $mensaje;

	}

	function actualizar_usuario($id_usuario, $url_imagen, $descripcion, $website, $instagram, $twitter, $facebook, $tipo_perfil) {
		$base_datos = conexion(); 
		$seleccionar_usuario="SELECT * FROM usuarios WHERE id_usuario='".$id_usuario."'";
		$seleccionando_usuario=$base_datos->query($seleccionar_usuario);
		$num_usuario=$seleccionando_usuario->num_rows;
		
$actualizar_usuario="UPDATE usuarios SET url_imagen='".$url_imagen."', descripcion='".$descripcion."', website='".$website."', instagram='".$instagram."', twitter='".$twitter."', facebook='".$facebook."', tipo_perfil='".$tipo_perfil."' WHERE id_usuario='".$id_usuario."'";
		$seleccionando_codigo=$base_datos->query($actualizar_usuario);
		return $actualizar_usuario;
}
	function codigo_autorizacion($usuario, $auth_code) {
		$base_datos=conexion();
		$mensaje=array();
		$seleccionar_codigo="SELECT * FROM usuario WHERE usuario='".$usuario."' AND auth_code='".$auth_code."'";
		$seleccionando_codigo=$base_datos->query($seleccionar_codigo);
		$autorizado=$seleccionando_codigo->num_rows;

		if ($autorizado==1) {
			$actualizar_usuario="UPDATE usuario SET activo=1 WHERE usuario='".$usuario."'";
			$actualizando_usuario=$base_datos->query($actualizar_usuario);
			$mensaje['app_status']=1;
 				
			$mensaje['aviso']='Tu usuario ha sido activado satisfactoriamente';
		} else {
			$mensaje['app_status']=0;
 				
			$mensaje['error']='Usuario no encontrado';
		}
		return (json_encode($mensaje, JSON_PRETTY_PRINT));
	} 
	function listar_todos() {
		$base_datos=conexion();
		$seleccionar_amigos="SELECT id_usuario, usuario, url_imagen FROM usuarios ORDER BY fecha_hora_sesion DESC";
		$seleccionando_amigos=$base_datos->query($seleccionar_amigos);
		$fila_amigos=$seleccionando_amigos->fetch_array(MYSQLI_ASSOC);
		$mensaje=array();
		$mensaje['app_status']=1;
 		$fila_amigos=array_merge($fila_amigos, $mensaje);
		print_r(json_encode($fila_amigos, JSON_PRETTY_PRINT));
	}
function luhn_check($number) {

  // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
  $number=preg_replace('/\D/', '', $number);

  // Set the string length and parity
  $number_length=strlen($number);
  $parity=$number_length % 2;

  // Loop through each digit and do the maths
  $total=0;
  for ($i=0; $i<$number_length; $i++) {
    $digit=$number[$i];
    // Multiply alternate digits by two
    if ($i % 2 == $parity) {
      $digit*=2;
      // If the sum is two digits, add them together (in effect)
      if ($digit > 9) {
        $digit-=9;
      }
    }
    // Total up the digits
    $total+=$digit;
  }

  // If the total mod 10 equals 0, the number is valid
  return ($total % 10 == 0) ? TRUE : FALSE;

}
function crear_pago($id_usuario, $nombre, $cc, $mes_expira, $anio_expira, $secure_cc, $cp, $periodo) {
		$base_datos=conexion();

		$seleccionar_otros_pagos="SELECT * FROM pagos WHERE id_usuario='".$id_usuario."'";
		$seleccionando_otros_pagos=$base_datos->query($seleccionar_otros_pagos);
		$num_pagos=$seleccionando_otros_pagos->num_rows;


		if ($num_pagos==0) {
		 
		}


	$subscriptionData = array(
		'trial_days' => ’14’,
		'plan_id' => 'pduar9iitv4enjftuwyl',
		'card_id' => 'konvkvcd5ih8ta65umie');
	$openpay = Openpay::getInstance();
	$customer = $openpay->customers->get('a9ualumwnrcxkl42l6mh');
	$subscription = $customer->subscriptions->add($subscriptionData);

	if (luhn_check($cc)) {
		$insertar_pago="INSERT INTO Payments 
		(paym_UserID, paym_BeginOn, paym_ExpiresOn, paym_PaymentDate, paym_Status, paym_Type, paym_IPAddress) VALUES 
		('".$id_usuario."', '".$periodo."', DATE_ADD('".$periodo_prox."', INTERVAL 30 DAY), NOW(), '1', '0', '".$_SERVER['REMOTE_ADDR']."')";
		$insertando_pago=$base_datos->query($insertar_pago);
		$last_cc=substr($cc, -4);
		$actualizar_usuario="UPDATE Users SET user_CCSecurity='".$last_cc."', user_Status='1' WHERE user_Userid='".$id_usuario."'";
		$actualizando_usuario=$base_datos->query($actualizar_usuario);

		$mensaje=array(); 
		$mensaje['app_status']=1;
		$mensaje['aviso']="Pago realizado satisfactoriamente"; } else {
			$mensaje=array();
			$mensaje['app_status']=0;
			$mensaje['error']="Tarjeta no válida o pago no cargado, favor de intentar nuevamente";
		}
		return $mensaje;
	}
	function actualizar_pago($id_usuario, $nombre, $cc, $mes_expira, $anio_expira, $secure_cc, $cp) {
		$base_datos=conexion(); 
		$seleccionar_pago="SELECT * FROM pagos WHERE id_usuario='".$id_usuario."' ORDER BY id_pago DESC LIMIT 0, 1";

		$seleccionando_pago=$base_datos->query($seleccionar_pagos);
		$fila_pago=$seleccionando_pago->fetch_array(MYSQLI_ASSOC);
		$periodo=$fila_pagos['periodo_inicio'];
		$periodo_prox=date($periodo, strtotime("+30 days"));

		$seleccionar_pago="SELECT * FROM pagos WHERE id_usuario='".$id_usuario."' ORDER BY id_pago DESC LIMIT 0, 1";
		$seleccionando_pago=$base_datos->query($seleccionar_pagos);
		$fila_pago=$seleccionando_pago->fetch_array(MYSQLI_ASSOC);
/* 
	$openpay = Openpay::getInstance();
	$customer = $openpay->customers->get('a9ualumwnrcxkl42l6mh');
	$subscription = $customer->subscriptions->get('s7ri24srbldoqqlfo4vp');
	$subscription->trial_end_date = $periodo_prox;
	$subscription->save();
*/
		$insertar_pago="UPDATE pagos SET periodo_inicio='".$periodo_prox."', status='1', fecha_pago=NOW(), tipo_pago='1', ip='".$_SERVER['REMOTE_ADDR']."' WHERE id_pago='".$fila_pago['id_pago']."'";
		$insertando_pago=$base_datos->query($insertar_pago);
		$last_cc=substr($cc, -4);
		$actualizar_usuario="UPDATE usuario SET cc='".$last_cc."', status=1 WHERE id_usuario='".$id_usuario."'";
		$actualizando_usuario=$base_datos->query($actualizar_usuario);

		$mensaje=array();

		$mensaje['app_status']=1;
		$mensaje['aviso']="Payment updated";
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}
	function cancelar_pago($id_usuario, $razon) {
		$base_datos=conexion();
		// Integrar WS de OpenPay para cancelaciones
		$actualizar_usuario="UPDATE usuarios SET status=3, razon_cancelacion='".$razon."' WHERE id_usuario='".$id_usuario."'";
		$actualizando_usuario=$base_datos->query($actualizar_usuario);
		$mensaje=array();
		$mensaje['aviso']="Subscripcion cancelada";

		$mensaje['app_status']=1;
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}
	function listar_pagos($id_usuario) {
		$base_datos=conexion();
		$seleccionar_pagos="SELECT * FROM pagos WHERE id_usuario='".$id_usuario."'";
		$seleccionando_pagos=$base_datos->query($seleccionar_pagos);
		$mensaje=array();
		$mensaje['app_status']=1;
		$fila_pagos=array_merge($fila_pagos, $mensaje);
		print_r(json_encode($fila_pagos, JSON_PRETTY_PRINT));
	}

	function listar_ultimo_pago($id_usuario) {
		$base_datos=conexion();
		$seleccionar_usuario="SELECT * FROM Users WHERE user_Userid='".$id_usuario."'";
		$seleccionando_usuario=$base_datos->query($seleccionar_usuario);
		$fila_usuario=$seleccionando_usuario->fetch_array(MYSQLI_ASSOC);
		$seleccionar_pagos="SELECT * FROM Payments WHERE paym_UserID='".$id_usuario."' ORDER BY paym_PaymentID DESC LIMIT 0,1";
		$seleccionando_pagos=$base_datos->query($seleccionar_pagos);
		$mensaje=array();
		$num_pagos=$seleccionando_pagos->num_rows;
		$mensaje=$seleccionando_pagos->fetch_array(MYSQLI_ASSOC);
		$mensaje['cc']=$fila_usuario['user_CCSecurity'];
		$mensaje['app_status']=1;
		
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}
	function guardar_amigo($id_usuario, $id_usuario_amigo) {
		$base_datos=conexion();
		$insertar_amigos="INSERT INTO followers (id_usuario_propio, id_usuario_amigo, fecha_hora, ip) VALUES ('".$id_usuario."', '".$id_usuario_amigo."', NOW(), '".$_SERVER['REMOTE_ADDR']."')";
		$insertando_amigo=$base_datos->query($insertar_amigos);

		$mensaje=array();

		$mensaje['app_status']=1;
		$mensaje['aviso']="Amigo agregado satisfactoriamente";
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));

	}
	function listar_amigos($id_usuario) {
		$base_datos=conexion();
		$seleccionar_amigos="SELECT * FROM Followers WHERE foll_UserID1='".$id_usuario."'";
		$seleccionando_amigos=$base_datos->query($seleccionar_amigos);
		$num_amigos=$seleccionando_amigos->num_rows;
		if ($num_amigos==0) {
			$mensaje=array();
		$mensaje['app_status']=0;
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));	
		} else {
			echo '{"app_status": 1, 
			"amigos" : [';
		$num_amigos=$seleccionando_amigos->num_rows;
		$num_ciclo=0;
		while($fila_amigos=$seleccionando_amigos->fetch_array(MYSQLI_ASSOC)) {
			$num_ciclo++;
			$seleccionar_usuario_amigo="SELECT user_Userid, user_Login, user_FullName, user_Avatar, user_Status FROM Users WHERE user_Userid='".$fila_amigos['foll_UserID2']."'";
			$seleccionando_usuario_amigo=$base_datos->query($seleccionar_usuario_amigo);
			$fila_usuario_amigo=$seleccionando_usuario_amigo->fetch_array();
			echo '{ ';
			$seleccionar_estado="SELECT * FROM Followers WHERE foll_UserID1='".$fila_amigos['foll_UserID2']."' AND foll_UserID2='".$id_usuario."'";
			$seleccionando_estado=$base_datos->query($seleccionar_estado);
			$num_follow=$seleccionando_estado->num_rows; 
			// print_r(json_encode($fila_usuario_amigo, JSON_PRETTY_PRINT));
				echo '"id_usuario_amigo" : "'.$fila_usuario_amigo['user_Userid'].'",
				"id_usuario" : "'.$fila_usuario_amigo['user_Login'].'",
				"url_imagen" : "'.$fila_usuario_amigo['user_Avatar'].'",
				"status" : "'.$fila_usuario_amigo['user_Status'].'",
				"seguimiento_mutuo" : "'.$num_follow.'"
			}';
			if ($num_ciclo<$num_amigos) { echo ', '; }

		}
		echo ']}';
	}
}
	function listar_de_amigos($id_usuario) {
		$base_datos=conexion();
		$seleccionar_amigos="SELECT * FROM Followers WHERE foll_UserID2='".$id_usuario."'";
		$seleccionando_amigos=$base_datos->query($seleccionar_amigos);
		$num_amigos=$seleccionando_amigos->num_rows;
		if ($num_amigos==0) {
			$mensaje=array();
		$mensaje['app_status']=0;
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));	
		} else {
			echo '{"app_status": 1, 
			"amigos" : [';
		$num_amigos=$seleccionando_amigos->num_rows;
		$num_ciclo=0;
		while($fila_amigos=$seleccionando_amigos->fetch_array(MYSQLI_ASSOC)) {
			$num_ciclo++;
			$seleccionar_usuario_amigo="SELECT user_Userid, user_Login, user_FullName, user_Avatar, user_Status FROM Users WHERE user_Userid='".$fila_amigos['foll_UserID1']."'";
			$seleccionando_usuario_amigo=$base_datos->query($seleccionar_usuario_amigo);
			$fila_usuario_amigo=$seleccionando_usuario_amigo->fetch_array();
			echo '{ '; 
			$seleccionar_estado="SELECT * FROM Followers WHERE foll_UserID1='".$id_usuario."' AND foll_UserID2='".$fila_amigos['foll_UserID1']."'";
			$seleccionando_estado=$base_datos->query($seleccionar_estado);
			$num_follow=$seleccionando_estado->num_rows; 
				echo '"id_usuario_amigo" : "'.$fila_usuario_amigo['user_Userid'].'",
				"id_usuario" : "'.$fila_usuario_amigo['user_Login'].'",
				"url_imagen" : "'.$fila_usuario_amigo['user_Avatar'].'",
				"status" : "'.$fila_usuario_amigo['user_Status'].'",
				"seguimiento_mutuo" : "'.$num_follow.'"
			}'; 
			if ($num_ciclo<$num_amigos) { echo ', '; }

		}
		echo ']}';
		}
	}
	function crear_mensaje($id_mensaje_padre, $id_usuario_origen, $id_usuario_destino, $mensaje) {
		$base_datos=conexion();
		$insertar_mensaje="INSERT INTO mensajes (id_usuario_origen, id_usuario_destino, mensaje, fecha_hora, ip, status) VALUES ('".$id_usuario_origen."', '".$id_usuario_destino."', '".$mensaje."', NOW(), '".$_SERVER['REMOTE_ADDR']."', 0)";
		$insertando_mensaje=$base_datos->query($insertar_mensaje);
		
		$mensaje=array();
		$mensaje['aviso']="Mensaje enviado satisfactoriamente";
		$seleccionar_mensaje_ultimo="SELECT * FROM mensajes ORDER BY id_mensaje DESC LIMIT 0, 1";
			$seleccionando_mensaje_ultimo=$base_datos->query($seleccionar_mensaje_ultimo);
			$fila_mensaje_ultimo=$seleccionando_mensaje_ultimo->fetch_array(MYSQLI_ASSOC); /*
		if ($id_mensaje_padre==0) {
			
			$id_mensaje_padre=$fila_mensaje_ultimo['id_mensaje'];

		}
		$actualizar_mensaje="UPDATE mensajes SET id_mensaje_padre='".$id_mensaje_padre."' WHERE id_mensaje='".$fila_mensaje_ultimo['id_mensaje_padre']."'";
		$actualizando_mensaje=$base_datos->query($actualizar_mensaje); */
 
		$mensaje['app_status']=1;

		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));

	}
	function leer_mensaje($id_mensaje_padre, $id_usuario) { 
		$base_datos=conexion();
		$actualizar_mensaje="UPDATE mensajes SET status=1 WHERE id_mensaje_padre='".$id_mensaje_padre."' AND id_usuario_destino='".$id_usuario."'";
		$actualizando_mensaje=$base_datos->query($actualizar_mensaje);
		$seleccionar_mensaje="SELECT * FROM mensajes WHERE id_mensaje_padre='".$id_mensaje_padre."' ORDER BY id_mensaje DESC";
		$seleccionando_mensaje=$base_datos->query($seleccionar_mensaje);
		$num_mensaje=$seleccionando_mensaje->num_rows;
		if ($num_mensaje>0) {
		$fila_mensaje=array();
		while($fila_mensaje[]=$seleccionando_mensaje->fetch_array(MYSQLI_ASSOC));
		$mensaje=array();
		$mensaje['app_status']=1;
		$seleccionar_usuario_origen="SELECT * FROM usuarios";
		$fila_mensaje=array_merge($mensaje, $fila_mensaje); }
		else {
			$fila_mensaje=array();
			$fila_mensaje['app_status']=0;
		}
		print_r(json_encode($fila_mensaje, JSON_PRETTY_PRINT));
	}
	function listar_mensajes($id_usuario) {
		$base_datos=conexion();
		$seleccionar_mensajes_tal="SELECT DISTINCT id_mensaje_padre FROM mensajes WHERE id_usuario_origen='".$id_usuario."' OR id_usuario_destino='".$id_usuario."' ORDER BY id_mensaje_padre DESC";
		$seleccionando_mensajes_tal=$base_datos->query($seleccionar_mensajes_tal);
		$num_mensajes_tal=$seleccionando_mensajes_tal->num_rows;
		if ($num_mensajes_tal==0) { 
			$mensaje=array();
		//$mensaje['consulta']=$seleccionar_mensajes;
		$mensaje['consulta']=$seleccionar_mensajes_tal;
		$mensaje['app_status']=0;
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
		} else {

		$mensaje=array();
		while($fila_mensajes_tal=$seleccionando_mensajes_tal->fetch_array(MYSQLI_ASSOC)) {
		$seleccionar_mensaje="SELECT * FROM mensajes WHERE id_mensaje_padre='".$fila_mensajes_tal['id_mensaje_padre']."'";
		$seleccionando_mensaje=$base_datos->query($seleccionar_mensaje);
		while($fila_mensajes=$seleccionando_mensaje->fetch_array(MYSQLI_ASSOC)){
			$mensaje['mensajes'][]=$fila_mensajes;
		}	
		}
		//$mensaje['mensajes']=$seleccionando_mensajes->fetch_array(MYSQLI_ASSOC);
		$mensaje['consulta']=$seleccionar_mensaje;
		$mensaje['app_status']=1;
		//$fila_mensajes=array_merge($mensaje, $fila_mensajes);
		//print_r(json_encode($fila_mensajes, JSON_PRETTY_PRINT));
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT)); }
	}
	function eliminar_mensaje($id_mensaje) {
		$base_datos=conexion();
		$eliminar_mensaje="DELETE mensajes WHERE id_mensaje_padre='".$id_mensaje."'";
		$eliminando_mensaje=$base_datos->query($eliminar_mensaje);
		$mensaje=array();

		$mensaje['app_status']=1;
		$mensaje['aviso']="Mensaje eliminado satisfactoriamente";
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT)); 
	}
	function listar_noticias($etiqueta) {
		$base_datos=conexion();
		$seleccionar_noticias="SELECT id_noticia, cabeza, balazo, url_imagen, fecha_hora FROM noticias WHERE etiquetas = '".$etiqueta."' ORDER BY id_noticia DESC LIMIT 0, 20";
		$seleccionando_noticias=$base_datos->query($seleccionar_noticias);
		$mensaje=array();
		$mensaje['app_status']=1;
		$fila_noticias=$seleccionando_noticias->fetch_array(MYSQLI_ASSOC);

		$fila_noticias=array_merge($mensaje, $fila_noticias);
		print_r(json_encode($fila_noticias, JSON_PRETTY_PRINT));
	}
	function ver_noticia($id_noticia) {
		$base_datos=conexion();
		$seleccionar_noticia="SELECT id_noticia, cabeza, texto_completo, url_imagen, fecha_hora FROM noticias WHERE id_noticia='".$id_noticia."'";
		$seleccionando_noticia=$base_datos->query($seleccionar_noticia);
		$fila_noticia=$seleccionando_noticia->fetch_array(MYSQLI_ASSOC);
				$mensaje=array();
		$mensaje['app_status']=1;
		$fila_noticia['noticia_url_share']='http://convoynetwork.com/convoy-webapp/noticias?id_noticia='.$id_noticia;
		$fila_noticia=array_merge($mensaje, $fila_noticia);
		print_r(json_encode($fila_noticia, JSON_PRETTY_PRINT));
	}
	function listar_podcasts($fecha) {
		
		$base_datos=conexion();
		$seleccionar_podcast="SELECT * FROM podcast WHERE status=0 AND fecha_hora_publicacion ORDER BY id_podcast DESC LIMIT 0, 20";
		$seleccionando_podcast=$base_datos->query($seleccionar_podcast);
		$mensaje=array();
		$mensaje['app_status']=1;

		echo '{ "app_status": 1,
		"podcast": [';
		$num_podcast=$seleccionando_podcast->num_rows;
		$num_ciclo=0;
		while($fila_podcasts=$seleccionando_podcast->fetch_array(MYSQLI_ASSOC)) {
			$num_ciclo++;

						$seleccionar_likes="SELECT * FROM usuarios_likes WHERE elemento='id_podcast=".$fila_podcasts['id_podcast']."'";
						$seleccionando_likes=$base_datos->query($seleccionar_likes);
						$num_likes=$seleccionando_likes->num_rows;
			echo '{
			"id_podcast": "'.$fila_podcasts['id_podcast'].'",
			"titulo": "'.utf8_encode($fila_podcasts['titulo']).'",
			"descripcion": "'.utf8_encode($fila_podcasts['descripcion']).'",
			"temporada": "'.$fila_podcasts['temporada'].'",
			"url_imagen": "'.$fila_podcasts['url_imagen'].'",
			"podcast_likes": "'.$num_likes.'",
			"podcast_url_share": "http://convoynetwork.com/convoy-webapp/podcast?id_podcast='.$fila_podcasts['id_podcast'].'",
			"fila_hora_publicacion": "'.$fila_podcasts['fila_hora_publicacion'].'",
			"status": "'.$fila_podcasts['status'].'" } ';
			if ($num_ciclo<$num_podcast) { echo ", "; }
		}
		echo ']}';
 	}
	function buscar_podcasts($podcast) {
		$base_datos=conexion();
		$seleccionar_podcast="SELECT * FROM podcast WHERE status=0 AND titulo LIKE '%".$podcast."%' ORDER BY id_podcast DESC LIMIT 0, 20";
		$seleccionando_podcast=$base_datos->query($seleccionar_podcast); 
		$mensaje=array(); 
		$mensaje['app_status']=1;
		echo '{ "app_status": 1,
		"podcast": [';
		$num_podcast=$seleccionando_podcast->num_rows;
		$num_ciclo=0;
		while($fila_podcasts=$seleccionando_podcast->fetch_array(MYSQLI_ASSOC)) {
			$num_ciclo++;
			echo '{
			"id_podcast": "'.$fila_podcasts['id_podcast'].'",
			"titulo": "'.utf8_encode($fila_podcasts['titulo']).'",
			"descripcion": "'.utf8_encode($fila_podcasts['descripcion']).'",
			"temporada": "'.$fila_podcasts['temporada'].'",
			"url_imagen": "'.$fila_podcasts['url_imagen'].'",
			"fila_hora_publicacion": "'.$fila_podcasts['fila_hora_publicacion'].'",
			"status": "'.$fila_podcasts['status'].'" } ';
			if ($num_ciclo<$num_podcast) { echo ", "; }
		}
		echo ']}';
		 
	}
	function buscar_usuario($usuario) {
		$base_datos=conexion();
		$seleccionar_usuarios="SELECT * FROM usuarios WHERE usuario LIKE '%".$usuario."%'";
		$seleccionando_usuarios=$base_datos->query($seleccionar_usuarios);
		$num_usuarios=$seleccionando_usuarios->num_rows;
		if ($num_usuarios>0) {
		$mensaje=array();
		$mensaje['app_status']=1;

		while($fila_usuarios=$seleccionando_usuarios->fetch_array(MYSQLI_ASSOC)) {
		$mensaje['usuarios'][]=$fila_usuarios; }
	} else {
		$mensaje['app_status']=0;
	}
	print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}

	function buscar_todo($buscar) {
		$base_datos=conexion();

		$mensaje=array();
		$mensaje['app_status']=1;

		$seleccionar_usuarios="SELECT * FROM usuarios WHERE usuario LIKE '%".$buscar."%'";
		$seleccionando_usuarios=$base_datos->query($seleccionar_usuarios);
		$num_usuarios=$seleccionando_usuarios->num_rows;
		if ($num_usuarios>0) {

		$mensaje['usuarios'][]['app_status']=1;
		while($fila_usuarios=$seleccionando_usuarios->fetch_array(MYSQLI_ASSOC)) {
		$mensaje['usuarios'][]=$fila_usuarios; }
		} else {
		$mensaje['usuarios'][]['app_status']=0;
		} 
	
		$seleccionar_usuarios="SELECT * FROM podcast WHERE titulo LIKE '%".$buscar."%'";
		$seleccionando_usuarios=$base_datos->query($seleccionar_usuarios);
		$num_usuarios=$seleccionando_usuarios->num_rows;
		if ($num_usuarios>0) {

		$mensaje['podcast'][]['app_status']=1;
		while($fila_usuarios=$seleccionando_usuarios->fetch_array(MYSQLI_ASSOC)) {
		$mensaje['podcast'][]=$fila_usuarios; }} else {

		$mensaje['podcast'][]['app_status']=0;
		} 
	

		$seleccionar_usuarios="SELECT * FROM radio WHERE titulo LIKE '%".$buscar."%'";
		$seleccionando_usuarios=$base_datos->query($seleccionar_usuarios);
		$num_usuarios=$seleccionando_usuarios->num_rows;
		if ($num_usuarios>0) {

		$mensaje['radio'][]['app_status']=1;
		while($fila_usuarios=$seleccionando_usuarios->fetch_array(MYSQLI_ASSOC)) {
		$mensaje['radio'][]=$fila_usuarios; }
		} else {

		$mensaje['radio'][]['app_status']=0;
		} 
	

		$seleccionar_usuarios="SELECT * FROM noticias WHERE cabeza LIKE '%".$buscar."%'";
		$seleccionando_usuarios=$base_datos->query($seleccionar_usuarios);
		$num_usuarios=$seleccionando_usuarios->num_rows;
		if ($num_usuarios>0) {

		$mensaje['noticias'][]['app_status']=1;
		while($fila_usuarios=$seleccionando_usuarios->fetch_array(MYSQLI_ASSOC)) {
		$mensaje['noticias'][]=$fila_usuarios; }} else {
		$mensaje['noticias'][]['app_status']=0;
		}
	
	print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}
	function ver_podcast($id_podcast) {
		
		$base_datos=conexion();
		$seleccionar_podcast="SELECT * FROM podcast WHERE id_podcast='".$id_podcast."'";
		$seleccionando_podcast=$base_datos->query($seleccionar_podcast);
		$fila_podcasts=$seleccionando_podcast->fetch_array(MYSQLI_ASSOC);
		echo '{ "app_status" : 1,
			"id_podcast": "'.$fila_podcasts['id_podcast'].'",
			"titulo": "'.utf8_encode($fila_podcasts['titulo']).'",
			"descripcion": "'.utf8_encode($fila_podcasts['descripcion']).'",
			"temporada": "'.$fila_podcasts['temporada'].'",
			"url_imagen": "'.$fila_podcasts['url_imagen'].'",
			"fila_hora_publicacion": "'.$fila_podcasts['fila_hora_publicacion'].'",
			"status": "'.$fila_podcasts['status'].'",
			"podcast_track" : [';

		$seleccionar_track="SELECT * FROM podcast_track WHERE id_podcast='".$id_podcast."' AND status=0 ORDER BY fecha_hora_activo";
		$seleccionando_track=$base_datos->query($seleccionar_track);
		$num_tracks=$seleccionando_track->num_rows;
		$num_ciclo=0;
		while($fila_tracks=$seleccionando_track->fetch_array(MYSQLI_ASSOC)) {
			$num_ciclo++;
			$fila_tracks['duracion']=str_replace("00:", "", $fila_tracks['duracion']);
			echo '{ "id_track": "'.$fila_tracks['id_track'].'",
			"id_podcast" : "'.$fila_tracks['id_podcast'].'",
			"episodio" : "'.$fila_tracks['episodio'].'",
			"titulo" : "'.utf8_encode($fila_tracks['titulo']).'",
			"duracion" : "'.$fila_tracks['duracion'].'",
			"descripcion" : "'.utf8_encode($fila_tracks['descripcion']).'",
			"url_track" : "'.$fila_tracks['url_track'].'"
		}';
		if ($num_ciclo<$num_tracks) { echo ','; }
		}
		echo ']}';
		// $fila_maestra=array_merge($fila_podcast, $fila_tracks, $mensaje);
		// print_r(json_encode($fila_maestra, JSON_PRETTY_PRINT));

	}

	function listar_radio($fecha, $hora) {

		$base_datos=conexion();

		// $seleccionar_podcast="SELECT * FROM radio WHERE status=0 AND fecha ='".$fecha."' AND hora BETWEEN DATE_ADD('".$hora."',INTERVAL -12 HOUR) AND  DATE_ADD('".$hora."',INTERVAL 12 HOUR) ORDER BY id_radio ASC LIMIT 0, 20";
		$seleccionar_podcast="SELECT * FROM radio WHERE (dia_semana='".date('N')."' OR dia_semana='8') AND DATE_ADD(NOW(),INTERVAL -1 HOUR) > hora OR  DATE_ADD(NOW(),INTERVAL 1 HOUR) < hora GROUP BY hora ORDER BY hora ASC LIMIT 0, 20";
		
		// $seleccionar_podcast="SELECT * FROM radio WHERE status=0 ORDER BY id_radio ASC LIMIT 0, 40";
		$seleccionando_podcast=$base_datos->query($seleccionar_podcast);
		if ($num_radios=$seleccionando_podcast->num_rows==0) { 
		$mensaje=array();
		$mensaje['app_status']=0;
		$mensaje['consulta']=$seleccionar_podcast;

		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
		} else {
		$fila_podcasts=$seleccionando_podcast->fetch_array(MYSQLI_ASSOC);

		$mensaje=array();
		echo '{ "app_status": 1,
		
		"radio": [';
		$num_podcast=$seleccionando_podcast->num_rows;
		$num_ciclo_p=0;
		while($fila_podcasts=$seleccionando_podcast->fetch_array(MYSQLI_ASSOC)) {
			$num_ciclo_p++;
			// echo $num_ciclo_p;
			echo '{
			"id_radio": "'.$fila_podcasts['id_radio'].'",
			"titulo": "'.utf8_encode($fila_podcasts['titulo']).'",
			"descripcion": "'.utf8_encode($fila_podcasts['descripcion']).'",
			"url_imagen": "'.$fila_podcasts['url_imagen'].'",
			"fecha": "'.$fila_podcasts['fecha'].'",
			"hora": "'.$fila_podcasts['hora'].'",
			"radio_url_share" : "http://convoynetwork.com/convoy-webapp/radio?id_radio='.$fila_podcasts['id_radio'].'",
			"url_radio" : "';
		$seleccionar_track="SELECT url_track FROM radio_tracks WHERE id_radio='".$fila_podcasts['id_radio']."' ORDER BY id_track";
		$seleccionando_track=$base_datos->query($seleccionar_track);
		$num_tracks=$seleccionando_track->num_rows;
		$num_ciclo=0;
		$fila_tracks=$seleccionando_track->fetch_array(MYSQLI_ASSOC);
			$num_ciclo++;
			echo urldecode($fila_tracks['url_track']);
			 
		echo '", "status": "'.$fila_podcasts['status'].'" } ';
			if ($num_ciclo_p<($num_podcast-1)) { echo ", "; }
		}
		echo ']}'; }
	}
	function ver_radio($id_radio) {
		
		$base_datos=conexion();
		  $seleccionar_podcast="SELECT * FROM radio WHERE status=0 AND id_radio='".$id_radio."'";
		$seleccionando_podcast=$base_datos->query($seleccionar_podcast);
		$fila_podcast=$seleccionando_podcast->fetch_array(MYSQLI_ASSOC);
		echo '{ "app_status": 1,
		"id_radio": "'.$fila_podcasts['id_radio'].'",
		"titulo": "'.utf8_encode($fila_podcasts['titulo']).'",
		"descripcion": "'.utf8_encode($fila_podcasts['descripcion']).'",
		"url_imagen": "'.$fila_podcasts['url_imagen'].'",
		"fecha": "'.$fila_podcasts['fecha'].'",
		"hora": "'.$fila_podcasts['hora'].'",
		"status": "'.$fila_podcasts['status'].'",
		"radio_tracks" : [';
		$seleccionar_track="SELECT * FROM radio_tracks WHERE id_radio='".$id_radio."' ORDER BY id_track";
		$seleccionando_track=$base_datos->query($seleccionar_track);
		$num_tracks=$seleccionando_track->num_rows;
		$num_ciclo=0;
		while($fila_tracks=$seleccionando_track->fetch_array(MYSQLI_ASSOC)) {
			$num_ciclo++;
			echo '{
			"id_track" : "'.$fila_tracks['id_track'].'",
			"url_track" : "'.$fila_tracks['url_track'].'",
			"episodio" : "'.$fila_tracks['episodio'].'",
			"descripcion" : "'.utf8_encode($fila_tracks['descripcion']).'",
			"status" : "'.$fila_tracks['status'].'"}
			';
			if ($num_ciclo<$num_tracks) { echo ", "; }
		};
	}
	function mostrar_actividades($id_usuario) {
		header('Content-Type: application/json');
		$base_datos=conexion();
		$seleccionar_notificaciones="SELECT * FROM notificaciones WHERE id_noticia=0 AND notificacion='' AND id_usuario='".$id_usuario."' ORDER BY id_notificacion DESC";
		echo '{';
		$seleccionando_notificaciones=$base_datos->query($seleccionar_notificaciones); 
		// $fila_notifica=$seleccionando_notificaciones->fetch_array(MYSQLI_ASSOC);
		// print_r(json_encode($fila_notifica, JSON_PRETTY_PRINT));
		$num_actividades=$seleccionando_notificaciones->num_rows;
		$ciclo_act=0;
		if ($num_actividades>0) {
			echo '"actividades" : [';
		while($fila_notifica=$seleccionando_notificaciones->fetch_array(MYSQLI_ASSOC)) {
			$ciclo_act++;
			echo '{ 
				
				"id_actividad" : "'.$fila_notifica['id_notificacion'].'",
				"id_usuario" : "'.$id_usuario.'",
				';
				  
				if ($fila_notifica['id_podcast']>0) {
					
					$seleccionar_podcast="SELECT * FROM podcast WHERE id_podcast='".$fila_notifica['id_podcast']."'";
					$seleccionando_podcast=$base_datos->query($seleccionar_podcast);
					$fila_podcast=$seleccionando_podcast->fetch_array(MYSQLI_ASSOC);
				echo '
				"podcast_id_podcast": "'.$fila_podcast['id_podcast'].'",
				"podcast_titulo": "'.utf8_encode($fila_podcast['titulo']).'",
				"tipo": "podcast",'; } else {
					echo '"podcast_id_podcast" : "",
					"podcast_titulo": "",
					';
				}
				if ($fila_notifica['id_radio']>0) {
					
					$seleccionar_radio="SELECT * FROM radio WHERE id_radio='".$fila_notifica['id_radio']."'";
					$seleccionando_radio=$base_datos->query($seleccionar_radio);
					$fila_radio=$seleccionando_radio->fetch_array(MYSQLI_ASSOC); 
				echo '
				"radio_id_radio" : "'.$fila_radio['id_radio'].'",
				"radio_titulo" : "'.utf8_encode($fila_radio['titulo']).'",
				"tipo": "radio",'; 
				} else {
					echo '"radio_id_radio" : "",
					"radio_titulo" : "",';
				}

				if ($fila_notifica['id_follower']>0) {{
					
					$seleccionar_follower="SELECT id_follower, id_usuario_propio, id_usuario_amigo, fecha_hora FROM followers WHERE id_usuario_amigo='".$fila_notifica['id_follower']."'";
					$seleccionando_follower=$base_datos->query($seleccionar_follower);
					$fila_follower=$seleccionando_follower->fetch_array(MYSQLI_ASSOC);
				echo '
				"followers_id_follower" : "'.$fila_follower['id_follower'].'",
				"followers_id_usuario_propio" : "'.$fila_follower['id_usuario_propio'].'",
				"followers_id_usuario_amigo" : "'.$fila_follower['id_usuario_amigo'].'",
				"tipo": "follower"'
				;
				}
			echo '
		

';
		} else {
			echo '"followers_id_follower":"",
			"followers_id_usuario_propio": "",
			"followers_id_usuario_amigo": ""';
		} echo '}';
		if ($ciclo_act<$num_actividades) { echo ', ';
		 }
		}
		echo '],
		"app_status": "1"}';

	} else {
		echo '"app_status": "0", }';
	} }
	

	

	function cargar_carpeta_elementos($id_carpeta) {
		$base_datos=conexion();
		$seleccionar_carpeta="SELECT * FROM usuarios_carpetas WHERE id_carpeta='".$id_carpeta."'";
		$seleccionando_carpeta=$base_datos->query($seleccionar_carpeta);
		$fila_carpeta_desc=$seleccionando_carpeta->fetch_array(MYSQLI_ASSOC);
		$seleccionar_elementos="SELECT * FROM usuarios_carpetas_elementos WHERE id_carpeta='".$id_carpeta."'";
		$insertando_coleccion=$base_datos->query($seleccionar_elementos);
		$num_coleccion=$insertando_coleccion->num_rows;
		if ($num_coleccion==0) {
			$mensaje=array();
			$mensaje['app_status']=0;
			print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
		} else {
		$fila_carpeta=$insertando_coleccion->fetch_array(MYSQLI_ASSOC);
		$mensaje=array();
		echo '{"app_status": "1", 
		"nombre_carpeta" : "'.$fila_carpeta_desc['nombre_carpeta'].'",
		"elementos": [{'; 
		// $mensaje=array_merge($mensaje, $fila_carpeta);
		$busca_noticia=strpos($fila_carpeta['elemento'], "noticia");
		$busca_podcast=strpos($fila_carpeta['elemento'], "podcast");
		$busca_radio=strpos($fila_carpeta['elemento'], "radio");
			if ($busca_noticia) {
				$seleccionar_noticias="SELECT * FROM noticias WHERE ".$fila_carpeta['elemento'];
				//$mensaje['elementos']['consulta']=$seleccionar_noticias;
				$seleccionando_noticias=$base_datos->query($seleccionar_noticias);
				$fila_noticias=$seleccionando_noticias->fetch_array(MYSQLI_ASSOC);
				echo '"nombre_elemento" : "'.$fila_noticias['cabeza'].'",
				"url_imagen" : "'.$fila_noticias['url_imagen'].'",
				"tipo_elemento" : "noticia"';

			}
			if ($busca_podcast) {
				$seleccionar_noticias="SELECT * FROM podcast WHERE ".$fila_carpeta['elemento'];
				//$mensaje['elementos']['consulta']=$seleccionar_noticias;
				$seleccionando_noticias=$base_datos->query($seleccionar_noticias);
				$fila_noticias=$seleccionando_noticias->fetch_array(MYSQLI_ASSOC);
				$mensaje['elementos']['nombre_elemento']=$fila_noticias['titulo'];
				$mensaje['elementos']['url_imagen']=$fila_noticias['url_imagen'];

				$mensaje['elementos']['tipo_elemento']='podcast';
			}

			if ($busca_radio) {
				$seleccionar_noticias="SELECT * FROM radio WHERE ".$fila_carpeta['elemento'];
				//$mensaje['elementos']['consulta']=$seleccionar_noticias;
				$seleccionando_noticias=$base_datos->query($seleccionar_noticias);
				$fila_noticias=$seleccionando_noticias->fetch_array(MYSQLI_ASSOC);
				
				$mensaje['elementos']['nombre_elemento']=$fila_noticias['titulo'];
				$mensaje['elementos']['url_imagen']=$fila_noticias['url_imagen'];

				$mensaje['elementos']['tipo_elemento']='radio';
			} 
		
		echo "}]}"; } }
	

	function guardar_carpeta($id_usuario, $tipo_carpeta, $nombre_carpeta) {
		$base_datos=conexion();
		$mensaje=array(); 
		$seleccionar_carpeta="SELECT * FROM usuarios_carpetas WHERE id_usuario='".$id_usuario."' AND nombre_carpeta='".$nombre_carpeta."'";
		$seleccionando_carpeta=$base_datos->query($seleccionar_carpeta);
		$num_carpeta=$seleccionando_carpeta->num_rows;
		if ($num_carpeta==0) {
		$insertar_carpeta="INSERT INTO usuarios_carpetas (id_usuario, nombre_carpeta, tipo_carpeta, fecha_hora, ip) VALUES ('".$id_usuario."', '".$nombre_carpeta."', '".$tipo_carpeta."', NOW(), '".$_SERVER['REMOTE_ADDR']."')";
		$insertando_carpeta=$base_datos->query($insertar_carpeta);
		
		$mensaje['aviso']="Carpeta guardada satisfactoriamente";
		$mensaje['app_status']=1;  } else {
			$mensaje['error']="El nombre de la carpeta ya existe";
			$mensaje['app_status']=0;
		}
		return $mensaje;
	}
	function guardar_elemento($id_usuario, $id_carpeta, $tipo_elemento, $id) {
		$base_datos=conexion();
		$insertar_coleccion="INSERT INTO usuarios_carpetas_elementos (id_carpeta, elemento, fecha_hora) VALUES ('".$id_carpeta."', 'id_".$tipo_elemento."=".$id."', NOW())";
		$insertando_coleccion=$base_datos->query($insertar_coleccion);
		$mensaje=array();
		$mensaje['app_status']=1;
		$mensaje['aviso']="Elemento agregado satisfactoriamente";
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}

	function nuevo_elemento_coleccion($id_usuario, $carpeta, $tipo_carpeta, $tipo_elemento, $id) {
		$base_datos=conexion();
		$insertar_coleccion="INSERT INTO usuarios_carpetas (id_usuario, nombre_carpeta, fecha_hora) VALUES ('".$id_usuario."', '".$carpeta."', NOW())";
		$insertando_coleccion=$base_datos->query($insertar_coleccion);
		$seleccionar_coleccion="SELECT * FROM usuarios_carpetas ORDER BY id_carpeta DESC LIMIT 0, 1";
		$seleccionando_coleccion=$base_datos->query($seleccionar_coleccion);
		$fila_carpeta=$seleccionando_coleccion->fetch_array();

		$insertar_coleccion="INSERT INTO usuarios_carpetas_elementos (id_carpeta, elemento, fecha_hora) VALUES ('".$fila_carpeta['id_carpeta']."', 'id_".$tipo_elemento."=".$id."', NOW())";
		$insertando_coleccion=$base_datos->query($insertar_coleccion);

		$mensaje=array();
		$mensaje['app_status']=1;
		$mensaje['aviso']="Elemento agregado satisfactoriamente";
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}	
	function compartir_elemento($id_elemento, $elemento, $id_usuario, $id_usuario_amigo) {
		$base_datos=conexion();
		$seleccionar_elementos="SELECT * FROM ".$elemento." WHERE id_".$elemento."='".$id_elemento."'";
		$seleccionando_elementos=$base_datos->query($seleccionar_elementos);
		$fila_elemento=$seleccionando_elementos->fetch_array(MYSQLI_ASSOC);
		print_r(json_encode($fila_elemento, JSON_PRETTY_PRINT));
	}
	function agregar_like($id_usuario, $elemento, $id) {
		$base_datos=conexion();
		$insertar_like="INSERT INTO usuarios_likes (id_usuario, elemento, fecha_hora, ip) VALUES ('".$id_usuario."', 'id_".$elemento."=".$id."', NOW(), '".$_SERVER['REMOTE_ADDR']."')";
		$insertando_like=$base_datos->query($insertar_like);
		{
			echo '{"app_status": "1" }';
		}

	}

	function eliminar_like($id_usuario, $elemento, $id) {
		$base_datos=conexion();
		$insertar_like="DELETE FROM usuarios_likes WHERE id_usuario='".$id_usuario."' AND elemento='id_".$elemento."=".$id."'";
		$insertando_like=$base_datos->query($insertar_like);
		{
			echo '{"app_status": "1" }';
		}

	}
	function cabecera($id_usuario, $cabecera) {
		$mensaje=array();
		$mensaje['app_status']=2;
		$base_datos=conexion();



		$seleccionar_cabecera="SELECT * FROM cabecera WHERE `fecha_hora` > DATE_SUB(NOW(), INTERVAL 3 MINUTE)";
		$seleccionando_cabecera=$base_datos->query($seleccionar_cabecera);
		while($fila_cabecera=$seleccionando_cabecera->fetch_array()){
		if ($cabecera!=$fila_cabecera['cabecera']) {
				$mensaje['app_status']=0;
			}
		}
		if ($mensaje['app_status']==0) {  } else { $mensaje['app_status']=1; }



		$insertar_cabecera="INSERT INTO cabecera (id_usuario, cabecera, fecha_hora, ip) VALUES ('".$id_usuario."', '".$cabecera."', NOW(), '".$_SERVER['REMOTE_ADDR']."')";
		$insertando_cabecera=$base_datos->query($insertar_cabecera);

		return $mensaje;
	}
	function reporte_problemas($id_usuario, $reporte) {
		$base_datos=conexion();
		$insertar_reporte="INSERT INTO usuarios_reportes (id_usuario, reporte, fecha_hora, ip) VALUES ('".$id_usuario."', '".$reporte."', NOW(), '".$_SERVER['REMOTE_ADDR']."')";
		$insertando_reporte=$base_datos->query($insertar_reporte);
		$mensaje=array();
		$mensaje['app_status']=1;
		print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}
	// Funciones internos
function buscar_contenido($buscar) {
		$base_datos=conexion();

		$mensaje=array();
		$mensaje['app_status']=1;
 
	
		$seleccionar_usuarios="SELECT * FROM Podcast WHERE podc_Title LIKE '%".$buscar."%'";
		$seleccionando_usuarios=$base_datos->query($seleccionar_usuarios);
		$num_usuarios=$seleccionando_usuarios->num_rows;
		if ($num_usuarios>0) {

		$mensaje['podcast'][]['app_status']=1;
		while($fila_usuarios=$seleccionando_usuarios->fetch_array(MYSQLI_ASSOC)) {
		$mensaje['podcast'][]=$fila_usuarios; }} else {

		$mensaje['podcast'][]['app_status']=0;
		} 
	

		$seleccionar_usuarios="SELECT * FROM Radio WHERE radi_Title LIKE '%".$buscar."%'";
		$seleccionando_usuarios=$base_datos->query($seleccionar_usuarios);
		$num_usuarios=$seleccionando_usuarios->num_rows;
		if ($num_usuarios>0) {

		$mensaje['radio'][]['app_status']=1;
		while($fila_usuarios=$seleccionando_usuarios->fetch_array(MYSQLI_ASSOC)) {
		$mensaje['radio'][]=$fila_usuarios; }
		} else {

		$mensaje['radio'][]['app_status']=0;
		} 
	

		$seleccionar_usuarios="SELECT * FROM News WHERE news_Title LIKE '%".$buscar."%'";
		$seleccionando_usuarios=$base_datos->query($seleccionar_usuarios);
		$num_usuarios=$seleccionando_usuarios->num_rows;
		if ($num_usuarios>0) {

		$mensaje['noticias'][]['app_status']=1;
		while($fila_usuarios=$seleccionando_usuarios->fetch_array(MYSQLI_ASSOC)) {
		$mensaje['noticias'][]=$fila_usuarios; }} else {
		$mensaje['noticias'][]['app_status']=0;
		}
		if ($num_usuarios==0 && $num_radio==0 && $num_podcast==0) { $mensaje['app_status']=0; }
	print_r(json_encode($mensaje, JSON_PRETTY_PRINT));
	}
	function guardar_notificacion($id_usuario, $modulo, $id) {
		$base_datos=conexion();

		if ($modulo=="noticia") { 
			$insertar_notificacion="INSERT INTO notificaciones (id_noticia, id_usuario, fecha_hora) VALUES ('".$id."', '".$id_usuario."', NOW())"; 
		}
		if ($modulo=="podcast") {
			$insertar_notificacion="INSERT INTO notificaciones (id_podcast, id_usuario, fecha_hora) VALUES ('".$id."', '".$id_usuario."', NOW())";
		}
		if ($modulo=="radio") {
			$insertar_notificacion="INSERT INTO notificaciones (id_radio, id_usuario, fecha_hora) VALUES ('".$id."', '".$id_usuario."', NOW())";
		}
		if ($modulo=="followers") {
			$insertar_notificacion="INSERT INTO notificaciones (id_follower, id_usuario, fecha_hora) VALUES ('".$id."', '".$id_usuario."', NOW())";
		}

		$insertando_notificacion=$base_datos->query($insertar_notificacion);
		return true;

	}

	// Servicios exclusivos para cronjob

	function pago_cancelado($id_usuario, $periodo) {
		$base_datos=conexion();
	}

	
?>
