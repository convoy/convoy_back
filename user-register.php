<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/User.Queries.php"); 
	$handle = fopen('php://input','r');
	$jsonInput = fgets($handle);
	$jsonArray = json_decode($jsonInput,true);
	$Login = $jsonArray['Login'];
	$Password = $jsonArray['Password'];
	$Email = $jsonArray['Email'];
	//$Birthdate = $jsonArray['Birthdate'];
    $Birthdate = isset($jsonArray['Birthdate']) ? $jsonArray['Birthdate'] : false;
	UserRegister($Login, $Email, $Password, $Birthdate);
	}
else{
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
	}
?>