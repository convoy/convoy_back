<?php 
header('Content-Type: application/json; charset=utf-8');
require_once("ws-queries/function.ApiKey.php"); 
$Headers = getallheaders();
if(!isset($Headers['Apikey'])){
	$Headers['Apikey']='';
	}
if(ApiKeyString($Headers['Apikey'])==true){
	require_once("ws-queries/Purchase.Queries.php");
	$handle = fopen('php://input','r');
	// Decoding JSON into an Array
	$jsonInput=fgets($handle);
	$jsonArray = json_decode($jsonInput,true);

	$UserID = $jsonArray['UserID'];
	$Expires = $jsonArray['Expires'];
    $ProductID = $jsonArray['ProductID'];
    $LastPurchase = $jsonArray['LastPurchase'];
    $Quantity = $jsonArray['Quantity'];
    $TransactionID = $jsonArray['TransactionID'];
    $CancelDate = $jsonArray['CancelDate'];

	PurchaseUpdate($UserID, $Expires, $ProductID, $LastPurchase, $Quantity, $TransactionID, $CancelDate);
    
} else {
	echo '{"AppStatus":"0", "AppResponse":"Permiso denegado"}';
}
?>